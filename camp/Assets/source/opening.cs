﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class opening : MonoBehaviour {
	[SerializeField]
	private UIController _uic;

	[SerializeField]
	private RectTransform _kuma;
	[SerializeField]
	private RectTransform _kumaF;
	[SerializeField]
	private Text _kumaTxt;
	[SerializeField]
	private RectTransform _kaeru;
	[SerializeField]
	private RectTransform _kaeruF;
	[SerializeField]
	private Text _kaeruTxt;
	[SerializeField]
	private RectTransform[] _mission;
	[SerializeField]
	private Transform _kido;
	[SerializeField]
	private RectTransform _gyokuza;
	[SerializeField]
	private GameObject _sankaku;
	[SerializeField]
	private GameObject _waitting;
	[SerializeField]
	private Text _waittingTxt;
	[SerializeField]
	private GameObject _tutoG;
	[SerializeField]
	private GameObject _skipB;
	[SerializeField]
	private GameObject _tapSkipB;

	private string _kumaSt = "まーた捕まったわ。\nさっさと助けに来い、\nこのカエル野郎。";
	private string _kaeruSt = "面倒くせえクマだな…\nいまそっち行くから\n黙って待ってろよ！";

	private bool _isShowTuto = false;
	private int _tutoNo = 1;
	private float _timeTuto = 0f;
	private float _timeBlink = 0f;

	void OnEnable ()
	{
		StartCoroutine (playOpening());
	}

	private IEnumerator playOpening()
	{
		if (Cpc.ONLINEMODE == true)
		{
			_skipB.SetActive(false);
		}
		else
		{
			_skipB.SetActive(true);
		}

		int it = 0;
		float ft = 0f;
		string st = "";

		while(ft < 1f)
		{
			_kuma.localScale = new Vector3(1f, ft, 1f);
			ft += 0.2f;
			yield return new WaitForSeconds (0.03f);
		}
		_kuma.localScale = new Vector3(1f, 1f, 1f);

		ft = 0f;
		while(ft < 1f)
		{
			_kumaF.localScale = new Vector3(ft, 1f, 1f);
			ft += 0.2f;
			yield return new WaitForSeconds (0.03f);
		}
		_kumaF.localScale = new Vector3(1f, 1f, 1f);

		st = "";
		while(it < _kumaSt.Length)
		{
			st += _kumaSt[it];
			_kumaTxt.text = st;
			it++;
			yield return new WaitForSeconds (0.03f);
		}

		yield return new WaitForSeconds (0.5f);
		ft = 0f;
		while(ft < 1f)
		{
			_kaeru.localScale = new Vector3(1f, ft, 1f);
			ft += 0.2f;
			yield return new WaitForSeconds (0.03f);
		}
		_kaeru.localScale = new Vector3(1f, 1f, 1f);

		ft = 0f;
		while(ft < 1f)
		{
			_kaeruF.localScale = new Vector3(ft, 1f, 1f);
			ft += 0.2f;
			yield return new WaitForSeconds (0.03f);
		}
		_kaeruF.localScale = new Vector3(1f, 1f, 1f);

		st = "";
		it = 0;
		while(it < _kaeruSt.Length)
		{
			st += _kaeruSt[it];
			_kaeruTxt.text = st;
			it++;
			yield return new WaitForSeconds (0.03f);
		}

		yield return new WaitForSeconds (0.8f);
		_kaeru.localScale = new Vector3(1f,0, 1f);
		_kaeruF.localScale = new Vector3(0, 1f, 1f);
		yield return new WaitForSeconds (0.5f);


		GameObject pre = (GameObject)Resources.Load ("imgMaru");

		//点線
		it = 0;
		while(it < 68)
		{
			GameObject instant_object = GameObject.Instantiate(pre, Vector3.zero, Quaternion.identity);
//			instant_object.transform.parent = _kido;
			instant_object.transform.SetParent(_kido);

			if (it < 13) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f, 27.26f + 0.8f * it, 0f);
			} else if (it < 31) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-it)*0.8f, 27.26f + 0.8f*13, 0f);
			} else if (it < 55) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-31)*0.8f, 27.26f + 0.8f*13 + (it-31)*0.8f, 0f);
			} else if (it < 60) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-31)*0.8f + (55-it)*0.8f, 27.26f + 0.8f*13 + (55-31)*0.8f, 0f);
			} else {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-31)*0.8f + (55-60)*0.8f, 27.26f + 0.8f*13 + (55-31)*0.8f + (it-60)*0.8f, 0f);
			}
			instant_object.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (0f, 0f, 0f);
			it++;
			yield return new WaitForSeconds(0.01f);
		}

		yield return new WaitForSeconds(0.2f);
		_sankaku.SetActive (true);

		yield return new WaitForSeconds(0.5f);
		_sankaku.SetActive (false);

		//点線2
		foreach ( Transform n in _kido.transform )
		{
			if (n.gameObject.name != "imgSankaku")
			{
				GameObject.Destroy(n.gameObject);
			}
		}

		while(it >= 15)
		{
			GameObject instant_object = GameObject.Instantiate(pre, Vector3.zero, Quaternion.identity);
//			instant_object.transform.parent = _kido;
			instant_object.transform.SetParent(_kido);

			if (it < 21) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-21)*0.8f, 27.26f + 0.8f*13+ (it-21)*0.8f, 0f);
			} else if (it < 31) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-it)*0.8f, 27.26f + 0.8f*13, 0f);
			} else if (it < 55) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-31)*0.8f, 27.26f + 0.8f*13 + (it-31)*0.8f, 0f);
			} else if (it < 60) {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-31)*0.8f + (55-it)*0.8f, 27.26f + 0.8f*13 + (55-31)*0.8f, 0f);
			} else {
				instant_object.GetComponent<RectTransform> ().localPosition = new Vector3 (55.56f + (13-31)*0.8f + (55-60)*0.8f, 27.26f + 0.8f*13 + (55-31)*0.8f + (it-60)*0.8f, 0f);
			}
			instant_object.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (0f, 0f, 0f);
			it--;
			yield return new WaitForSeconds(0.01f);
		}
		yield return new WaitForSeconds(0.2f);
		_sankaku.transform.localRotation = Quaternion.Euler(180,0,0);
		_sankaku.transform.localPosition = new Vector3 (48.565f , 30.338f, 0f);
		_sankaku.SetActive (true);

		_kuma.gameObject.SetActive(false);
		_kumaF.gameObject.SetActive(false);

		yield return new WaitForSeconds(1f);

		it = 0;
		while (it < 10) {
			if (Cpc.MaxChara == 10)
			{
				_mission[1].localPosition = new Vector3 (_mission[1].localPosition.x + it * 0.428f, _mission[1].localPosition.y, _mission[1].localPosition.z);
			}
			else
			{
				_mission[0].localPosition = new Vector3 (_mission[0].localPosition.x + it * 0.428f, _mission[0].localPosition.y, _mission[0].localPosition.z);
			}
			it++;
			yield return new WaitForSeconds(0.01f);
		}

		yield return new WaitForSeconds(1f);


		ft = 0f;
		while(ft < 1f)
		{
			_gyokuza.localScale = new Vector3(1f, ft, 1f);
			ft += 0.2f;
			yield return new WaitForSeconds (0.03f);
		}
		_gyokuza.localScale = new Vector3(1f, 1f, 1f);

		it = 0;
		int it2 = 0;
		while (it < 70)
		{
			if (it2 == 21) {
				it2 = 0;	
			} else if (it2 > 10) {
				_gyokuza.localPosition = new Vector3 (_gyokuza.localPosition.x, _gyokuza.localPosition.y - 0.04f, _gyokuza.localPosition.z);
				it2++;
			} else if (it2 < 11) {
				_gyokuza.localPosition = new Vector3 (_gyokuza.localPosition.x, _gyokuza.localPosition.y + 0.04f, _gyokuza.localPosition.z);
				it2++;
			}
			it++;

			yield return new WaitForSeconds(0.01f);
		}
		_gyokuza.localScale = new Vector3(1f, 0, 1f);

		_waitting.SetActive (true);


		//チュートリアル画像表示
		_mission[0].gameObject.SetActive(false);
		_mission[1].gameObject.SetActive(false);
		_tutoG.SetActive(true);
		_isShowTuto = true;
		_tutoNo = 1;
		_tutoG.transform.FindChild("tuto" + _tutoNo).transform.gameObject.SetActive(true);

		int iCount = 25;

		int iC = 0;

		_tapSkipB.SetActive(true);

		if (Cpc.isFastOpening == true)
		{
			PhotonNetwork.room.IsOpen = false;
			Cpc.netC.MakeComList();
			_uic.initName();
		}
		else
		{
			while(iC < iCount)
			{
				_waittingTxt.text = (iCount - iC)+"";
				yield return new WaitForSeconds(1f);
				iC++;

				//10秒前に締め切り---------------------------------
				if ((iCount - iC) == 10 && Cpc.ONLINEMODE == true)
				{
					PhotonNetwork.room.IsOpen = false;
				}
				if ((iCount - iC) == 9 && Cpc.ONLINEMODE == true)
				{
					Cpc.netC.MakeComList();
				}
				if ((iCount - iC) == 8 && Cpc.ONLINEMODE == true)
				{
					_uic.initName();
				}
			}
		}

		_tapSkipB.SetActive(false);
		//開始---------------------------------
		if (Cpc.ONLINEMODE == true)
		{
			if (Cpc.appState == Cpc.AppState.matching)
			{
				yield return new WaitForSeconds(1f);
				Cpc.changeAppState(Cpc.AppState.battleStart);
			}
		}
		//オフライン用
		else
		{
			_skipB.SetActive(false);
			_uic.startOffLine ();
		}
		this.gameObject.SetActive (false);
		yield return null;
	}

	void Update()
	{

		if (_isShowTuto == true)
		{
			if (_timeTuto > 2.7f)
			{
				onTaptapSkip();
			}
			else
			{
				_timeTuto += Time.deltaTime;
			}

		}

		if (_tapSkipB.activeSelf == true)
		{
//			Debug.Log("_timeBlink : " + _timeBlink);

			if (_timeBlink > 2.4f)
			{
				_timeBlink = 0;

			}
			else
			{
				_tapSkipB.GetComponent<Image>().color = Util.rgbColor(255, 255, 255,_timeBlink/2.4f);
				_tapSkipB.transform.FindChild("Text").GetComponent<Text>().color = Util.rgbColor(210, 202, 202, _timeBlink/2.4f);
				_timeBlink += Time.deltaTime;
			}


		}
	}

	public void onTapSkip()
	{
		_uic.initName();
		_tapSkipB.SetActive(false);
		_skipB.SetActive(false);
		_uic.startOffLine ();
		this.gameObject.SetActive (false);

	}

	public void onTaptapSkip()
	{
		Debug.Log("_tutoNo : " + _tutoNo);

		_timeTuto = 0;
		_tutoG.transform.FindChild("tuto" + (_tutoNo+1)).transform.gameObject.SetActive(true);

		if (_tutoNo == 0)
		{
			_tutoG.transform.FindChild("tuto" + 9).transform.gameObject.SetActive(false);
		}
		else
		{
			_tutoG.transform.FindChild("tuto" + _tutoNo).transform.gameObject.SetActive(false);
		}

		if (_tutoNo < 8)
		{
			_tutoNo++;
		}
		else
		{
			_tutoNo = 0;
		}
	}
}
