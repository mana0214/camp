﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hamburger : MonoBehaviour {

	public Cpc.Team myteam;
	private Transform _mapTran;

	public bool isFix = false;

	void OnEnable ()
	{
		if (Cpc.ONLINEMODE == true)
		{
			if(PhotonNetwork.player.ID != 1)
			{
				_mapTran = GameObject.Find("map").gameObject.transform;
				this.transform.parent = _mapTran;
			}
		}
	}

	void Update()
	{
		//重力
		if (isFix == false)
		{
			if (this.transform.position.y > -5f)
			{
				this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y-0.2f, this.transform.position.z);
			}
		}
	}

	/*
	public void SendTransform()
	{
		Cpc.netC.SendHamPos(this.transform.position, this.gameObject.GetComponent<PhotonView>().viewID);
	}
	*/

}
