﻿using UnityEngine;
using System;

public class Util
{
    //Canvasの座標に変換
	/*
    public static Vector2 changeCanvasPos (Vector3 pos, float sizeX, float sizeY)
    {
        Vector3 worldPos = Camera.main.ScreenToWorldPoint (new Vector3 (pos.x, pos.y, 10f));
        Vector3 screenPos = Camera.main.WorldToViewportPoint (worldPos);
        Vector2 newPos = new Vector2 ((screenPos.x * sizeX - sizeX * 0.5f), (screenPos.y * sizeY - sizeY * 0.5f));

        return newPos;
    }
    */

	public static Vector2 changeCanvasPos ()
	{
		Vector3 screen_position;
		//座標の変換
		#if UNITY_EDITOR
		screen_position = Input.mousePosition;
		#else
		screen_position = Input.touches[0].position;
		#endif

		screen_position.z = Camera.main.transform.position.y + 2959f;
		screen_position = Camera.main.ScreenToWorldPoint(screen_position);
		return new Vector2(screen_position.x, screen_position.y);
	}

    /// <summary>
    /// 2点か間の距離計算
    /// </summary>
    /// <param name="x1">X座標その１</param>
    /// <param name="y1">y座標その１</param>
    /// <param name="x2">X座標その２</param>
    /// <param name="y2">y座標その２</param>
    /// <returns>2点間の距離</returns>
    public static float geoLength (float x1, float y1, float x2, float y2)
    {
        double ret = Math.Sqrt (Math.Pow (x2 - x1, 2) + Math.Pow (y2 - y1, 2));
        return (float)ret;
    }

	//色変換
	public static Color rgbColor(int r, int g, int b, float a=1.0f)
	{
		return new Color((float)(r/(float)255),(float)(g/(float)255),(float)(b/(float)255),a);
	}

	public static GameObject playEff(string effName, Cpc.Team team, Transform parent, Vector3 position, float rotX, float rotY, float rotZ, bool isDestory=true, float delaySec=0.5f, float posY=0f)
	{
		if (team == Cpc.Team.Red)
		{
			effName = "eff_" + effName + "_r";
		}
		else if(team == Cpc.Team.Blue)
		{
			effName = "eff_" + effName + "_b";
		}
		else if(team == Cpc.Team.NoTeam)
		{
			effName = "eff_" + effName;
		}

//		Debug.Log("effName :" + effName);

		GameObject instant_object = null;

		GameObject pre = (GameObject)Resources.Load (effName);
		instant_object = (GameObject) GameObject.Instantiate(pre, position, Quaternion.identity);

		instant_object.transform.parent = parent;

//		Debug.Log("----------------- " + rotX + "|" + rotY + "|" + rotZ);

		instant_object.transform.localRotation = Quaternion.Euler(rotX, rotY, rotZ);

//		instant_object.transform.localRotation = Quaternion.Euler(-90f, 0, 0);
		if (posY != 0f) {
			instant_object.transform.localPosition = new Vector3(0f, posY, 0f);
		}

		/*
		if (effName == "")
		{
			instant_object.transform.localRotation = Quaternion.Euler(-90,0,0));
		}
		*/

		if (isDestory == true)
		{
			GameObject.Destroy(instant_object,delaySec);
		}

		if (Cpc.ONLINEMODE == true)
		{
			//waterは送らない
			if (effName != "eff_water")
			{
				if (effName == "eff_attack_r" || effName == "eff_attack_b" || effName == "eff_attack_bk" || effName == "eff_attack_wh")
				{
					Cpc.netC.SendEffect(effName, instant_object.transform.position, 180f, rotY, 0f, isDestory, delaySec);
				}
				else
				{
					Cpc.netC.SendEffect(effName, instant_object.transform.position, rotX, -1*rotY, rotZ, isDestory, delaySec);
				}
			}
		}

		return instant_object;

	}

	public static void playEffByNet(string sInfo)
	{
		string[] st = sInfo.Split('|');

//		Debug.Log("EFF      " + sInfo);


		GameObject pre = (GameObject)Resources.Load (st[1]);
		GameObject instant_object = (GameObject) GameObject.Instantiate(pre, Vector3.zero, Quaternion.identity);

		instant_object.transform.position = new Vector3(float.Parse(st[2]), float.Parse(st[3]),float.Parse(st[4]));
		instant_object.transform.rotation = Quaternion.Euler(float.Parse(st[5]), float.Parse(st[6]), float.Parse(st[7]));

		if (st[8] == "1")
		{
			GameObject.Destroy(instant_object,float.Parse(st[9]));
		}
	}

	// p2からp1への角度を求める
	// @param p1 自分の座標
	// @param p2 相手の座標
	// @return 2点の角度(Degree)
	public static float GetAim(Vector2 p1, Vector2 p2)
	{
		float dx = p2.x - p1.x;
		float dy = p2.y - p1.y;
		float rad = Mathf.Atan2(dy, dx);
		return rad * Mathf.Rad2Deg;
	}
}
