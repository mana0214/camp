﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SwipeDetector : MonoBehaviour
{
    internal enum dirSwipe
    {
        none,
        up,
        left,
        right,
        down
    }

    //スワイプ判定の最低距離
    private float minSwipeDistX = 100;
    private float minSwipeDistY = 20;
    //実際にスワイプした距離
    internal float swipeDistX;
    internal float swipeDistY;
    //方向判定に使うSign値
    internal float SignValueX;
    internal float SignValueY;
    //タッチしたポジション
    internal Vector2 startPos;
    //タッチを離したポジション
    private Vector2 endPos;

    internal dirSwipe dswipe;
    internal Vector3 tpos;
    internal TouchPhase tp;
	internal bool isIntouch = false;

#if !UNITY_EDITOR
    private static Vector3 TouchPosition = Vector3.zero;
#endif

    public static TouchPhase GetTouch ()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown (0)) { return TouchPhase.Began; }
        if (Input.GetMouseButton (0)) { return TouchPhase.Moved; }
        if (Input.GetMouseButtonUp (0)) { return TouchPhase.Ended; }
#else
            if (Input.touchCount > 0)
            {
                return (TouchPhase)((int)Input.GetTouch(0).phase);
            }
#endif
        return TouchPhase.Stationary;
    }

    public static Vector3 GetTouchPosition ()
    {

#if UNITY_EDITOR
        TouchPhase touch = GetTouch ();
        if (touch != TouchPhase.Stationary) { return Input.mousePosition; }
#else
		Debug.Log("Input.touchCount : " + Input.touchCount);
            if (Input.touchCount > 0)
            {
                Touch touch = Input.touches [0];
                TouchPosition.x = touch.position.x;
                TouchPosition.y = touch.position.y;
                return TouchPosition;
            }
#endif
		Debug.Log("Vector3.zero !!! ");
        return Vector3.zero;
    }

    internal void checkSwipe ()
    {
        tp = GetTouch ();
        //タッチされたら
        if (tp != TouchPhase.Stationary) {
            tpos = GetTouchPosition ();

            switch (tp) {
            //タッチ開始時
			case TouchPhase.Began:
				startPos = tpos;
				isIntouch = true;
                break;
            //タッチ中
            case TouchPhase.Moved:
                dswipe = dirSwipe.none;
                //タッチ終了のポジションをendPosに代入
                endPos = new Vector2 (tpos.x, tpos.y);

                //横方向判定
                //X方向にスワイプした距離を算出
                swipeDistX = (new Vector3 (endPos.x, 0, 0) - new Vector3 (startPos.x, 0, 0)).magnitude;
                //xの差分をとっているので絶対にサインの値は1(90度)か-1(270度)
                SignValueX = Mathf.Sign (endPos.x - startPos.x);

                //Y方向にスワイプした距離を算出
                swipeDistY = (new Vector3 (0, endPos.y, 0) - new Vector3 (0, startPos.y, 0)).magnitude;
                //y座標の差分のサインを計算
                //yの差分をとっているので絶対にサインの値は1(90度)か-1(270度)
                SignValueY = Mathf.Sign (endPos.y - startPos.y);

                if (swipeDistX > minSwipeDistX)
                {
                    if (SignValueX > 0)
                    {
                        //右方向にスワイプしたとき
                        dswipe = dirSwipe.right;
                    } else if (SignValueX < 0)
                    {
                        //左方向にスワイプしたとき
                        dswipe = dirSwipe.left;
                    }
                } else {
                    //縦方向判定
                    //差分が最低スワイプ分を超えていた場合
                    if (swipeDistY > minSwipeDistY)
                    {
                        if (SignValueY > 0)
                        {
                            //上方向にスワイプしたとき
                            dswipe = dirSwipe.up;
                        }
                        else if (SignValueY < 0)
                        {
                            //sin = -1
                            //下方向にスワイプしたとき
                            dswipe = dirSwipe.down;
                        }
                    }
                }
                break;

			case TouchPhase.Ended:
				isIntouch = false;

                break;
            }
        }
    }
}