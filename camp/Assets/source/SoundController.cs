﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
	[SerializeField]
	private AudioClip[] bgm;
	[SerializeField]
	private AudioClip[] se;
	[SerializeField]
	private AudioClip[] voice;

	[SerializeField]
	private AudioSource bgmSource;
	[SerializeField]
	private AudioSource seSource;

	public void changeBgm(int bgmNo=-1)
	{
		if (bgmNo == -1)
		{
			switch(Cpc.appState)
			{
			case Cpc.AppState.title:
				bgmSource.clip = bgm[6];
				break;
			case Cpc.AppState.matching:
				bgmSource.clip = bgm[7];
				break;
			case Cpc.AppState.battle:
				bgmSource.clip = bgm[4];
				break;
			case Cpc.AppState.battleWin:
				bgmSource.clip = bgm[2];
				break;
			case Cpc.AppState.battleLose:
				bgmSource.clip = bgm[0];
				break;
			case Cpc.AppState.countdown:
				bgmSource.clip = bgm[5];
				break;
			case Cpc.AppState.battleTimeUp:
				bgmSource.clip = bgm[3];
				break;
			default:
				bgmSource.clip = null;
				break;
			}
//			Debug.Log("BGM 1 :" + Cpc.appState);
		}
		else
		{
//			Debug.Log("BGM 2 :" + (Cpc.AppState)bgmNo);
			bgmSource.clip = bgm[bgmNo];
		}



		if (bgmSource.clip == null)
		{
			bgmSource.Stop();
		}
		else
		{
			bgmSource.Play ();
		}
	}

	public void playSe(Cpc.SeType type)
	{
		seSource.PlayOneShot (se[(int)type]);
	}
}