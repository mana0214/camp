﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class water : MonoBehaviour {

	void OnTriggerEnter(Collider collision)
	{
		if (collision.gameObject.name.StartsWith("myChara_") == true) {
			collision.gameObject.GetComponent<chara> ().inWater ();
		}	
	}

	void OnTriggerExit(Collider collision)
	{
		if (collision.gameObject.name.StartsWith("myChara_") == true) {
			collision.gameObject.GetComponent<chara> ().outWater ();
		}
	}
}
