﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Cpc : MonoBehaviour
{
	[SerializeField]
	private UIController _uic;
	[SerializeField]
	private SoundController _sc;
	[SerializeField]
	private NetController _netc;

	//くま
	//	[SerializeField]
	public static GameObject[] _kumaObj;

	public static chara _myChara = null;
	private static AppState _stateBk;

	//SETTINGS ##################################################################################
	//ログモード
	public const bool LOGMODE = true;
	public const bool isSkipOpening = false;
	public const bool isFastOpening = false; //オープニング後の待ち時間短縮

	public static int MaxChara = 10;  //バトル中参加キャラ数

	public const int MaxHumburger = 3; //最大ハンバーガー数
	public const float HUMBTIME = 3f; //ハンバーガー追加感覚
	//UI関連
	//	public const float MAXPOINTER  = 360f;  //ポインター移動範囲
	public const float MAXPOINTER  = 720f;  //ポインター移動範囲
	//	public const float POINTERRYO  = 0.2f;  //ポインター速度
	//	public const float POINTERRYO  = 0.8f;  //ポインター速度
	public const float POINTERRYO  = 1.2f;  //ポインター速度
	//	public const float IDOSPEED    = 0.008f; //移動速度
	public const float IDOSPEED    = 0.03f; //移動速度
	//	public const float JUNMPRANGE  = 100;   //ジャンプ判定値
	public const float JUNMPRANGE  = 500;   //ジャンプ判定値
	public const float JUMPHEIGHT  = 1.1f;  //ジャンプの高さ
	public const float JUMPWIDTH   = 2.0f;  //ジャンプ移動量
	public const int MAXWEIGHT = 8;   //最大くま体重
	public const float ROTATESPEED = 500f;  //回旋速度

	//バトル設定
	public const int TIMEMAX     = 180;  //バトル時間
//	public const int TIMEMAX     = 20;  //バトル時間
	public const int KUMACOUNT   = 30;   //くま設置時間
	public const int KUMAUIMAX   = 820;  //くまUIの最大位置
	public const int KUMAWEIGHT  = 6;   //クマの重さ
	public const float BENCHRANGE = 4f;  //ベンチの範囲
	public const float TIME_REVIVE = 5f; //復活時間
	public const float TIME_MAKEC  = 4f; //キャラ生成間隔
	public const float SUPPORT_RANGE = 2.8f; //くまのサポート範囲
	public const float LEFT_DIS = 1.8f;  //LEFTアクション判定距離
	public const float LOSEWTTIME = 30.0f; //くまが痩せる間隔
	public const float MASIC_STIFFNESS = 1f; //魔導師魔法の硬直時間
	public const float MASIC_INTERVAL = 2.5f; //魔導師魔法の可能間隔

	public const float ATK_RANGE = 1.2f;
	public const float ATK_M_RANGE = 6f;

	//COM設定
	public const float COM_SEARCH_T = 0.3f; //検索間隔
	public const float COM_SPDCK_T = 0.3f;  //移動速度監視間隔
	public const float COM_RUN_DIS = 0.6f;    //RUNアクション判定距離
	public const float COM_ATK_DIS = 6f;    //攻撃開始距離
	public const float COM_ATK_MIN = 0.4f;  //攻撃間隔(最短)
	public const float COM_ATK_MAX = 1.4f;  //攻撃間隔(最長)
//	public const float COM_SPD_MAX = 3.5f;    //最大スピード
	public const float COM_SPD_MAX = 4.5f;    //最大スピード
	public const float COM_CHANGEAI = 8.0f;  //AI思考変更判定間隔
	public const int COM_CHANGEAIRATE = 50;  //AI思考変更確率(%)

	//STATIC ##################################################################################
	public static bool ONLINEMODE = false;

	public static string[] preFix = new string[] {"r", "b"};

	public static int[]   JobHP  = new int [] { 500, 250, 250 };
	public static int[]   Atk    = new int [] { 100, 50, 50 };
	public static string[] playerName = new string[]{"ライオン","トラ","チーター","ヒョウ","カラカル","ウマ","アルパカ","リス","ヤギ","ラクダ","イタチ","ウシ","ロバ","イルカ","モモンガ","ゴリラ","クズリ","ウサギ","ラマ","ネコ","ドール","キツネ","タヌキ","コアラ","ヒツジ","ゾウ","ブタ","キリン","オカピ","カバ","ラマ","ポニー","トナカイ","ボンゴ","ジャガー","ネズミ","テン","サイ","シカ","ムササビ"};
	public static int[] AIRate  = new int[] {70 ,30, 0};

	public static List<Transform> charaPos_r = new List<Transform>();
	public static List<Transform> charaPos_b = new List<Transform>();

	//初期位置関係
	public static Vector3 KUMAInitPos_r = new Vector3(-90f, 2f, 12f);
	public static Vector3 KUMAInitPos_b = new Vector3(-56f , 2f, 36f);
	public static Quaternion KUMAInitRot_r = Quaternion.Euler(0,0,0);
	public static Quaternion KUMAInitRot_b = Quaternion.Euler(0,0,0);
	public static Vector3 myInitPos_r = new Vector3(-58.4f, 2f, 25f);
	public static Vector3 myInitPos_b = new Vector3(-94.4f, 4f, 31.46f);
	public static Quaternion myInitRot_r = Quaternion.Euler(0,89.9f,0);
	public static Quaternion myInitRot_b = Quaternion.Euler(0,89.9f,0);

	public static int myPlayerNo = -1;

	//マイネーム
	public static string myName = "";

	//要クリア -------
	public static int[] charaNum  = new int[] {0 ,0};
	public static int[] burgerNum  = new int[] {0 ,0};
	public static int[] killNum  = new int[] {0 ,0};
	public static int[] killNumC  = new int[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	public static CharaInfo[] playerRoster = new CharaInfo[20];
	public static GameObject[] playerObject = new GameObject[20];
	public static bool isMaster = true;
	public static int _playerRosterNo;
	public static int makedPlayer = 0; //for Net
	public static bool isCanMakePlayer = true; //for Net
	public static bool isCanComPlayer = true; //for Net
	public static int[] AICounter_r = new int[] {0,0,0,0,0,0,0,0,0,0};
	public static int[] AICounter_b = new int[] {0,0,0,0,0,0,0,0,0,0};
	public static bool[] isGuideShow = new bool[] {false, false, false, false, false};
	private static int makedComR = 0;
	private static int makedComB = 0;
	//要クリア -------

	public static Cpc.CharaInfo mycharaInfo;

	private bool isReusultShow = false;

	/// <summary>アプリステータス</summary>
	public static AppState appState
	{
		private set;
		get;
	}

	/// <summary>現在チーム</summary>
	public static Team myTeam
	{
		set;
		get;
	}

	public static NetController netC
	{
		set;
		get;
	}

	public static UIController uiC
	{
		set;
		get;
	}

	public static SoundController soC
	{
		set;
		get;
	}


	public static Cpc.PLayerStatus myState;
	public static Cpc.PLayerStatus _myStateBk;

	private static int _gameTime;
	public static int gameTime
	{
		set
		{
			_gameTime = value;
			netC.SendTimeCount(value);
		}
		get
		{
			return _gameTime;
		}
	}

	private float _secondTime;
	private float _charaTime;
	private float _reviveTime;
	private float _humbTime;
	private float _kumaChek;
	private static Camera _nowCamera;

	public static RoomMode nowRoomMode;

	//ENUMS ###################################################################################
	public enum RoomMode
	{
		ten,
		three
	}

	public enum AppState
	{
		title,
		matching,
		battleStart,
		battle,
		battleEnd,
		battleWin,
		battleLose,
		battleTimeUp,
		battleResult,
		countdown,
		other
	}

	public enum SeType
	{
		attack,
		battle30,
		battleStart,
		catapult,
		click_yes,
		eat,
		heal,
		jump,
		magic,
		pickup,
	}

	public enum Team
	{
		Red,
		Blue,
		NoTeam
	}

	public enum KumaState
	{
		Putting,
		Lifting,
		Sitting,
		Gaining,
		Infested
	}

	public enum JobType
	{
		Knight,
		Sorcerer,
		Priest,
		Bare
	}

	public enum PLayerStatus
	{
		notStart,
		normal,
		kumaCarrying,
		supporting,
		foodCarrying,
		dead
	}


	public enum ActionType
	{
		Idle,
		Run,
		Attack,
		Damaged,
		Dead,
		Eat,
		Jump,
		Weak,
		Joy,
		JoyR,
		DamageR,
		Beath,
		Carry,
		LiftUp,
		Releasing,
		Released,
		Sitting,
		Win,
		Lose,
		ChageJob,
		Canon,
		Diying
	}

	public enum AIType
	{
		attcker,
		carrier,
		cannon,
		stealer,
		circular,
		sorcerer,
		priest,
		carrier_m,
		move,
		stopper
	}

	public enum AIAttackType
	{
		no_type,
		cannon_attaker1,
		cannon_attaker2,
		stealer_attaker,
		circular_attacker
	}
	public enum ColType
	{
		atk,
		atkm,
		rev
	}
	public enum CarryState
	{
		noCarry,
		lifinig,
		myCarry,
		emCarry,
		releasing
	}

	public enum GuideType
	{
		start,
		pickup,
		sitting,
		humburger,
		epickup
	}

	private static Transform _mapTran;
	//METHOD ##################################################################################

	//起動直後
	void OnEnable ()
	{
		isMaster = true;

		netC = _netc;
		netC._cpc = this;

		uiC = _uic;
		soC = _sc;

		_mapTran = GameObject.Find("map").transform;

		myState = PLayerStatus.notStart;
		_myStateBk = myState;

		changeAppState (AppState.title);
	}

	public static void initStatus(Cpc.Team team = Team.Red)
	{
		
		if (Cpc.ONLINEMODE == false)
		{
			myTeam = Team.Red; /* 自チームの切り替え */
			charaNum [(int)myTeam]++;
		}
		else
		{
			myTeam = team; /* 自チームの切り替え */
			charaNum [(int)myTeam]++;
		}

	}

	public static void changeAppState(Cpc.AppState newState, bool isNet=false)
	{
		if (isNet == false)
		{
			if (ONLINEMODE == true)
			{
				netC.SendAppState(newState);
			}
		}

		_stateBk = Cpc.appState;
		Cpc.appState = newState;
		soC.changeBgm ();

	}

	void Update ()
	{
		if (_stateBk != appState)
		{
			if (appState == AppState.battleStart)
			{
				killNum  = new int[] {0 ,0};
				killNumC  = new int[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

				uiC.showKillNum(Cpc.Team.Red);
				uiC.showKillNum(Cpc.Team.Blue);
				uiC.hideOpening();
				_sc.playSe (Cpc.SeType.battleStart);

				myState = PLayerStatus.normal;
				gameTime = Cpc.TIMEMAX;

				if (ONLINEMODE == false)
				{
					var clones = GameObject.FindGameObjectsWithTag ("chara");
					foreach(var ch in clones) {Destroy(ch);}
					clones = GameObject.FindGameObjectsWithTag ("eff");
					foreach(var ch in clones) {Destroy(ch);}
				}
				_uic.initIngameUI(_kumaObj[(int)Cpc.Team.Red].GetComponent<kuma>().getBenchDistance(), _kumaObj[(int)Cpc.Team.Blue].GetComponent<kuma>().getBenchDistance()); 

				//アイコン表示
				_kumaObj[(int)Cpc.Team.Red].GetComponent<kuma>().showIcon();
				_kumaObj[(int)Cpc.Team.Blue].GetComponent<kuma>().showIcon();
				GameObject.Find ("Bench_r").gameObject.transform.FindChild("Canvas").gameObject.SetActive(true);
				GameObject.Find ("Bench_b").gameObject.transform.FindChild("Canvas").gameObject.SetActive(true);

				_charaTime = 0;
				_reviveTime = 0;

				//マスター以外は時間差で作る
				if (isMaster == true)
				{
					_myChara = makeChara(true, myTeam);
					if (ONLINEMODE == true)
					{
						if (PhotonNetwork.room.PlayerCount == 1)
						{
							isCanComPlayer = true;
						}
					}
				}

				makedComR = charaNum[(int)Team.Red];
				makedComB = charaNum[(int)Team.Blue];

				uiC.showGuide(Cpc.GuideType.start);
			}
			_stateBk = appState;
		}

		//バトル中処理
		if (appState == Cpc.AppState.battle || Cpc.appState == Cpc.AppState.countdown)
		{
			//時間差で作る
			if (isMaster == false && isCanMakePlayer == true)
			{
				isCanMakePlayer = false;
				_myChara = makeChara(true, myTeam);
			}

			//キャラ生成関係 ------------------------------------------------------------------------------------
			//Myキャラ復活
			if (myState == Cpc.PLayerStatus.dead)
				//			if (_myChara != null && _myChara.isDead == true)
			{
				if (_reviveTime == 0)
				{
					_uic.showFade ();
				}

				if (_reviveTime > TIME_REVIVE)
				{
					_reviveTime = 0;
					_myChara = makeChara(true, myTeam, false, true);
				}
				else
				{
					_reviveTime += Time.deltaTime;
				}
			}
			//その他キャラ生成
			if (_charaTime > TIME_MAKEC)
			{
				_charaTime = 0;
				//マスターでかつすべてのプレイヤーの生成済みで作成開始
				if (isMaster == true && isCanComPlayer == true)
				{
					StartCoroutine(makeCom());
				}
			}
			else
			{
				_charaTime += Time.deltaTime;
			}
				

			//タイム関係 ------------------------------------------------------------------------------------
			if (_secondTime > 1f)
			{
				_secondTime = 0;
				gameTime--;
				_uic.showGameTime(gameTime);

				if (gameTime == 30)
				{
					playSe (Cpc.SeType.battle30);
					changeAppState (Cpc.AppState.countdown);
				}

				//くまカウント
				if(_kumaObj[(int)Cpc.Team.Red].GetComponent<kuma>().nowState == Cpc.KumaState.Sitting)
				{
					_uic.showCounter(Team.Red);
				}

				if(_kumaObj[(int)Cpc.Team.Blue].GetComponent<kuma>().nowState == Cpc.KumaState.Sitting)
				{
					_uic.showCounter(Team.Blue);
				}

				if (gameTime < 1)
				{
					Cpc.changeAppState(Cpc.AppState.battleTimeUp);
				}
			}
			else
			{
				_secondTime += Time.deltaTime;
			}

			//クマの状態監視 --------------------------------------------------------------------------------
			if (_kumaChek > 1f) {
				_kumaChek = 0f;
				if (_kumaObj [(int)Cpc.Team.Red] != null) {
					if (_kumaObj [(int)Cpc.Team.Red].GetComponent<kuma> ().nowState == Cpc.KumaState.Sitting) {
						if (_uic._fukiRed.activeSelf == false) {
							_uic.showFuki (Team.Red);
							sittingEvent (Team.Red);
						}
					} else {
						_uic.sitCount_r = Cpc.KUMACOUNT;
						if (_uic._fukiRed.activeSelf == true) {
							_uic.hideFuki (Team.Red);
						}
						_uic.showBearIcon (_kumaObj [(int)Cpc.Team.Red].GetComponent<kuma> ().getBenchDistance (), Team.Red);
					}
				}
				if (_kumaObj [(int)Cpc.Team.Blue] != null) {
					if (_kumaObj [(int)Cpc.Team.Blue].GetComponent<kuma> ().nowState == Cpc.KumaState.Sitting) {
						if (_uic._fukiBlue.activeSelf == false) {
							_uic.showFuki (Team.Blue);
							sittingEvent (Team.Blue);
						}
					} else {
						_uic.sitCount_b = Cpc.KUMACOUNT;
						if (_uic._fukiBlue.activeSelf == true) {
							_uic.hideFuki (Team.Blue);
						}
						_uic.showBearIcon (_kumaObj [(int)Cpc.Team.Blue].GetComponent<kuma> ().getBenchDistance (), Team.Blue);
					}
				}
			} else {
				_kumaChek += Time.deltaTime;
			}

			//ハンバーガー生成関係 -------------------------------------------------------------------------------
			if (isMaster == true)
			{
				if (_humbTime > HUMBTIME)
				{
					_humbTime = 0f;
					if (burgerNum [0] < MaxHumburger) {
						makeHamburger(Cpc.Team.Red);
					}

					if (burgerNum [1] < MaxHumburger) {
						makeHamburger(Cpc.Team.Blue);
					}
				}
				else{
					_humbTime += Time.deltaTime;
				}
			}

			//ナビ関係
			if (myState != _myStateBk)
			{
				_myStateBk = myState;
				_uic.changeNaviIcon ();
			}
		}

		if (isReusultShow == false && (Cpc.appState == Cpc.AppState.battleEnd || Cpc.appState == Cpc.AppState.battleTimeUp))
		{
			isReusultShow = true;
			//Static クリア
			clearStatic();

			_uic.showMessage(Cpc.appState);
		}
	}

	private IEnumerator makeCom()
	{
		yield return new WaitForSeconds(0.1f);

		//最初は3倍の速さで作る
		if (Cpc.makedPlayer < Cpc.MaxChara*2) {
			if (makedComR > makedComB) {
				makeChara (false, Team.Blue, true);
			} else {
				makeChara (false, Team.Red, true);
			}
			if (makedComR > makedComB) {
				makeChara (false, Team.Blue, true);
			} else {
				makeChara (false, Team.Red, true);
			}
			if (makedComR > makedComB) {
				makeChara (false, Team.Blue, true);
			} else {
				makeChara (false, Team.Red, true);
			}
		} else {
			makeChara (false, Team.Red, true);
			yield return new WaitForSeconds(0.1f);
			makeChara (false, Team.Blue, true);
		}
	}

	//くま掴むことによるイベント
	public static void pickingEvent(Team te)
	{
		try {
			//一斉にattakerに変更
			if (te == Team.Red) {
				foreach(Transform tf in Cpc.charaPos_b)
				{
					if (tf.gameObject.GetComponent<chara> ()._nowAI != AIType.circular && tf.gameObject.GetComponent<chara> ().carryState == CarryState.noCarry) {
						if (UnityEngine.Random.Range(0,4) == 1)
						{
							tf.gameObject.GetComponent<chara> ().decideAI ((int)Cpc.AIType.attcker);
						}
					}
				}
			} else {
				foreach(Transform tf in Cpc.charaPos_r)
				{
					if (tf.gameObject.GetComponent<chara> ()._nowAI != AIType.circular && tf.gameObject.GetComponent<chara> ().carryState == CarryState.noCarry) {
						if (UnityEngine.Random.Range(0,4) == 1)
						{
							tf.gameObject.GetComponent<chara> ().decideAI ((int)Cpc.AIType.attcker);
						}
					}
				}
			}
		}
		catch(Exception e) {
			Debug.Log ("Error:" + e.ToString());
		}
		/*
		*/
	}

	//くま座ることによるイベント
	private void sittingEvent(Team te)
	{
		try {
			//一斉にstealerに変更
			if (te == Team.Red) {
				foreach(Transform tf in Cpc.charaPos_b)
				{
					if (tf.gameObject.GetComponent<chara> ()._nowAI != AIType.circular && tf.gameObject.GetComponent<chara> ().carryState == CarryState.noCarry) {
						if (UnityEngine.Random.Range(0,2) != 1)
						{
							tf.gameObject.GetComponent<chara> ().decideAI ((int)Cpc.AIType.stealer);
						}
					}
				}
				foreach(Transform tf in Cpc.charaPos_r)
				{
					if (tf.gameObject.GetComponent<chara> ()._nowAI != AIType.circular && tf.gameObject.GetComponent<chara> ().carryState == CarryState.noCarry)
					{
						tf.gameObject.GetComponent<chara> ().decideAI ((int)Cpc.AIType.move);
					}
				}
			} else {
				foreach(Transform tf in Cpc.charaPos_r)
				{
					if (tf.gameObject.GetComponent<chara> ()._nowAI != AIType.circular && tf.gameObject.GetComponent<chara> ().carryState == CarryState.noCarry) {
						if (UnityEngine.Random.Range(0,2) != 1)
						{
							tf.gameObject.GetComponent<chara> ().decideAI ((int)Cpc.AIType.stealer);
						}
					}
				}
				foreach(Transform tf in Cpc.charaPos_b)
				{
					if (tf.gameObject.GetComponent<chara> ()._nowAI != AIType.circular && tf.gameObject.GetComponent<chara> ().carryState == CarryState.noCarry) {
						tf.gameObject.GetComponent<chara> ().decideAI ((int)Cpc.AIType.move);
					}
				}
			}
		}
		catch(Exception e) {
			Debug.Log ("Error:" + e.ToString());
		}
	}

	//キャラ受付
	public static CharaInfo accpetChara(bool isPlayer, Team team, int playerNo=-1)
	{
		string s = "";
		while(s == "")
		{
			s = Cpc.playerName[UnityEngine.Random.Range(0,40)];
			for(int i=0; i<20; i++)
			{
				if (playerRoster[i].name == s)
				{
					s = "";
					break;
				}
			}
		}
		CharaInfo cInfo = new CharaInfo();
		cInfo.name = s;

		//オフライン
		if (playerNo == -1)
		{
			cInfo.init(_playerRosterNo,team,isPlayer);
			cInfo.playerNo = _playerRosterNo;

			playerRoster[_playerRosterNo] = cInfo;
			if (isPlayer == true) {
				myPlayerNo = _playerRosterNo;
			}
			_playerRosterNo++;
		}
		else
		{
			cInfo.init(playerNo,team,isPlayer);
			playerRoster[playerNo] = cInfo;
			if (isPlayer == true) {
				myPlayerNo = playerNo;
			}
			Debug.Log("ON ################ accpetChara ##################:" + playerNo + " | " + cInfo.name + "|" + cInfo.team+ " | " + cInfo.isPlayer + "|" + isPlayer);
		}

		return cInfo;
	}


	/// <summary>
	/// キャラ生成
	/// </summary>
	public static chara makeChara(bool isPlayer, Team team, bool isCom = false, bool reBorn=false)
	{
		


		GameObject instant_object = null;
		chara charaCom = null;

		//キャラ生成
		if (isPlayer == true || charaNum[(int)team] < Cpc.MaxChara)
		{
			CharaInfo cInfo = new CharaInfo();

			//新規追加
			if (reBorn == false && Cpc.makedPlayer < Cpc.MaxChara*2)
			{
				if (Cpc.ONLINEMODE == false)
				{
					cInfo = accpetChara(isPlayer, team);
				}
				else
				{
					//Human・Net Com
					if (isCom == true)
					{
						for(int i=Cpc.makedPlayer; i<20; i++)
						{
							if (playerRoster[i].isPlayer == false && playerRoster[i].team == team)
							{
								cInfo = playerRoster[i];
								cInfo.init(cInfo.playerNo,team,isPlayer);
								cInfo.isCom = true;
								playerRoster[i] = cInfo;
								break;
							}
						}
					}
					else
					{
						cInfo = playerRoster[PhotonNetwork.player.ID-1];
					}
				}
				if (isPlayer == true)
				{
					myState = Cpc.PLayerStatus.normal;
				}
				if (ONLINEMODE == true)
				{
					instant_object = PhotonNetwork.Instantiate("myChara_" + preFix[(int)team], Vector3.zero, Quaternion.identity, 0);
				}
				else
				{
					GameObject pre = (GameObject)Resources.Load ("myChara_" + preFix[(int)team]);
					instant_object = (GameObject) GameObject.Instantiate(pre, new Vector3(0,0,0), Quaternion.identity);
				}

//				instant_object.transform.parent = _mapTran;
				instant_object.transform.SetParent(_mapTran);

				if (isPlayer == true && myTeam == team)
				{
					instant_object.GetComponent<chara>().bornChara(true, team, cInfo);
				}
				else
				{
					instant_object.GetComponent<chara>().bornChara(false, team, cInfo);

					if (team == Cpc.Team.Red) {
						makedComR++;
					} else {
						makedComB++;
					}
				}

				if (isPlayer == false)
				{
					charaNum[(int)team]++;
				}

				if (isPlayer == true) {
					_nowCamera = instant_object.transform.FindChild ("CameraB").transform.FindChild ("Camera").GetComponent<Camera> ();
				}

				//全Charaのオブジェクト格納
				playerObject[cInfo.playerNo] = instant_object;
				charaCom = instant_object.GetComponent<chara>();

//				Cpc.uiC.showName(cInfo, true);
			}
			else
			{
				if (isPlayer == true && _myChara.isDead == true) {
					cInfo = playerRoster[myPlayerNo];
					cInfo.init(cInfo.playerNo,team,isPlayer);
					playerRoster[myPlayerNo] = cInfo;
					_myChara.toAlive(cInfo);
					charaCom = _myChara;
				} else if (isPlayer == false) {
					for(int i=0; i<20; i++)
					{
						if (playerRoster[i].isPlayer == false && playerRoster[i].isDead == true && playerRoster[i].team == team)
						{
							cInfo = playerRoster[i];
							cInfo.init(cInfo.playerNo,team,isPlayer);
							playerRoster[i] = cInfo;

							playerObject[cInfo.playerNo].GetComponent<chara>().toAlive(cInfo);
							charaCom = playerObject[cInfo.playerNo].GetComponent<chara>();
							break;
						}
					}
				}
//				Cpc.uiC.showName(cInfo);
			}


			return charaCom;
		}
		return null;
	}

	public static void makeKuma()
	{
		GameObject instant_r = null;
		GameObject instant_b = null;

		if (ONLINEMODE == true)
		{
			instant_r = PhotonNetwork.Instantiate ("Kuma_r", Vector3.zero, Quaternion.identity, 0);
			instant_b = PhotonNetwork.Instantiate ("Kuma_b", Vector3.zero, Quaternion.identity, 0);
		}
		else
		{
			GameObject pre = (GameObject)Resources.Load ("Kuma_r");
			instant_r = (GameObject) GameObject.Instantiate(pre, new Vector3(0,0,0), Quaternion.identity);
			pre = (GameObject)Resources.Load ("Kuma_b");
			instant_b = (GameObject) GameObject.Instantiate(pre, new Vector3(0,0,0), Quaternion.identity);

		}

		instant_r.gameObject.GetComponent<kuma> ().init ();
		instant_b.gameObject.GetComponent<kuma> ().init ();

		_kumaObj = new GameObject[2];
		_kumaObj [(int)Cpc.Team.Red] = instant_r.gameObject;
		_kumaObj [(int)Cpc.Team.Blue] = instant_b.gameObject;
	}

	public void makeKumaByNet()
	{
		StartCoroutine (makingKuma());
	}

	private IEnumerator makingKuma()
	{
		int i = 0;
		GameObject instant_r = null;
		GameObject instant_b = null;

		while(i == 0)
		{
			instant_r = GameObject.Find ("Kuma_r(Clone)");
			instant_b = GameObject.Find ("Kuma_b(Clone)");

			if (instant_r != null && instant_b != null) {
				break;
			}

			yield return new WaitForSeconds(0.01f);
		}
		instant_r.gameObject.GetComponent<kuma> ().init ();
		instant_b.gameObject.GetComponent<kuma> ().init ();

		_kumaObj = new GameObject[2];
		_kumaObj [(int)Cpc.Team.Red] = instant_r.gameObject;
		_kumaObj [(int)Cpc.Team.Blue] = instant_b.gameObject;
	}

	public static void enableCamera()
	{
		if (_nowCamera != null) {
			_nowCamera.enabled = true;
		}
	}

	public static void disableCamera()
	{
		if (_nowCamera != null) {
			_nowCamera.enabled = false;
		}
	}

	public static void actChara(ActionType type, Vector3 pos)
	{
		if (_myChara == null) return;

		switch(type)
		{
		case ActionType.Idle:
			_myChara.GetComponent<chara>().idle();
			break;
		case ActionType.Run:
			_myChara.GetComponent<chara>().move(pos);
			break;
		case ActionType.Jump:
			_myChara.GetComponent<chara>().jump(pos);
			break;
		case ActionType.Attack:
			_myChara.GetComponent<chara>().attack();
			break;
		}
	}

	public void makeHamburger(Cpc.Team team)
	{
		GameObject instant_object;
		if (ONLINEMODE == true)
		{
			if (team == Team.Red) {
				instant_object = PhotonNetwork.Instantiate ("hamburger_r", Vector3.zero, Quaternion.identity, 0);
			} else {
				instant_object = PhotonNetwork.Instantiate ("hamburger_b", Vector3.zero, Quaternion.identity, 0);
			}
		}
		else
		{
			if (team == Team.Red) {
				GameObject pre = (GameObject)Resources.Load ("hamburger_r");
				instant_object =  (GameObject)GameObject.Instantiate(pre, new Vector3(0,0,0), Quaternion.identity);
			} else {
				GameObject pre = (GameObject)Resources.Load ("hamburger_b");
				instant_object =  (GameObject)GameObject.Instantiate(pre, new Vector3(0,0,0), Quaternion.identity);
			}
		}

		instant_object.transform.parent = _mapTran;
		if (team == Cpc.Team.Red)
		{
			instant_object.transform.localPosition = new Vector3(-64f+UnityEngine.Random.Range(-2.0f,2.0f),3f,38f+UnityEngine.Random.Range(-2.0f,2.0f));
		}
		else
		{
			instant_object.transform.localPosition = new Vector3(-82f+UnityEngine.Random.Range(-2.0f,2.0f),3f,10f+UnityEngine.Random.Range(-2.0f,2.0f));
		}
		instant_object.GetComponent<hamburger>().myteam = team;
		burgerNum[(int)team]++;
	}

	public static void playSe(Cpc.SeType type)
	{
		soC.playSe(type);
	}

	public GameObject getKumaObj(Cpc.Team team, bool isMyteam)
	{
		if (isMyteam == true)
		{
			return _kumaObj [(int)team];
		}
		else
		{
			if (team == Cpc.Team.Red)
			{
				return _kumaObj [(int)Cpc.Team.Blue];
			}
			else
			{
				return _kumaObj [(int)Cpc.Team.Blue];
			}
		}
	}

	//[中継]
	public void changeOnlineIcon(int playerNo, string name)
	{
		_uic.changeOnlineIcon(playerNo, name);
	}

	//static領域クリア
	public static void clearStatic()
	{
		charaNum = new int[] {0 ,0};
		burgerNum = new int[] {0 ,0};

		playerRoster = new CharaInfo[20];
		playerObject = new GameObject[20];
		AICounter_r = new int[] {0,0,0,0,0,0,0,0,0,0};
		AICounter_b = new int[] {0,0,0,0,0,0,0,0,0,0};
		isGuideShow = new bool[] {false, false, false, false, false};
		_playerRosterNo = 0;
		makedPlayer = 0;
		makedComR = 0;
		makedComB = 0;

		isCanMakePlayer = true;
		isCanComPlayer = true;
	}


	//STRUCTS ###################################################################################
	public struct CharaInfo
	{
		public int playerNo;
		public bool isPlayer;
		public string name;
		public JobType nowJob;
		public int nowHp;
		public int nowMaxHp;
		public Team team;
		public bool isDead;
		public int viewId;
		public bool isCom;

		public void init (int no, Team charaTeam, bool isP=true)
		{
			playerNo = no;
			isPlayer = isP;
			team = charaTeam;
			jobChange(JobType.Knight);
			isDead = false;
			viewId = -1;
			isCom = false;
		}

		public void jobChange (JobType type)
		{
			nowJob = type;
			nowHp = JobHP [(int)type];
			nowMaxHp = nowHp;
		}

		public int attack ()
		{
			return Atk [(int)nowJob];
		}

		public bool damaged (int damage)
		{
			nowHp -= damage;

			//toDead
			if (nowHp <= 0)
			{
				nowHp = 0;
				isDead = true;
				return true;
			}
			else
			{
				if (nowHp > nowMaxHp) 
				{
					nowHp = nowMaxHp;
				}
				return false;
			}
		}

		public string toString()
		{
			string s = "";

			s += playerNo+"|";
			s += "0|";  //他者に送る時はfalse固定
			s += name+"|";
			s += (int)nowJob+"|";
			s += nowHp+"|";
			s += nowMaxHp+"|";
			s += (int)team+"|";
			if (isDead == true)
			{
				s += "1|";
			}
			else
			{
				s += "0|";
			}
			s += viewId+"|";
			if (isCom == true)
			{
				s += "1|";
			}
			else
			{
				s += "0|";
			}
			return s;
		}

		public void toCharaInfo(string s)
		{
			string[] st = s.Split('|');

			playerNo = int.Parse(st[0]);

			if (st[1] == "1")
			{
				isPlayer = true;
			}
			else
			{
				isPlayer = false;
			}
			name = st[2];
			nowJob = (JobType)int.Parse(st[3]);
			nowHp = int.Parse(st[4]);
			nowMaxHp = int.Parse(st[5]);
			team = (Team)int.Parse(st[6]);
			if (st[7] == "1")
			{
				isDead = true;
			}
			else
			{
				isDead = false;
			}
			viewId = int.Parse(st[8]);
			if (st[9] == "1")
			{
				isCom = true;
			}
			else
			{
				isCom = false;
			}
		}
	}
}
