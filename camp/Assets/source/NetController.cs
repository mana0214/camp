using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetController : Photon.MonoBehaviour {

	private System.Action<bool> _joinedLobyCallback; // errorなら引数がtrue
	private System.Action<bool> _joinedRoomCallback; // errorなら引数がtrue
	public Cpc _cpc;

	//ルーム一覧が取れると
	void OnReceivedRoomListUpdate()
	{
		string roomName = "";

		//ルーム一覧を取る
		RoomInfo[] rooms = PhotonNetwork.GetRoomList();
		if (rooms.Length == 0) {
			if (Cpc.nowRoomMode == Cpc.RoomMode.ten)
			{
				roomName = "TEN:0";
			}
			else
			{
				roomName = "THREE:0";
			}
		} else {
			int playerCount = 0;

			//ルームが1件以上ある時ループでRoomInfo情報をログ出力
			for (int i = 0; i < rooms.Length; i++) {
				playerCount += rooms [i].PlayerCount;

				if (playerCount > 19)
				{
					roomName = "MAX";
					break;
				}

				if (Cpc.nowRoomMode == Cpc.RoomMode.ten)
				{
					if (rooms [i].Name.StartsWith("TEN")) {
						if ( rooms[i].IsOpen == true) {
							roomName = rooms [i].Name;
							break;
						}
						else
						{
							roomName = "TEN:" + (i+1);
							break;
						}
					}
				}
				else
				{
					if (rooms [i].Name.StartsWith("THREE")) {
						if ( rooms[i].IsOpen == true && rooms[i].PlayerCount < 6) {
							roomName = rooms [i].Name;
							break;
						}
						else
						{
							roomName = "THREE:" + (i+1);
							break;
						}
					}
				}
			}
		}
		if (roomName != "MAX")
		{
			PhotonNetwork.SetPlayerCustomProperties(null); // ルームに入るごとに初期化
//			_joinedRoomCallback = joinedRoomCallback;

			RoomOptions roomOptions = new RoomOptions() { MaxPlayers = 20 };

			if (Cpc.nowRoomMode == Cpc.RoomMode.three)
			{
				roomOptions = new RoomOptions() { MaxPlayers = 6 };
			}
				
			PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);

			StartCoroutine(doStartGame());
		}
		else
		{
			Cpc.uiC.onClickOffline();
		}
	}

	private IEnumerator doStartGame()
	{
		yield return new WaitForSeconds(2.0f);

		if (PhotonNetwork.room.PlayerCount > 0)
		{
			Cpc.changeAppState(Cpc.AppState.matching);
			StartCoroutine(Cpc.uiC.startOpening ());

			//アイコン表示
			Cpc.uiC.showOnlineIcon();
		}
		else
		{
			Cpc.uiC.showErrorMsg ("NetWork is Busy!!!!");
		}

	}

	public void JoinLobby(System.Action<bool> joinedLobbyCallback = null)
	{
		_joinedLobyCallback = joinedLobbyCallback;

		byte Version = 1;
		PhotonNetwork.autoJoinLobby = true;
		PhotonNetwork.ConnectUsingSettings(Version + "." + SceneManagerHelper.ActiveSceneBuildIndex);

	}

	public virtual void OnJoinedLobby()
	{
		if (_joinedLobyCallback != null)
		{
			_joinedLobyCallback(false);
		}
	}

	public virtual void JoinRoom(string roomName, System.Action<bool> joinedRoomCallback = null)
	{
	}

	public virtual void OnPhotonJoinRoomFailed()
	{
		_joinedRoomCallback(true);
	}

	public virtual void OnPhotonCreateRoomFailed()
	{
		_joinedRoomCallback(true);
	}

	public virtual void OnDisconnectedFromPhoton()
	{
		Cpc.uiC.showErrorMsg ("NetWork Error!!!");
	}

	public virtual void OnJoinedRoom() {
		if (_joinedRoomCallback != null)
		{
			_joinedRoomCallback(false);
		}

		AcceptUser();

	}

	/// <summary>
	/// 他のユーザーのルーム退室時
	/// </summary>
	/// <param name="otherPlayer">Other player.</param>
	public virtual void OnPhotonPlayerDisconnected (PhotonPlayer otherPlayer)
	{
		Cpc.uiC.showErrorMsg ("NetWork Error!!!");
	}

	/// <summary>
	/// 他ユーザーがルームに接続した時
	/// </summary>
	/// <param name="newPlayer">New player.</param>
	public virtual void OnPhotonPlayerConnected (PhotonPlayer newPlayer)
	{
		// 入室ログ表示
		if (Cpc.isMaster == true)
		{
			if ((newPlayer.ID % 2) == 1)
			{
				Cpc.charaNum[(int)Cpc.Team.Red]++;
			}
			else
			{
				Cpc.charaNum[(int)Cpc.Team.Blue]++;
			}
		}

		//自分の情報を送信する
		Cpc.mycharaInfo.viewId = 0;
		SendPlayerInfo(Cpc.mycharaInfo.toString());
	}

	//Send Controll-----------------

	//TimeCount
	public void SendTimeCount(int count)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (Cpc.isMaster == false) return;

		photonView.RPC("TimeCount", PhotonTargets.Others, new object[]{(int)count});
	}
	[PunRPC]
	private void TimeCount(int newCount)
	{
		Cpc.gameTime = newCount;
	}

	//GameState
	public void SendAppState(Cpc.AppState state, int forceSend=-1)
	{
		if (PhotonNetwork.player.ID != 1 && forceSend == -1) return;

		photonView.RPC("AcceptAppState", PhotonTargets.Others, new object[]{(string)(forceSend + "|" + (int)state+"")});
	}
	[PunRPC]
	private void AcceptAppState(string sInfo)
	{
		string[] st = sInfo.Split('|');

		//強制でない場合は、マスターは無視
		if (st[0] == "-1")
		{
			if (PhotonNetwork.player.ID == 1) return;
		}
		//強制の場合は、送信者は無視
		if (st[0] != "-1")
		{
			if (int.Parse(st[0]) == PhotonNetwork.player.ID) return;
		}

		Cpc.changeAppState((Cpc.AppState)int.Parse(st[1]), true);
	}

	//KumaCount
	public void SendKumaCount(int count, Cpc.Team team)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (Cpc.isMaster == false) return;

		photonView.RPC("AcceptKumaCount", PhotonTargets.Others, new object[]{(string)(count + "|" + (int)team)});
	}
	[PunRPC]
	private void AcceptKumaCount(string sInfo)
	{
		if (Cpc.isMaster == true) return;

		string[] st = sInfo.Split('|');

		Cpc.uiC.setKumaCount(int.Parse(st[0]), (Cpc.Team)int.Parse(st[1]));
	}


	//PlayerInfo
	public void SendPlayerInfo(string sInfo)
	{
		if (Cpc.ONLINEMODE == false) return;
		photonView.RPC("PlayerInfo", PhotonTargets.Others, new object[]{(string)sInfo});
	}
	[PunRPC]
	private void PlayerInfo(string sInfo)
	{
		Cpc.CharaInfo cInfo = new Cpc.CharaInfo();
		cInfo.toCharaInfo(sInfo);

		if (cInfo.playerNo == Cpc.mycharaInfo.playerNo) {
			Debug.Log("自分だから破棄");
			return;
		}

		Cpc.playerRoster[cInfo.playerNo] = cInfo;
		_cpc.changeOnlineIcon(cInfo.playerNo, cInfo.name);
	}

	//Player生成許可送信
	public void SendMakedPlayer(int playerId)
	{
		if (Cpc.ONLINEMODE == false) return;

		photonView.RPC("AcceptMakedPlayer", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + playerId)});
	}
	[PunRPC]
	private void AcceptMakedPlayer(string sInfo)
	{
		string[] st = sInfo.Split('|');

		//自分のみ受付
		if (int.Parse(st[1]) == PhotonNetwork.player.ID)
		{
			Cpc.isCanMakePlayer = true;
		}
		else if (int.Parse(st[1]) == -1)
		{
			if (Cpc.isMaster == true)
			{
				Cpc.isCanComPlayer = true;
			}
		}
	}

	//COM情報送信
	public void SendComInfo(string sInfo)
	{
		if (Cpc.ONLINEMODE == false) return;
		photonView.RPC("ComInfo", PhotonTargets.Others, new object[]{(string)sInfo});
	}
	[PunRPC]
	private void ComInfo(string sInfo)
	{
		if (Cpc.isMaster == true) return;

		Cpc.CharaInfo cInfo = new Cpc.CharaInfo();
		cInfo.toCharaInfo(sInfo);

		Cpc.playerRoster[cInfo.playerNo] = cInfo;
		if (Cpc.playerObject[cInfo.playerNo] != null)
		{
			Cpc.playerObject[cInfo.playerNo].GetComponent<chara>()._charaInfo = cInfo;
		}
	}

	//被ダメージ
	public void SendDamaged(int playerNo, int attakedPlayerNo, int Atk)
	{
		if (Cpc.ONLINEMODE == false) return;

		//自分自身の被ダメージは送らない
//		if (playerNo == PhotonNetwork.player.ID-1) return;

		photonView.RPC("Damaged", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + playerNo+ "|" + attakedPlayerNo + "|"+Atk)});
	}
	[PunRPC]
	private void Damaged(string sInfo)
	{
		string[] st = sInfo.Split('|');
		Cpc.playerObject[int.Parse(st[1])].GetComponent<chara>().damaged(int.Parse(st[2]), int.Parse(st[3]), true);
	}

	//アニメーション
	public void SendAnime(int playerNo, int animeNo, bool isOnece = false)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (isOnece == true)
		{
			photonView.RPC("DoAnime", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + playerNo+"|"+animeNo+"|1")});
		}
		else
		{
			photonView.RPC("DoAnime", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + playerNo+"|"+animeNo+"|0")});
		}
	}
	[PunRPC]
	private void DoAnime(string sInfo)
	{
		string[] st = sInfo.Split('|');

		//自分自身以外に関するものだけ受付(すべて強制)
		if (int.Parse(st[0]) != PhotonNetwork.player.ID && Cpc.playerObject[int.Parse(st[1])] != null)
		{
			Cpc.playerObject[int.Parse(st[1])].GetComponent<chara>().playAnime(true, int.Parse(st[2]));
		}
	}

	//エフェクト
	public void SendEffect(string effName, Vector3 position, float rotX, float rotY, float rotZ, bool isDestory, float delaySec)
	{
		if (Cpc.ONLINEMODE == false) return;

		int isD = 0;
		if (isDestory == true)
		{
			isD = 1;
		}

		photonView.RPC("DoEffect", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + effName+"|"+position.x+"|"+position.y+"|"+position.z+"|"+rotX+"|"+rotY+"|"+rotZ+"|"+isD+"|"+delaySec)});
	}
	[PunRPC]
	private void DoEffect(string sInfo)
	{
		string[] st = sInfo.Split('|');

		if (int.Parse(st[0]) != PhotonNetwork.player.ID)
		{
			Util.playEffByNet(sInfo);
		}
	}

	//Kuma状態
	public void SendKumaState(Cpc.KumaState state, Cpc.Team team)
	{
		if (Cpc.ONLINEMODE == false) return;
		photonView.RPC("accpetKumaState", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + (int)state + "|" + (int)team)});
	}
	[PunRPC]
	private void accpetKumaState(string sInfo)
	{
		string[] st = sInfo.Split('|');

		//自分自身以外に関するものだけ受付(すべて強制)
		if (int.Parse(st[0]) != PhotonNetwork.player.ID)
		{
			Cpc._kumaObj[int.Parse(st[2])].GetComponent<kuma>().changeState((Cpc.KumaState)int.Parse(st[1]), true);
		}
	}

	//KumaのPosition
	public void SendKumaPos(Vector3 pos, Cpc.Team team)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (Cpc.isMaster == true) return;

		photonView.RPC("accpetKumaPos", PhotonTargets.MasterClient, new object[]{(string)(PhotonNetwork.player.ID + "|" + pos.x + "|" + pos.y + "|" + pos.z + "|" + (int)team)});
	}
	[PunRPC]
	private void accpetKumaPos(string sInfo)
	{
		string[] st = sInfo.Split('|');

		if (st.Length < 4) return;

		//マスターのみ受信
		if (Cpc.isMaster == true)
		{
			Cpc._kumaObj[int.Parse(st[4])].transform.localPosition =  new Vector3(float.Parse(st[1]), float.Parse(st[2]), float.Parse(st[3]));
		}
	}

	//KumaのWeight
	public void SendKumaWeight(int weight, Cpc.Team team)
	{
		if (Cpc.ONLINEMODE == false) return;
		photonView.RPC("accpetKumaWeight", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + weight + "|" + (int)team)});
	}
	[PunRPC]
	private void accpetKumaWeight(string sInfo)
	{
		string[] st = sInfo.Split('|');

		//自分自身以外に関するものだけ受付(すべて強制)
		if (int.Parse(st[0]) != PhotonNetwork.player.ID)
		{
			Cpc._kumaObj[int.Parse(st[2])].GetComponent<kuma>().acceptWeight(int.Parse(st[1]));
		}
	}

	//HamburgerのPosition
	public void SendHamPos(Vector3 pos, int ViewId)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (Cpc.isMaster == true) return;

		photonView.RPC("accpetHamPos", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + ViewId + "|" + pos.x + "|" + pos.y + "|" + pos.z)});
	}
	[PunRPC]
	private void accpetHamPos(string sInfo)
	{
		if (PhotonNetwork.player.ID != 1) return;

		string[] st = sInfo.Split('|');

		if (st.Length < 4) return;

		GameObject[] obj = GameObject.FindGameObjectsWithTag("hamburger");
		for (int i=0; i < obj.Length; i++)
		{
			if (obj[i].GetComponent<PhotonView>().viewID == int.Parse(st[1]))
			{
				obj[i].transform.position = new Vector3(float.Parse(st[2]), float.Parse(st[3]), float.Parse(st[4]));
				break;
			}
		}
	}

	//HamburgerのDestroy
	public void SendHamDestroy(int ViewId)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (Cpc.isMaster == true) return;

		photonView.RPC("accpetHamDestroy", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + ViewId)});
	}
	[PunRPC]
	private void accpetHamDestroy(string sInfo)
	{
		if (PhotonNetwork.player.ID != 1) return;

		string[] st = sInfo.Split('|');

		if (st.Length < 1) return;

		//自分自身以外に関するものだけ受付
		GameObject[] obj = GameObject.FindGameObjectsWithTag("hamburger");
		for (int i=0; i < obj.Length; i++)
		{
			if (obj[i].GetComponent<PhotonView>().viewID == int.Parse(st[1]))
			{
				Destroy(obj[i].gameObject);
				Cpc.burgerNum[(int)(obj[i].GetComponent<hamburger>().myteam)]--;

				break;
			}
		}
	}

	//COMキャラの復活(マスターのみ送信)
	public void SendComRestore(int playerNo, int ViewId)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (Cpc.isMaster == false) return;

//		Debug.Log("COMキャラの復活 : [" + (string)(playerNo + "|" + ViewId) + "]");

		photonView.RPC("accpetComRestore", PhotonTargets.Others, new object[]{(string)(playerNo + "|" + ViewId)});
	}
	//(マスター以外のみ受信)
	[PunRPC]
	private void accpetComRestore(string sInfo)
	{
//		Debug.Log("COMキャラの復活 受信? : [" + sInfo + "]");
		if (Cpc.isMaster == true) return;

//		Debug.Log("COMキャラの復活 受信 : [" + sInfo + "]");
//		Debug.Log("(PhotonNetwork.player.ID-1): [" + (PhotonNetwork.player.ID-1) + "]");

		string[] st = sInfo.Split('|');

		if (st.Length < 1) return;

		//自分自身の情報は捨てる
		if ((PhotonNetwork.player.ID-1) == int.Parse(st[0])) return;

		GameObject[] obj = GameObject.FindGameObjectsWithTag("chara");

		for (int i=0; i < obj.Length; i++)
		{
			if (obj[i].GetComponent<PhotonView>().viewID == int.Parse(st[1]))
			{
				obj[i].GetComponent<chara>().toAliveByNet();
				break;
			}
		}
	}
	//kill数送信
	public void SendKillNum(Cpc.Team team)
	{
		if (Cpc.ONLINEMODE == false) return;
		if (Cpc.isMaster == false) return;

//		Debug.Log("kill数送信 : [" + PhotonNetwork.player.ID + "|" + team + "|" + Cpc.killNum[(int)team] + "]");

		photonView.RPC("accpetKillNum", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + (int)team + "|" + Cpc.killNum[(int)team])});
	}

	//(マスター以外のみ受信)
	[PunRPC]
	private void accpetKillNum(string sInfo)
	{
//		Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@ kill数 受信 : [" + sInfo + "]");

//		Debug.Log("kill数 受信 OK : [" + sInfo + "]");

		string[] st = sInfo.Split('|');

		if (st.Length < 2) return;

		Cpc.killNum [int.Parse (st[1])] = int.Parse (st[2]);
		Cpc.uiC.showKillNum((Cpc.Team)int.Parse (st[1]));
	}

	//killC数送信
	public void SendKillNumC(int atkPlayer)
	{
		if (Cpc.ONLINEMODE == false) return;

//		Debug.Log("kill C 数送信 : [" + atkPlayer+ "|" + Cpc.killNumC[atkPlayer] + "]");

		photonView.RPC("accpetKillNumC", PhotonTargets.Others, new object[]{(string)(atkPlayer + "|" +  Cpc.killNumC[atkPlayer])});
	}

	//(マスター以外のみ受信)
	[PunRPC]
	private void accpetKillNumC(string sInfo)
	{
//		Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@ kill C数 受信 : [" + sInfo + "]");


		string[] st = sInfo.Split('|');

		if (st.Length < 2) return;
//		Debug.Log("kill数 C 受信 OK : [" + sInfo + "]");

		Cpc.killNumC[int.Parse (st[0])] = int.Parse (st[1]);
	}

	//Magic Bound
	public void SendMagic(int playerNo)
	{
		if (Cpc.ONLINEMODE == false) return;
		photonView.RPC("accpetMagic", PhotonTargets.Others, new object[]{(string)(PhotonNetwork.player.ID + "|" + playerNo + "|" + 1 + "|")});
	}
	[PunRPC]
	private void accpetMagic(string sInfo)
	{
		string[] st = sInfo.Split('|');

		Cpc.playerObject[int.Parse(st[1])].GetComponent<chara>().magicBound(true);
	}

	//オンラインユーザー受付
	public void AcceptUser()
	{
		if (PhotonNetwork.player.ID == 1)
		{
			Cpc.isMaster = true;
			Cpc.isCanMakePlayer = true;
			Cpc.isCanComPlayer = false;
			Cpc.makeKuma ();
		}
		else
		{
			Cpc.isCanMakePlayer = false;
			Cpc.isMaster = false;
			_cpc.makeKumaByNet ();
		}

		Cpc.CharaInfo cInfo = new Cpc.CharaInfo();

		if (PhotonNetwork.player.ID % 2 == 1)
		{
			cInfo = Cpc.accpetChara(true,Cpc.Team.Red,  PhotonNetwork.player.ID-1);
			Cpc.initStatus(Cpc.Team.Red);
		}
		else
		{
			cInfo = Cpc.accpetChara(true,Cpc.Team.Blue,  PhotonNetwork.player.ID-1);
			Cpc.initStatus(Cpc.Team.Blue);
		}
		cInfo.playerNo = PhotonNetwork.player.ID-1;

		Cpc.mycharaInfo = cInfo;
		cInfo.viewId = 0;
		SendPlayerInfo(cInfo.toString());

		_cpc.changeOnlineIcon(PhotonNetwork.player.ID-1, cInfo.name);

	}

	//COM名簿作成
	public void MakeComList()
	{
		if (Cpc.isMaster == false) return;

		Cpc.CharaInfo cInfo;

		int i=PhotonNetwork.room.PlayerCount;

		if (i % 2 == 1)
		{
			cInfo = Cpc.accpetChara(false, Cpc.Team.Blue, i);
			cInfo.isCom = true;
			SendComInfo (cInfo.toString());
			i++;
		}


		for (; i<Cpc.MaxChara*2-1;)
		{
			cInfo = Cpc.accpetChara(false, Cpc.Team.Red, i);
			cInfo.isCom = true;
			SendComInfo (cInfo.toString());
			i++;
			cInfo = Cpc.accpetChara(false, Cpc.Team.Blue, i);
			cInfo.isCom = true;
			SendComInfo (cInfo.toString());
			i++;
		}
	}

}
