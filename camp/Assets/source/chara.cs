﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class chara : MonoBehaviour
{
	public Cpc.Team myTeam;
	public Cpc.JobType myJob;

	[SerializeField]
	private charaUI _cui;
	[SerializeField]
	private RectTransform[] _cuiRect;

	[SerializeField]
	private GameObject _myAvator;
	[SerializeField]
	private Animation _nowAnime;
	[SerializeField]
	private AnimationClip[] _animeClips;
	[SerializeField]
	private Collider _atkCollider;
	[SerializeField]
	private Collider _mgcCollider;
	[SerializeField]
	private Collider _rcvCollider;
	[SerializeField]
	private Transform _attack_col;
	[SerializeField]
	private Transform _arrow;
	[SerializeField]
	private GameObject[] _shadow;

	//エフェクト用
	[SerializeField]
	private SkinnedMeshRenderer _mySkin;

	//プレイヤーフラグ
	public bool isPlayer
	{
		set;
		get;
	}
	public bool isNetPlayer = false;

	//ステータス
	public Cpc.ActionType nowActionType
	{
		set;
		get;
	}
	public bool isDead
	{
		set;
		get;
	}

	private Vector3 _cuiBk;
	private Cpc _cpc;
	private Cpc.ActionType _bkActionType;
	public Cpc.CharaInfo _charaInfo;

	private float leftWeight = 1f;
	private float _nowWeight = 1f;

	private float _nowPosX = 0f;
	private float _nowPosY = -0.9f;
	private float _nowPosYBk = -0.9f;
	private float _nowPosZ = 0f;

	private bool isJumped = false;
	private float jumpX = 0f;
	private float jumpZ = 0f;
	private float _timeCheck = 1f;
	private float _timedelta = 0f;
	private float _timeBodyCheck = 0f;
	private float _kumaCheck = 0.02f;
	private float _kumadelta = 0f;
	private float _humCheck = 0.02f;
	private float _humdelta = 0f;
	private float _rcvCheck = 1f;
	private float _rcvdelta = 0f;
	private GameObject _carryKuma;
	private GameObject _carryKumaEm;
	private bool _inKuma; //KUMAが近くいる
	private string _name;

	private Transform _charag;
	private Transform _uiCamera;
	private GameObject _eff_support;
	private int _nowSupportNum;
	private bool isDamaging = false;
	private bool isRecovering = false;
	private bool isRecoverEffect = false;
	public Cpc.CarryState carryState = Cpc.CarryState.noCarry;

	//COM関連
	public UnityEngine.AI.NavMeshAgent _agent;
	private Vector2 _myPos;
	private float _timeSearch = 0f;
	private float _timeSpeedCk = 0f;
	private float _timeAtkNext = 0f;
	private float _timeAttakerTimer = 0f;
	private float _timePosCk = 0f;
	private float _checkTime = 0f;
	public Cpc.AIType _nowAI;
	private Cpc.AIAttackType _nowAttackerType;
	private bool isLiftAI = false;
	public bool isShrinkage = false; //ダメージなどのすくみ
	private GameObject _myKuma;
	private bool isHumCarry = false;
	private float _timeChangeAI = 0f;
	private int _aiState = 0;
	private Vector3 _comPosBk = Vector3.zero;

	private bool isReadyAgent = false;

	private GameObject burgerObj;
	private GameObject burgerObj_t;

	private bool isInWater = false;
	private float _timewtCheck = 0f;

	private bool isMagicAttacking = false;
	private bool isMagicWait = false;
	private bool isMagicBound = false;

	void OnEnable ()
	{
		//ネットを介して生成
		if (Cpc.ONLINEMODE == true)
		{
			Debug.Log("### CHARA : " + Cpc.playerRoster[Cpc.makedPlayer].name + " : " + Cpc.playerRoster[Cpc.makedPlayer].team + " : " + this.GetComponent<PhotonView>().viewID);

			int id = this.gameObject.GetComponent<PhotonView> ().ownerId;

			if (PhotonNetwork.player.ID != id) {
				bornChara (false, Cpc.playerRoster[Cpc.makedPlayer].team, Cpc.playerRoster[Cpc.makedPlayer]);

				Cpc.playerObject[Cpc.makedPlayer] = this.gameObject;

				//ナビを非表示
				if (Cpc.playerRoster[Cpc.makedPlayer].team == Cpc.Team.Red)
				{
					this.transform.FindChild("navi_r").gameObject.SetActive(false);
				}
				else
				{
					this.transform.FindChild("navi_b").gameObject.SetActive(false);
				}
			}
			else
			{
				//COM生成までは許可を出す
				if (Cpc.makedPlayer+1 < PhotonNetwork.room.PlayerCount)
				{
					Cpc.netC.SendMakedPlayer(PhotonNetwork.player.ID+1);
				}
				//最後にマスターに許可を返す
				else if (Cpc.makedPlayer+1 == PhotonNetwork.room.PlayerCount)
				{
					Cpc.netC.SendMakedPlayer(-1);
				}
			}
		}

		Cpc.uiC.showName(Cpc.playerRoster[Cpc.makedPlayer], true);

		Cpc.makedPlayer++;
	}

	public void bornChara(bool isplayer, Cpc.Team team, Cpc.CharaInfo info)
	{
		_charaInfo = info;
		_charaInfo.isPlayer = isplayer;

		myJob = Cpc.JobType.Knight;
		myTeam = team;
		_cpc = GameObject.Find("Cpc").gameObject.GetComponent<Cpc>();
		_charag = GameObject.Find("charag").gameObject.transform;
		_uiCamera = GameObject.Find("UICamera").gameObject.transform;
		_eff_support = this.gameObject.transform.FindChild("eff_support").gameObject;

		_myKuma = _cpc.getKumaObj (myTeam, true);
		_eff_support.SetActive(false);

		isPlayer = isplayer;
		isNetPlayer = false;

		//[ネット対応]
		if (Cpc.ONLINEMODE == true) {
			if (PhotonNetwork.player.ID != this.gameObject.GetComponent<PhotonView> ().ownerId) {
				isPlayer = false;
				isNetPlayer = true;
			}
		}

		if (isPlayer != true && isNetPlayer != true)
		{
			StartCoroutine (setAgent());
		}

		if (myTeam == Cpc.Team.Red)
		{
			_carryKuma = GameObject.Find("Kuma_r(Clone)").gameObject;
			_carryKumaEm = GameObject.Find("Kuma_b(Clone)").gameObject;
		}
		else
		{
			_carryKuma = GameObject.Find("Kuma_b(Clone)").gameObject;
			_carryKumaEm = GameObject.Find("Kuma_r(Clone)").gameObject;
		}

		_cuiBk = _cuiRect[0].transform.localPosition;
		if (isPlayer == true)
		{
			StartCoroutine(arrowAnime());
		}

		if (Cpc.isMaster == true)
		{
			if (isPlayer == false && isNetPlayer == false)
			{
				_cuiBk = new Vector3(_cuiBk.x, _cuiBk.y+1.5f, _cuiBk.z);
			}
		}
		else
		{
			if (_charaInfo.isCom == true)
			{
				_cuiBk = new Vector3(_cuiBk.x, _cuiBk.y+1.5f, _cuiBk.z);
			}
		}

		init();
	}

	private IEnumerator arrowAnime()
	{
		int i=0;
		int j=0;

		while(i ==0)
		{
			if (j < 10)
			{
				_arrow.transform.localPosition = new Vector3(_arrow.transform.localPosition.x, _arrow.transform.localPosition.y, 1.884f + 0.04f*j);
			}
			else if (j < 20)
			{
				_arrow.transform.localPosition = new Vector3(_arrow.transform.localPosition.x, _arrow.transform.localPosition.y, 1.884f + 0.04f*10 - 0.04f*(j-10));
			}

			if (j == 20)
			{
				j=0;
			}
			j++;
			yield return new WaitForSeconds(0.01f);
		}

		yield return null;
	}

	private IEnumerator setAgent()
	{
		yield return new WaitForSeconds (1.5f);

		//NaviAgent
		_agent = this.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
		_agent.enabled = true;
		_agent.speed = Cpc.COM_SPD_MAX / leftWeight; 

		this.gameObject.GetComponent<CharacterController>().enabled = false;
		_myAvator.transform.localPosition = new Vector3(0, 0.22f, 0);

		decideAI ();

		if (_cuiRect [0].gameObject.activeSelf == false) {
			_cuiRect [0].gameObject.SetActive(true);
		}
		if (_cuiRect [1].gameObject.activeSelf == false) {
			_cuiRect [1].gameObject.SetActive(true);
		}

		isReadyAgent = true;
		yield return null;
	}

	public void decideAI(int toJob = -1)
	{
		/*
		if (toJob == -1)
		{
			initAIInfo (Cpc.AIType.stopper);
			return;
		}
		*/

		if (isMagicBound == true)return;
		if (isPlayer == true)return;
		if (isNetPlayer == true)return;
		if (carryState != Cpc.CarryState.noCarry) return;

		if (toJob != -1) {
//			Debug.Log ("newAItype :" + (Cpc.AIType)toJob);
			initAIInfo ((Cpc.AIType)toJob);
			return;
		}

		//最初の生成はstealer

		if (Cpc.makedPlayer < Cpc.MaxChara) {
			initAIInfo ( Cpc.AIType.stealer);
			return;
		}

		//carrierがゼロだと、だれかがcarrierになる
		if (_nowAI != Cpc.AIType.carrier &&
			_myKuma.GetComponent<kuma> ().nowState != Cpc.KumaState.Sitting &&
			((myTeam == Cpc.Team.Red && Cpc.AICounter_r [(int)Cpc.AIType.carrier] < 1) ||
			 (myTeam == Cpc.Team.Blue && Cpc.AICounter_b [(int)Cpc.AIType.carrier] < 1))) {
			initAIInfo ( Cpc.AIType.carrier);
			return;
		}

		//敵クマがsit状態だと、stealerになる
		if ((myTeam == Cpc.Team.Red && Cpc._kumaObj[(int)Cpc.Team.Blue].GetComponent<kuma> ().nowState == Cpc.KumaState.Sitting) ||
			(myTeam == Cpc.Team.Blue && Cpc._kumaObj[(int)Cpc.Team.Red].GetComponent<kuma> ().nowState == Cpc.KumaState.Sitting))
		{
			initAIInfo ( Cpc.AIType.stealer);
			return;
		}
		//味方クマがInfestedだとストッパーに
		if (_carryKumaEm.GetComponent<kuma> ().nowState == Cpc.KumaState.Lifting &&
			((myTeam == Cpc.Team.Red && Cpc.AICounter_r [(int)Cpc.AIType.stopper] < 1) ||
				(myTeam == Cpc.Team.Blue && Cpc.AICounter_b [(int)Cpc.AIType.stopper] < 1)))
		{
			initAIInfo ( Cpc.AIType.stopper);
			return;
		}

		//確率で9つの性格になる
		int i=0;
		while(i<100)
		{
			//アタッカー率は3倍
			int t = UnityEngine.Random.Range (0, Enum.GetNames(typeof(Cpc.AIType)).Length-1 +2);
			if (t > Enum.GetNames(typeof(Cpc.AIType)).Length-1)
			{
				t = (int)Cpc.AIType.attcker;
			}

			Cpc.AIType newAItype = (Cpc.AIType)t;

			//味方くまがSit状態の場合は、carrierにならない
			if (newAItype == Cpc.AIType.carrier && Cpc._kumaObj [(int)Cpc.Team.Red].GetComponent<kuma> ().nowState == Cpc.KumaState.Sitting) {
				i++;
				continue;
			}
			if (newAItype == _nowAI) {
				i++;
				continue;
			}
			//プリーストの数は制限
			if (myTeam == Cpc.Team.Red && Cpc.AICounter_r [(int)Cpc.AIType.stealer] > 1) {i++; continue;}
			if (myTeam == Cpc.Team.Blue && Cpc.AICounter_b [(int)Cpc.AIType.stealer] > 1) {i++; continue;}
			if (Cpc.MaxChara < 5) {
				if (myTeam == Cpc.Team.Red && Cpc.AICounter_r [(int)Cpc.AIType.stealer] > 0) {i++; continue;}
				if (myTeam == Cpc.Team.Blue && Cpc.AICounter_b [(int)Cpc.AIType.stealer] > 0) {i++; continue;}
			}
			/*
			*/

			//確率で左領域専門キャラに
			if (newAItype == Cpc.AIType.circular || newAItype == Cpc.AIType.sorcerer || newAItype == Cpc.AIType.stopper) {
				if (UnityEngine.Random.Range (0, 100) < 50) {
					isLiftAI = true;
				}
			}
			initAIInfo (newAItype, isLiftAI);

			break;
		}
	}

	private void initAIInfo(Cpc.AIType newAItype, bool isLeft = false)
	{
		isLiftAI = isLeft;

//		Debug.Log ("newAItype :" + newAItype);


		if (myTeam == Cpc.Team.Red) {
			Cpc.AICounter_r [(int)_nowAI]--;
			if (Cpc.AICounter_r [(int)_nowAI] < 0)
			{
				Cpc.AICounter_r [(int)_nowAI] = 0;
			}
		} else {
			Cpc.AICounter_b [(int)_nowAI]--;
			if (Cpc.AICounter_b [(int)_nowAI] < 0)
			{
				Cpc.AICounter_b [(int)_nowAI] = 0;
			}
		}
		_aiState = 0;
		_nowAI = newAItype;
		_nowAttackerType = Cpc.AIAttackType.no_type;
		if (_agent != null && _agent.enabled == true) {
			_agent.stoppingDistance = 1f;
		}
		if (myTeam == Cpc.Team.Red) {
			Cpc.AICounter_r [(int)_nowAI]++;
		} else {
			Cpc.AICounter_b [(int)_nowAI]++;
		}
	}

	public void init()
	{
		if (isPlayer == true) {
			Debug.Log ("######### init #########");
		}

		isDead = false;
		carryState = Cpc.CarryState.noCarry;
		isHumCarry = false;
		_charaInfo.isDead = false;

		nowActionType = Cpc.ActionType.Idle;
		playAnime(true);

		//初期位置
		if (myTeam == Cpc.Team.Red)
		{
			Cpc.charaPos_r.Remove (this.gameObject.transform);
			Cpc.charaPos_r.Add (this.gameObject.transform);
			this.transform.localPosition = Cpc.myInitPos_r;
			this.transform.localRotation = Cpc.myInitRot_r;
		}
		else
		{
			Cpc.charaPos_b.Remove (this.gameObject.transform);
			Cpc.charaPos_b.Add (this.gameObject.transform);
			this.transform.localPosition = Cpc.myInitPos_b;
			this.transform.localRotation = Cpc.myInitRot_b;
		}
//		this.transform.parent = _charag;
		this.transform.SetParent(_charag);

		//カメラの有効・無効
		if (myTeam == Cpc.myTeam && isPlayer == true)
		{
			Cpc.myState = Cpc.PLayerStatus.normal;
			Cpc.uiC._mapCamera.enabled = false;
			this.gameObject.transform.FindChild("CameraB").gameObject.SetActive(true);
		}
		else
		{
			this.gameObject.transform.FindChild("CameraB").gameObject.SetActive(false);
		}

		_myPos.x = this.transform.localPosition.x;
		_myPos.y = this.transform.localPosition.z;
		_shadow[0].SetActive (false);
		_shadow[1].SetActive (false);

		_cui.init();
		_cui.jobchg(isPlayer, myJob, _charaInfo);

//		Debug.Log("<<<<<<<<<<<<<<<<< いにっと : " + _charaInfo.toString());
		Cpc.uiC.showName(_charaInfo);

		Cpc.playerRoster [_charaInfo.playerNo] = _charaInfo;


		if (myTeam == Cpc.myTeam && isPlayer == true)
		{
			Cpc.myState = Cpc.PLayerStatus.normal;
		}
		_cuiRect [0].GetComponent<RectTransform>().localPosition = _cuiBk;
		_cuiRect [1].GetComponent<RectTransform>().localPosition = _cuiBk;
		if (isPlayer == false && isNetPlayer == false) {
			_cuiRect [0].gameObject.SetActive(false);
			_cuiRect [1].gameObject.SetActive(false);
		}
	}

	void Update ()
	{
		if (Cpc.appState != Cpc.AppState.battle && Cpc.appState != Cpc.AppState.countdown) return;

	
		if (this.transform.position.y < -200f) {
//			Cpc.uiC.showErrorMsg ("NetWork is Busy!!!!");
			Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!");

			Cpc.CharaInfo cInfo = Cpc.playerRoster[Cpc.myPlayerNo];
			cInfo.init(cInfo.playerNo,myTeam,isPlayer);
			Cpc.playerRoster[Cpc.myPlayerNo] = cInfo;
			toAlive(cInfo);
		}

		//くまの追随
		if (carryState == Cpc.CarryState.myCarry || carryState == Cpc.CarryState.emCarry)
		{
			_kumadelta += Time.deltaTime;
			if (_kumadelta > _kumaCheck)
			{
				_kumadelta = 0;
				if (carryState == Cpc.CarryState.myCarry)
				{
					float f = Util.geoLength (_carryKuma.transform.position.x, _carryKuma.transform.position.z, this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.z);
					//くまから離れすぎると自動リリース
					if (f > 150f)
					{
						Debug.Log ("releaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKuma");

						releaseKuma();
					}
					else
					{
						_carryKuma.transform.localPosition = new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y+1.4f, this.gameObject.transform.localPosition.z);
						Cpc.netC.SendKumaPos(_carryKuma.transform.localPosition, _carryKuma.GetComponent<kuma>()._myTeam);
					}
				}
				else
				{
					float f = Util.geoLength (_carryKumaEm.transform.position.x, _carryKumaEm.transform.position.z, this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.z);

//					Debug.Log("F :" + f);


					//くまから離れすぎると自動リリース
					if (f > 150f)
					{
						Debug.Log ("!!!!!!! releaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKumareleaseKuma");
						releaseKuma();
					}
					else
					{
						_carryKumaEm.transform.localPosition = new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y+1.4f, this.gameObject.transform.localPosition.z);
						Cpc.netC.SendKumaPos(_carryKumaEm.transform.localPosition, _carryKumaEm.GetComponent<kuma>()._myTeam);
					}
				}
			}
		}
		//ハンバーガーの追随
		if (isHumCarry == true)
		{
			float f = Util.geoLength (_carryKumaEm.transform.position.x, _carryKumaEm.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);

			if(f < 3 && _carryKumaEm.GetComponent<kuma>().nowState == Cpc.KumaState.Putting)
			{
				StartCoroutine(feedBurger());
			}
			else
			{
				_humdelta += Time.deltaTime;
				if (_humdelta > _humCheck)
				{
					_humdelta = 0;
					burgerObj.transform.position = new Vector3(_myAvator.transform.position.x, _myAvator.transform.position.y+1.4f, _myAvator.transform.position.z);
					Cpc.netC.SendHamPos(this.transform.position, burgerObj.GetComponent<PhotonView>().viewID);
				}
			}
		}

		//リカバリー処理 
		if (myJob == Cpc.JobType.Priest)
		{
			if (_rcvdelta > _rcvCheck) {
				if (_rcvCollider.enabled == true) {
					_rcvCollider.enabled = false;
				} else {
					_rcvCollider.enabled = true;
				}
				_rcvdelta = 0;
			} else {
				_rcvdelta += Time.deltaTime;
			}
		}

		//くまサポート
		if (_myKuma != null)
		{
			isInCarryingKuma();
			if (_nowSupportNum != _myKuma.GetComponent<kuma>()._nowSupport)
			{
				_nowSupportNum = _myKuma.GetComponent<kuma>()._nowSupport;

				float f = leftWeight - _nowSupportNum;

				if (f < 1f) f = 1f;

				if (isPlayer == false && isNetPlayer == false)
				{
					if (_agent != null)
					{
						_agent.speed = Cpc.COM_SPD_MAX / f; 
					}
				}
				else
				{
					_nowWeight = f;
				}
			}
		}
		//くまセンサー


		//COM処理 -------------------------------
		if (isPlayer == false && isNetPlayer == false && nowActionType != Cpc.ActionType.Diying)
		{
			//一時停止
			if (_agent != null)
			{
				//すくみ
				if (isCanAction() == false && isShrinkage == false && isMagicBound == false)
				{
					_agent.speed = 0;
					isShrinkage = true;
					StartCoroutine(clearShrinkage());
				}
			}

			if (isReadyAgent == false) {
				this.GetComponent<CharacterController> ().Move (new Vector3 (0, -10, 0) * Time.deltaTime);
				return;
			}

			//位置チェック
			if (_timePosCk > 3f)
			{
				_timePosCk = 0f;
				if ((_comPosBk.x - this.transform.position.x < 0.1f || _comPosBk.x - this.transform.position.x < -0.1f) ||
					(_comPosBk.z - this.transform.position.z < 0.1f || _comPosBk.z - this.transform.position.z < -0.1f))
				{
					decideAI();
				}
			}
			else
			{
				_comPosBk = this.transform.position;
				_timePosCk += Time.deltaTime;
			}

			//歩く・待機
			if (_timeSpeedCk > Cpc.COM_SPDCK_T)
			{
				_timeSpeedCk = 0;
				float f = Util.geoLength(this.transform.localPosition.x, this.transform.localPosition.z, _myPos.x, _myPos.y);
				_myPos.x = this.transform.localPosition.x;
				_myPos.y = this.transform.localPosition.z;
				if (f > Cpc.COM_RUN_DIS && nowActionType != Cpc.ActionType.Run)
				{
					nowActionType = Cpc.ActionType.Run;
					playAnime();
				}
				else
				{
					if (f <= Cpc.COM_RUN_DIS)
					{
						idle();
					}
				}

			}
			else
			{
				_timeSpeedCk += Time.deltaTime;
			}

			//AI変更チェック(くま運び中や stealerは変化しない)
			if (_timeChangeAI > Cpc.COM_CHANGEAI && carryState == Cpc.CarryState.noCarry) {
				_timeChangeAI = 0;
				//確率でロジックが変わる
				if (UnityEngine.Random.Range(0 ,100) < Cpc.COM_CHANGEAIRATE)
				{
//					Debug.Log ("変更!");
					decideAI ();
				}
			}
			else
			{
				_timeChangeAI += Time.deltaTime;
			}

			_agent.stoppingDistance = Cpc.LEFT_DIS;


			//目標を探す・攻撃
			if (_timeSearch > Cpc.COM_SEARCH_T)
			{
				_timeSearch = 0;
				Transform t = null;

				//Attacker -> 最寄りの敵を探して攻撃
				if (_nowAI == Cpc.AIType.attcker)
				{
					//TODO:ナイトにジョブチェンジ -> 動作確認 
					if (myJob != Cpc.JobType.Knight)
					{
						if (myTeam == Cpc.Team.Red) {
							t = GameObject.Find ("chg_kn_r").gameObject.transform;
						} else {
							t = GameObject.Find ("chg_kn_b").gameObject.transform;
						}
						if (_agent.enabled == true) {
							_agent.autoRepath = true;
							_agent.SetDestination (t.position);
						}
						return;
					}

					t = nearestTarget ();

					if (t != null)
					{
						if (_agent.enabled == true) {
							_agent.autoRepath = true;
							_agent.SetDestination (t.position);
						}
						float f2 = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);

						if (f2 < Cpc.COM_ATK_DIS && isCanAction () == true) {
							float r = UnityEngine.Random.Range (Cpc.COM_ATK_MIN, Cpc.COM_ATK_MAX);
							if (_timeAtkNext > r) {
								attack ();
								_timeAtkNext = 0;
							}
						}
					}
				//Carrier -> 自分のくまに向かって行って運ぶ
				} else if (_nowAI == Cpc.AIType.carrier)
				{
					//くま掴む・サポート
					if (_myKuma.GetComponent<kuma> ().nowState != Cpc.KumaState.Sitting && (_myKuma.GetComponent<kuma> ().nowState != Cpc.KumaState.Infested))
					{
						if (carryState == Cpc.CarryState.noCarry) {
							t = _myKuma.transform;
							if (_agent.enabled == true) {
								_agent.autoRepath = true;
								_agent.SetDestination (t.position);
							}

							float f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
							if (f < Cpc.LEFT_DIS && isCanAction () == true) {
								if (_myKuma.GetComponent<kuma>().nowState == Cpc.KumaState.Putting)
								{
									//くまを掴みに行く
									lift(Cpc.CarryState.myCarry);
								}
								else
								{
									decideAI((int)Cpc.AIType.attcker);
								}

							}

						//くまをベンチに運ぶ
						} else {
							t = _myKuma.GetComponent<kuma> ().getMyBench ();
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
							}
							float f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
							if (f < Cpc.BENCHRANGE) {
								releaseKuma ();
								Cpc.playSe (Cpc.SeType.pickup);

								//Attackerに変化
//								_nowAI = Cpc.AIType.attcker;
								decideAI();
							}
						}
					}
					else
					{
						if (_nowAI != Cpc.AIType.attcker)
						{
							//Attackerに変化
//							_nowAI = Cpc.AIType.attcker;
							decideAI();
						}
					}
				//Cannon思考
				} else if (_nowAI == Cpc.AIType.cannon) {
					float f = 0f;
//					Debug.Log ("_aiState : " + _aiState);

					switch (_aiState) {
						case 0:
						//1.味方大砲に行く
							if(myTeam == Cpc.Team.Red){
								t = GameObject.Find ("cannonCol_r").gameObject.transform;
							} else {
								t = GameObject.Find ("cannonCol_b").gameObject.transform;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
								_aiState = 1;
							}
							break;
						//2.飛ばされる
						//3.敵ベンチの近くに行く
						case 2:
							if(myTeam == Cpc.Team.Red){
								t = GameObject.Find ("Bench_b").gameObject.transform;
							} else {
								t = GameObject.Find ("Bench_r").gameObject.transform;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
								_aiState = 3;
							}
							break;
						//4.アタッカーになる(6秒)
						case 3:
							if(myTeam == Cpc.Team.Red){
								t = GameObject.Find ("Bench_b").gameObject.transform;
							} else {
								t = GameObject.Find ("Bench_r").gameObject.transform;
							}
							f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
							if (f < Cpc.BENCHRANGE*2) {
								_nowAI = Cpc.AIType.attcker;
								_nowAttackerType = Cpc.AIAttackType.cannon_attaker1;
								_timeAttakerTimer = 0f;
							}
							break;
						//5.敵大砲に行く
						case 4:
							if(myTeam == Cpc.Team.Red){
								t = GameObject.Find ("cannonCol_b").gameObject.transform;
							} else {
								t = GameObject.Find ("cannonCol_r").gameObject.transform;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
								_aiState = 5;
							}
							break;
						//6.飛ばされる
						//7.味方ベンチの近くに行く
						case 6:
							if(myTeam == Cpc.Team.Red){
								t = GameObject.Find ("Bench_r").gameObject.transform;
							} else {
								t = GameObject.Find ("Bench_b").gameObject.transform;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
								_aiState = 7;
							}
							break;
						//8.アタッカーになる(6秒)
					case 7:
						if (myTeam == Cpc.Team.Red) {
							t = GameObject.Find ("Bench_r").gameObject.transform;
						} else {
							t = GameObject.Find ("Bench_b").gameObject.transform;
						}
						f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);

//						Debug.Log ("f : " + f );
//						Debug.Log ("Cpc.BENCHRANGE : " + Cpc.BENCHRANGE );

							if (f < Cpc.BENCHRANGE*2) {
								_nowAI = Cpc.AIType.attcker;
								_nowAttackerType = Cpc.AIAttackType.cannon_attaker2;
								_timeAttakerTimer = 0f;
								_aiState = 8;
							}
							break;
					}
				//stealer思考
				} else if (_nowAI == Cpc.AIType.stealer) {
					
					float f = 0f;

					switch (_aiState) {
					//1.敵クマに行く
					//2.担いでいたら、アタッカーになる(put状態まで)
					//  担いでなかったら、担ぐ
					case 0:
						if (_agent.enabled == true) {
							_agent.SetDestination (_carryKumaEm.transform.position);
						}
						f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, _carryKumaEm.transform.localPosition.x, _carryKumaEm.transform.localPosition.z);
						if (f < Cpc.LEFT_DIS && isCanAction () == true) {
							if (_carryKumaEm.GetComponent<kuma>().nowState == Cpc.KumaState.Sitting)
							{
								//敵くまを掴みに行く
								lift(Cpc.CarryState.emCarry);
								_aiState = 2;
							}
							else
							{
								_nowAI = Cpc.AIType.attcker;
								_nowAttackerType = Cpc.AIAttackType.stealer_attaker;
								_timeAttakerTimer = 0f;
								_aiState = 1;
							}
						}
						break;
					//3.担いでいる時は、味方牢屋まで移動
					case 2:
						if(myTeam == Cpc.Team.Red){
							t = GameObject.Find ("prison_r").gameObject.transform;
						} else {
							t = GameObject.Find ("prison_b").gameObject.transform;
						}
						if (_agent.enabled == true) {
							_agent.autoRepath = false;
							_agent.SetDestination (t.position);
							_aiState = 3;
						}
						break;
					//4. 牢屋まで移動したら、下ろして、性格変化
					case 3:
						if (myTeam == Cpc.Team.Red) {
							t = GameObject.Find ("prison_r").gameObject.transform;
						} else {
							t = GameObject.Find ("prison_b").gameObject.transform;
						}
						f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);

						if (f < Cpc.BENCHRANGE) {
							_aiState = 0;
							releaseKuma ();
							decideAI();
						}

						break;
					}
				//クルーザー思考
				} else if (_nowAI == Cpc.AIType.circular) {
					//1.Sit状態なら、敵クマに移動して担いて、橋付近まで持ってくる
					//2.Sit状態でなければ、橋付近を行ったり来たり（縦）
					float f = 0f;
					switch (_aiState) {
						case 0:
							
							if (_carryKumaEm.GetComponent<kuma>().nowState == Cpc.KumaState.Sitting) {
								_aiState = 3;
								t = _carryKumaEm.transform;
							//うろうろ開始
							} else {
								if (isLiftAI == false) {
									if (myTeam == Cpc.Team.Red) {
										t = GameObject.Find ("bridge_b_r").gameObject.transform;
									} else {
										t = GameObject.Find ("bridge_r_l").gameObject.transform;
									}
								} else {
									if (myTeam == Cpc.Team.Red) {
										t = GameObject.Find ("bridge_r_r").gameObject.transform;
									} else {
										t = GameObject.Find ("bridge_b_l").gameObject.transform;
									}
								}
								_aiState = 1;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
							}

							break;
						//うろうろ1
						case 1:
							if (isLiftAI == false) {
								if (myTeam == Cpc.Team.Red) {
									t = GameObject.Find ("bridge_b_r").gameObject.transform;
								} else {
									t = GameObject.Find ("bridge_r_l").gameObject.transform;
								}
							} else {
								if (myTeam == Cpc.Team.Red) {
									t = GameObject.Find ("bridge_r_r").gameObject.transform;
								} else {
									t = GameObject.Find ("bridge_b_l").gameObject.transform;
								}
							}
							f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
							if (f < Cpc.LEFT_DIS) {
								if (isLiftAI == false) {
									if (myTeam == Cpc.Team.Red) {
										t = GameObject.Find ("bridge_r_l").gameObject.transform;
									} else {
										t = GameObject.Find ("bridge_b_r").gameObject.transform;
									}
								} else {
									if (myTeam == Cpc.Team.Red) {
										t = GameObject.Find ("bridge_b_l").gameObject.transform;
									} else {
										t = GameObject.Find ("bridge_r_r").gameObject.transform;
									}
								}
								_aiState = 2;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
							}
							break;
						//2.うろうろ2
						case 2:
							if (isLiftAI == false) {
								if (myTeam == Cpc.Team.Red) {
									t = GameObject.Find ("bridge_r_l").gameObject.transform;
								} else {
									t = GameObject.Find ("bridge_b_r").gameObject.transform;
								}
							} else {
								if (myTeam == Cpc.Team.Red) {
									t = GameObject.Find ("bridge_b_l").gameObject.transform;
								} else {
									t = GameObject.Find ("bridge_r_r").gameObject.transform;
								}
							}
							f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
							if (f < Cpc.LEFT_DIS) {
								_aiState = 0;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
							}
							break;
						//3.座ってるクマを掴みに行く
						case 3:
							f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, _carryKumaEm.transform.localPosition.x, _carryKumaEm.transform.localPosition.z);
							if (_carryKumaEm.GetComponent<kuma> ().nowState == Cpc.KumaState.Sitting) {
								if (f < Cpc.LEFT_DIS && isCanAction () == true) {
									if (carryState == Cpc.CarryState.noCarry) {
										//敵くまを掴みに行く
										lift (Cpc.CarryState.emCarry);
										_aiState = 4;
									}
								} else {
									_aiState = 0;
								}
							} else {
								_aiState = 0;
							}
							break;
						//4.担いでいる時は、橋付近まで持ってくる
						case 4:
							if (myTeam == Cpc.Team.Red) {
								t = GameObject.Find ("bridge_l").gameObject.transform;
							} else {
								t = GameObject.Find ("bridge_r").gameObject.transform;
							}
							f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
							if (f < Cpc.LEFT_DIS && isCanAction () == true) {
								releaseKuma ();
								_aiState = 0;
							}
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
							}

						break;
					}
				//ソーサラ思考
				} else if (_nowAI == Cpc.AIType.sorcerer) {
//					Debug.Log ("_aiState : " + _aiState);
					/*
					  1.魔導師にジョブチェンジ
					  2.橋付近をうろうろ（横、味方陣地）
					  3.範囲に入ったら、魔法発動(広範囲)。敵追尾
  					*/
					float f = 0f;
					switch (_aiState) {
					case 0:
						if (myJob != Cpc.JobType.Sorcerer) {
							if (myTeam == Cpc.Team.Red) {
								t = GameObject.Find ("chg_bk_r").gameObject.transform;
							} else {
								t = GameObject.Find ("chg_bk_b").gameObject.transform;
							}
						} else {
							if (myTeam == Cpc.Team.Red) {
								t = GameObject.Find ("so_r_l").gameObject.transform;
							} else {
								t = GameObject.Find ("so_b_l").gameObject.transform;
							}
							_aiState = 1;
						}
						break;
					case 1:
						if (myTeam == Cpc.Team.Red) {
							t = GameObject.Find ("so_r_l").gameObject.transform;
						} else {
							t = GameObject.Find ("so_b_l").gameObject.transform;
						}
						f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
						if (f < Cpc.LEFT_DIS) {
							if (myTeam == Cpc.Team.Red) {
								t = GameObject.Find ("so_r_r").gameObject.transform;
							} else {
								t = GameObject.Find ("so_b_r").gameObject.transform;
							}
							_aiState = 2;
						}
						break;
					case 2:
						if (myTeam == Cpc.Team.Red) {
							t = GameObject.Find ("so_r_r").gameObject.transform;
						} else {
							t = GameObject.Find ("so_b_r").gameObject.transform;
						}
						f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
						if (f < Cpc.LEFT_DIS) {
							_aiState = 0;
						}
						break;
					}
					if (_agent.enabled == true) {
						_agent.autoRepath = false;
						_agent.SetDestination (t.position);
					}
					//魔法
					if (myJob == Cpc.JobType.Sorcerer) {
						t = nearestTarget();
						float f2 = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);

						if (f2 < Cpc.ATK_M_RANGE && isCanAction () == true) {
							float r = UnityEngine.Random.Range (Cpc.COM_ATK_MIN, Cpc.COM_ATK_MAX);
							if (_timeAtkNext > r) {
								attack ();
								_timeAtkNext = 0;
							}
						}
					}
				//プリースト思考
				} else if (_nowAI == Cpc.AIType.priest) {
					/*
					  1.プリーストにジョブチェンジ
					  2.アタッカーを探す
					  3.アタッカーのそばを付かず離れず、魔法発動で追尾					 
  					*/
					float f = 0f;
					switch (_aiState) {
					case 0:
						if (myJob != Cpc.JobType.Priest) {
							_agent.stoppingDistance = 0f;
							if (myTeam == Cpc.Team.Red) {
								t = GameObject.Find ("chg_wh_r").gameObject.transform;
							} else {
								t = GameObject.Find ("chg_wh_b").gameObject.transform;
							}
						} else {
							_aiState = 1;
						}
						break;
					case 1:
						if (myJob == Cpc.JobType.Priest) {
							_agent.stoppingDistance = 6f;
							float minf = 9999f;
							//一番近いアタッカーをターゲットに
							if (myTeam == Cpc.Team.Red && Cpc.charaPos_b.Count > 0)
							{
								foreach(Transform tf in Cpc.charaPos_r)
								{
									if (tf == null) continue;
									chara c = tf.gameObject.GetComponent<chara> ();
									if (c.isPlayer == true || c.isNetPlayer == true || c._nowAI == Cpc.AIType.attcker) {
										f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, tf.localPosition.x, tf.localPosition.z);
										if (f < minf)
										{
											minf = f;
											t = tf;
										}
									}
								}
							}
							if (myTeam == Cpc.Team.Blue && Cpc.charaPos_r.Count > 0)
							{
								foreach(Transform tf in Cpc.charaPos_b)
								{
									if (tf == null) continue;
									chara c = tf.gameObject.GetComponent<chara> ();
									if (c.isPlayer == true || c.isNetPlayer == true || c._nowAI == Cpc.AIType.attcker) {
										f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, tf.localPosition.x, tf.localPosition.z);
										if (f < minf)
										{
											minf = f;
											t = tf;
										}
									}
								}
							}
						}
						break;
					}
					if (_agent.enabled == true && t != null) {
						_agent.autoRepath = true;
						_agent.SetDestination (t.position);
					}

				//キャリア(m)思考
				} else if (_nowAI == Cpc.AIType.carrier_m) {
					//くま掴む・サポート
					if (_myKuma.GetComponent<kuma> ().nowState != Cpc.KumaState.Sitting && (_myKuma.GetComponent<kuma> ().nowState != Cpc.KumaState.Infested))
					{
						if (carryState == Cpc.CarryState.noCarry) {
							t = _myKuma.transform;
							if (_agent.enabled == true) {
								_agent.autoRepath = true;
								_agent.SetDestination (t.position);
							}

							if (_myKuma.GetComponent<kuma>().nowState == Cpc.KumaState.Putting)
							{
								float f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
								if (f < Cpc.LEFT_DIS && isCanAction () == true) {
									//くまを掴みに行く
									lift(Cpc.CarryState.myCarry);
								}

							}

						//くまを中央に運ぶ
						} else {
							t = GameObject.Find ("carry_m").gameObject.transform;
							if (_agent.enabled == true) {
								_agent.autoRepath = false;
								_agent.SetDestination (t.position);
							}
							float f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
							if (f < Cpc.BENCHRANGE) {
								releaseKuma ();
								Cpc.playSe (Cpc.SeType.pickup);

								decideAI();
							}
						}
					}
					else
					{
						decideAI();
					}
				//中央に移動 思考
				} else if (_nowAI == Cpc.AIType.move) {
					if (_aiState == 0)
					{
						t = GameObject.Find ("carry_m").gameObject.transform;
						if (_agent.enabled == true && t != null) {
							_agent.autoRepath = false;
							_agent.SetDestination (t.position);
						}
						float f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);
						if (f < Cpc.LEFT_DIS) {
							_aiState = 1;
						}
					}
					else
					{
						decideAI ((int)Cpc.AIType.cannon);
					}
				//ストッパー思考
				} else if (_nowAI == Cpc.AIType.stopper) {
					switch (_aiState) {
					case 0:
						if (myJob != Cpc.JobType.Sorcerer) {
							if (myTeam == Cpc.Team.Red) {
								t = GameObject.Find ("chg_bk_r").gameObject.transform;
							} else {
								t = GameObject.Find ("chg_bk_b").gameObject.transform;
							}
						} else {
							t = _carryKumaEm.transform;
							_aiState = 1;
						}
						if (_agent.enabled == true) {
							_agent.autoRepath = false;
							_agent.SetDestination (t.position);
						}
						break;
					case 1:
						if (myJob == Cpc.JobType.Sorcerer) {
							_agent.stoppingDistance = 12f;
						}
						//攻撃待機
						break;
					}
					//魔法
					if (myJob == Cpc.JobType.Sorcerer) {
						t = nearestTarget();
						float f2 = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, t.localPosition.x, t.localPosition.z);

						if (f2 < Cpc.ATK_M_RANGE && isCanAction () == true) {
							float r = UnityEngine.Random.Range (Cpc.COM_ATK_MIN, Cpc.COM_ATK_MAX);
							if (_timeAtkNext > r) {
								attack ();
								_timeAtkNext = 0;
							}
						}
						if (_agent.enabled == true) {
							_agent.autoRepath = true;
							_agent.SetDestination (t.position);
						}
					}
				}
			} else {
				_timeSearch += Time.deltaTime;
				_timeAtkNext += Time.deltaTime;

				if (_nowAttackerType != Cpc.AIAttackType.no_type) {
					_timeAttakerTimer += Time.deltaTime;

					//Cannon思考に戻る
					if (_nowAttackerType == Cpc.AIAttackType.cannon_attaker1) {
						if (_timeAttakerTimer > 6) {
							_nowAttackerType = Cpc.AIAttackType.no_type;
							_nowAI = Cpc.AIType.cannon;
							_aiState = 4;
						}
					}
					if (_nowAttackerType == Cpc.AIAttackType.cannon_attaker2) {
						if (_timeAttakerTimer > 6) {
							_nowAttackerType = Cpc.AIAttackType.no_type;
							_nowAI = Cpc.AIType.cannon;
							_aiState = 0;
						}
					}
					if (_nowAttackerType == Cpc.AIAttackType.stealer_attaker) {
						//くまが解放されたらStealer思考に戻る
						GameObject obj;
						if(myTeam == Cpc.Team.Red){
							obj = GameObject.Find ("Kuma_b(Clone)").gameObject;
						} else {
							obj = GameObject.Find ("Kuma_r(Clone)").gameObject;
						}
						if (obj.GetComponent<kuma>().nowState == Cpc.KumaState.Putting)
						{
							_nowAttackerType = Cpc.AIAttackType.no_type;
							_nowAI = Cpc.AIType.stealer;
							_aiState = 0;
						}
					}
				}

			}
		}
		else if (isNetPlayer == false)
		{
			//idleに戻す
			if (_timedelta > _timeCheck)
			{
				_timedelta = 0;
				idle();
			}
			else
			{
				_timedelta += Time.deltaTime;
			}

			if (_myAvator.transform.localPosition.y > -1.4f && nowActionType != Cpc.ActionType.Jump)
			{
				_myAvator.transform.localPosition = new Vector3(_myAvator.transform.localPosition.x, _myAvator.transform.localPosition.y-0.1f, _myAvator.transform.localPosition.z);
			}

		}

		if (_timeBodyCheck > 0.02f && nowActionType != Cpc.ActionType.Jump && nowActionType != Cpc.ActionType.Canon) {
			_timeBodyCheck = 0f;
			//UI部を常に正面に
			adjustCUI();

			//重力
			if (isPlayer == true && isNetPlayer == false && nowActionType != Cpc.ActionType.Diying && nowActionType != Cpc.ActionType.Dead) {
				this.GetComponent<CharacterController> ().Move (new Vector3 (0, -30, 0) * Time.deltaTime);
			}
		} else {
			_timeBodyCheck += Time.deltaTime;
		}


		if (isPlayer == true && nowActionType == Cpc.ActionType.Canon) {
			this.GetComponent<CharacterController> ().Move (new Vector3 (0, -10, 0) * Time.deltaTime);
		}
			
		if (nowActionType == Cpc.ActionType.Jump)
		{

			_nowPosX += jumpX * Cpc.JUMPWIDTH * 0.1f;
			_nowPosZ += jumpZ * Cpc.JUMPWIDTH * 0.1f;

			if (isJumped == false)
			{
				_nowPosY += (Cpc.JUMPHEIGHT/5f);

				if (_nowPosY > Cpc.JUMPHEIGHT)
				{
					isJumped = true;
				}
			}
			else
			{
				_nowPosY -= (Cpc.JUMPHEIGHT/5f);

				if (_myAvator.transform.localPosition.y < _nowPosYBk)
				{
					isJumped = false;
					nowActionType = Cpc.ActionType.Idle;
					playAnime();
					_nowPosY = _nowPosYBk;
				}
			}
			_myAvator.transform.localPosition = new Vector3(0, _nowPosY, 0);
			this.transform.localPosition = new Vector3(_nowPosX, this.transform.localPosition.y, _nowPosZ);
		}

		if (isInWater == true)
		{
			if (_timewtCheck > 3f) {
				_timewtCheck = 0f;
				/*
				if (isPlayer == true || isNetPlayer == true) {
					Util.playEff("water", Cpc.Team.NoTeam, this.gameObject.transform, this.gameObject.transform.position,0f,0f,0f, true, 3f, 0f);
				} else if (isPlayer == false) {
					Util.playEff("water", Cpc.Team.NoTeam, this.gameObject.transform, this.gameObject.transform.position,0f,0f,0f, true, 3f, 0.2f);
				}
				*/
			} else {
				_timewtCheck += Time.deltaTime;
			}
		}

		//影の表示
		if (_checkTime > 0.1f) {
			_checkTime = 0f;
			//影の表示
			if (_shadow[0] != null && _shadow[0].activeSelf == false && this.transform.localPosition.y < 1f && isInWater == false && nowActionType != Cpc.ActionType.Diying) {
				_shadow[0].SetActive (true);
				_shadow[1].SetActive (true);
			}
		} else {
			_checkTime += Time.deltaTime;
		}

	}

	private void adjustCUI()
	{
//		if (isPlayer == false || isNetPlayer == true) {
		if (isPlayer == false) {
			if (myTeam == Cpc.Team.Red) {
				_cuiRect [0].rotation = Quaternion.LookRotation (_uiCamera.forward, Vector3.up) * Quaternion.FromToRotation (Vector3.up, Vector3.forward);
				_cuiRect [0].Rotate (-90, 0, 0);
				_cuiRect [1].rotation = Quaternion.LookRotation (_uiCamera.forward, Vector3.up) * Quaternion.FromToRotation (Vector3.up, Vector3.forward);
				_cuiRect [1].Rotate (-90, 0, 0);
				//分けて回転させないとダメ
				_cuiRect [1].Rotate (0, 180, 0);
			} else {
				_cuiRect [0].rotation = Quaternion.LookRotation (_uiCamera.forward, Vector3.up) * Quaternion.FromToRotation (Vector3.up, Vector3.forward);
				_cuiRect [0].Rotate (-90, 0, 0);
				_cuiRect [1].rotation = Quaternion.LookRotation (_uiCamera.forward, Vector3.up) * Quaternion.FromToRotation (Vector3.up, Vector3.forward);
				_cuiRect [1].Rotate (-90, 0, 0);
				//分けて回転させないとダメ
				_cuiRect [0].Rotate (0, 180, 0);
			}
		}
	}

	private Transform nearestTarget()
	{
		Transform t = this.transform;

		float minf = 9999f;
		//一番近いのをターゲットに
		if (myTeam == Cpc.Team.Red && Cpc.charaPos_b.Count > 0)
		{
			foreach(Transform tf in Cpc.charaPos_b)
			{
				if (tf == null) continue;
				float f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, tf.localPosition.x, tf.localPosition.z);
				if (f < minf)
				{
					minf = f;
					t = tf;
				}
			}
		}
		if (myTeam == Cpc.Team.Blue && Cpc.charaPos_r.Count > 0)
		{
			foreach(Transform tf in Cpc.charaPos_r)
			{
				if (tf == null) continue;
				float f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, tf.localPosition.x, tf.localPosition.z);
				if (f < minf)
				{
					minf = f;
					t = tf;
				}
			}
		}

		return t;
	}

	void OnTriggerEnter(Collider collision)
	{
		if (isDead == true) return;

		//ナイト・プリーストからの被攻撃
		if ((myTeam == Cpc.Team.Red && collision.gameObject.name == "attack_b") ||
			(myTeam == Cpc.Team.Blue && collision.gameObject.name == "attack_r"))
		{
			float f = Util.geoLength (collision.gameObject.transform.position.x, collision.gameObject.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);

			if (f < Cpc.ATK_RANGE)
			{
				damaged(collision.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<chara>()._charaInfo.playerNo, collision.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<chara>()._charaInfo.attack());
			}
		}
		//魔導師の被攻撃予約
		if ((myTeam == Cpc.Team.Red && collision.gameObject.name == "attack_mb") ||
			(myTeam == Cpc.Team.Blue && collision.gameObject.name == "attack_mr"))
		{
			float f = Util.geoLength (collision.gameObject.transform.position.x, collision.gameObject.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);

			if (f < Cpc.ATK_M_RANGE)
			{
				magicBound ();
			}
		}
		//プリーストの回復
		if ((myTeam == Cpc.Team.Red && collision.gameObject.name == "recoveryArea_r") ||
			(myTeam == Cpc.Team.Blue && collision.gameObject.name == "recoveryArea_b"))
		{
			if (collision.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<chara> ()._charaInfo.playerNo != _charaInfo.playerNo) {

				if (_charaInfo.nowHp != _charaInfo.nowMaxHp)
				{
					collision.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<chara> ().showRcvEffect ();
					recovery(-100);
				}
			}
		}

		//ジョブチェンジ
		if (nowActionType != Cpc.ActionType.Attack)
		{
			float f = Util.geoLength (collision.gameObject.transform.position.x, collision.gameObject.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);

			//リカバリーでも反応するため、距離も判定
			if (f < 2)
			{
				bool isCanJobChange = false;

				//COMの場合特定の性格以外はジョブチェンジしない
				if (isPlayer == false && isNetPlayer == false)
				{
					if (_nowAI == Cpc.AIType.attcker && collision.gameObject.name == "chg_kn_r" && myTeam == Cpc.Team.Red && myJob != Cpc.JobType.Knight) {
						isCanJobChange = true;
					}
					if (_nowAI == Cpc.AIType.attcker && collision.gameObject.name == "chg_kn_b" && myTeam == Cpc.Team.Blue && myJob != Cpc.JobType.Knight) {
						isCanJobChange = true;
					}
					if ((_nowAI == Cpc.AIType.sorcerer || _nowAI == Cpc.AIType.stopper) && collision.gameObject.name == "chg_bk_r" && myTeam == Cpc.Team.Red && myJob != Cpc.JobType.Sorcerer) {
						isCanJobChange = true;
					}
					if ((_nowAI == Cpc.AIType.sorcerer || _nowAI == Cpc.AIType.stopper) && collision.gameObject.name == "chg_bk_b" && myTeam == Cpc.Team.Blue && myJob != Cpc.JobType.Sorcerer) {
						isCanJobChange = true;
					}
					if (_nowAI == Cpc.AIType.priest && collision.gameObject.name == "chg_wh_r" && myTeam == Cpc.Team.Red && myJob != Cpc.JobType.Priest) {
						isCanJobChange = true;
					}
					if (_nowAI == Cpc.AIType.priest && collision.gameObject.name == "chg_wh_b" && myTeam == Cpc.Team.Blue && myJob != Cpc.JobType.Priest) {
						isCanJobChange = true;
					}
				}
				else
				{
					isCanJobChange = true;
				}

				//ジョブチェンジ
				if (isCanJobChange == true)
				{
					if (myTeam == Cpc.Team.Red) {
						if (collision.gameObject.name == "chg_kn_r") {if (myJob != Cpc.JobType.Knight) {_cui.jobchg (isPlayer, Cpc.JobType.Knight, _charaInfo, true);}
						} else if (collision.gameObject.name == "chg_bk_r") {if (myJob != Cpc.JobType.Sorcerer) {_cui.jobchg (isPlayer, Cpc.JobType.Sorcerer, _charaInfo, true);}
						} else if (collision.gameObject.name == "chg_wh_r") {if (myJob != Cpc.JobType.Priest) {_cui.jobchg (isPlayer, Cpc.JobType.Priest, _charaInfo, true);}
						}
					}
					else
					{
						if (collision.gameObject.name == "chg_kn_b") {if (myJob != Cpc.JobType.Knight) {_cui.jobchg (isPlayer, Cpc.JobType.Knight, _charaInfo, true);}
						} else if (collision.gameObject.name == "chg_bk_b") {if (myJob != Cpc.JobType.Sorcerer) {_cui.jobchg (isPlayer, Cpc.JobType.Sorcerer, _charaInfo, true);}
						} else if (collision.gameObject.name == "chg_wh_b") {if (myJob != Cpc.JobType.Priest) {_cui.jobchg (isPlayer, Cpc.JobType.Priest, _charaInfo, true);}
						}
					}
				}
			}

			//ハンバーガー
			if (isPlayer == true  && isNetPlayer == false && ((Cpc.myTeam == Cpc.Team.Red && collision.gameObject.name == "hamburger_r(Clone)") || (Cpc.myTeam == Cpc.Team.Blue && collision.gameObject.name == "hamburger_b(Clone)")))
			{
				burgerObj_t = collision.gameObject;
			}
		}

		//キャノン
		if (collision.gameObject.name == "cannonCol_r" || collision.gameObject.name == "cannonCol_b")
		{
			float f = Util.geoLength (collision.gameObject.transform.position.x, collision.gameObject.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);

			//リカバリーでも反応するため、距離も判定
			if (f < 4) {
				if (_nowAI == Cpc.AIType.cannon) {
					if (_aiState == 1) {
						_aiState = 2;
					}
					if (_aiState == 5) {
						_aiState = 6;
					}
				}
				Cpc.playSe(Cpc.SeType.catapult);
				StartCoroutine (cannonJump(collision));
			}
		}
	}

	public void inWater()
	{
		isInWater = true;
		_shadow[0].SetActive (false);
		_shadow[1].SetActive (false);
		if (_agent != null && _agent.enabled == true) {
			if (isMagicBound == false)
			{
				_agent.speed = 0.3f*Cpc.COM_SPD_MAX / leftWeight;
			}
			_myAvator.transform.localPosition = new Vector3(0, -0.5f, 0);
		}
		else
		{
			_myAvator.transform.localPosition = new Vector3(0, -2f, 0);
		}
		if (isPlayer == true || isNetPlayer == true) {
			Util.playEff("water", Cpc.Team.NoTeam, this.gameObject.transform, this.gameObject.transform.position,0f,0f,0f, true, 3f, -1f);
		} else if (isPlayer == false) {
			Util.playEff("water", Cpc.Team.NoTeam, this.gameObject.transform, this.gameObject.transform.position,0f,0f,0f, true, 3f, 0.2f);
		}

	}

	public void outWater()
	{
		isInWater = false;
		_shadow[0].SetActive (true);
		_shadow[1].SetActive (true);
		if (_agent != null && _agent.enabled == true) {
			if (isMagicBound == false)
			{
				_agent.speed = Cpc.COM_SPD_MAX / leftWeight;
			}
			_myAvator.transform.localPosition = new Vector3(0, 0.22f, 0);
		}
		else
		{
			_myAvator.transform.localPosition = new Vector3(0, -1.4f, 0);
		}

	}

	private IEnumerator cannonJump(Collider collision)
	{
		_shadow[0].SetActive (false);
		_shadow[1].SetActive (false);

		_myAvator.transform.localRotation = Quaternion.Euler (new Vector3(0, 0, 0));
		yield return new WaitForSeconds (0.1f);

		collision.gameObject.transform.FindChild ("eff_cannon").GetComponent<ParticleSystem> ().Play ();
		changeState (Cpc.ActionType.Canon);

		playAnime(true);

		int i = 0;

		float fCom = 0f;
		int iCom = 0;
		if (isPlayer == false)
		{
			fCom = 0.5f;
			iCom = 10;
		}


		if (collision.name == "cannonCol_r") {
			while(i < 30+iCom)
			{
				this.gameObject.transform.localPosition = new Vector3 (this.gameObject.transform.localPosition.x-0.9f, this.gameObject.transform.localPosition.y+0.8f+4f*fCom, this.gameObject.transform.localPosition.z+0.5f);
				i++;
				yield return new WaitForSeconds (0.01f);
			}
			while(i < 60+iCom)
			{
				this.gameObject.transform.localPosition = new Vector3 (this.gameObject.transform.localPosition.x-0.9f, this.gameObject.transform.localPosition.y-0.8f-4f*fCom, this.gameObject.transform.localPosition.z+0.5f);
				i++;

				if (this.gameObject.transform.localPosition.x < -90f) {
					this.gameObject.transform.localPosition = new Vector3 (-90f, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z);
				}
				if (this.gameObject.transform.localPosition.z > 39.4f) {
					this.gameObject.transform.localPosition = new Vector3 (this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, 39.4f);
				}

				if (this.gameObject.transform.localPosition.y < 0) {
					break;
				}
				yield return new WaitForSeconds (0.01f);
			}
		} else {
			while(i < 30+iCom)
			{
				this.gameObject.transform.localPosition = new Vector3 (this.gameObject.transform.localPosition.x+0.9f, this.gameObject.transform.localPosition.y+0.8f+4f*fCom, this.gameObject.transform.localPosition.z-0.5f);
				i++;
				yield return new WaitForSeconds (0.01f);
			}
			while(i < 60+iCom)
			{
				this.gameObject.transform.localPosition = new Vector3 (this.gameObject.transform.localPosition.x+0.9f, this.gameObject.transform.localPosition.y-0.8f-4f*fCom, this.gameObject.transform.localPosition.z-0.5f);
				i++;

				if (this.gameObject.transform.localPosition.x > -56f) {
					this.gameObject.transform.localPosition = new Vector3 (-56f, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z);
				}
				if (this.gameObject.transform.localPosition.z < 10.6f) {
					this.gameObject.transform.localPosition = new Vector3 (this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, 10.6f);
				}

				if (this.gameObject.transform.localPosition.y < 0) {
					break;
				}
				yield return new WaitForSeconds (0.01f);
			}
		}
		_myAvator.transform.localRotation = Quaternion.Euler (new Vector3(0, 0, 0));
		_shadow[0].SetActive (true);
		_shadow[1].SetActive (true);

		changeState (Cpc.ActionType.Idle);

		yield return null;
	}

	private IEnumerator clearShrinkage()
	{
		yield return new WaitForSeconds(0.8f);

		if (isMagicBound == false)
		{
			isShrinkage = false;
			_agent.speed = Cpc.COM_SPD_MAX / leftWeight; 
		}
		else
		{
			isShrinkage = true;
		}
	}



	public void move(Vector3 newDirection)
	{
		if (isCanAction() == false) return;
		if (isMagicBound == true) return;

		if (isInWater == true) {
			this.GetComponent<CharacterController> ().Move (0.5f * newDirection * Time.deltaTime * (Cpc.IDOSPEED / _nowWeight));
		} else {
			this.GetComponent<CharacterController> ().Move (newDirection * Time.deltaTime * (Cpc.IDOSPEED / _nowWeight));
		}

		//向き変更
		if (newDirection != Vector3.zero)
		{
			Quaternion q = Quaternion.LookRotation (newDirection);
			_myAvator.transform.rotation = Quaternion.RotateTowards (_myAvator.transform.rotation, q, Cpc.ROTATESPEED * Time.deltaTime);
		}

		if ((carryState == Cpc.CarryState.myCarry || carryState == Cpc.CarryState.emCarry) ) return;

		nowActionType = Cpc.ActionType.Run;
		playAnime();
	}

	public void attack()
	{
		float f = 0f;

		//ハンバーガーを食べさせる・放す
		if (isHumCarry == true)
		{
			f = Util.geoLength (_carryKumaEm.transform.position.x, _carryKumaEm.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);

			if(f < 2 && _carryKumaEm.GetComponent<kuma>().nowState == Cpc.KumaState.Putting)
			{
				StartCoroutine(feedBurger());
			}
			else
			{
				StartCoroutine(releaseBurger());
			}
			return;
		}

		if (isPlayer == true && isNetPlayer == false)
		{
			f = Util.geoLength (_carryKuma.transform.position.x, _carryKuma.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);
			float fe = Util.geoLength (_carryKumaEm.transform.position.x, _carryKumaEm.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);

			//持ち上げる
			if (f < Cpc.LEFT_DIS && carryState == Cpc.CarryState.noCarry && _carryKuma.GetComponent<kuma>().nowState != Cpc.KumaState.Lifting && _carryKuma.GetComponent<kuma>().nowState != Cpc.KumaState.Infested)
			{
				lift(Cpc.CarryState.myCarry);
				return;
			}
			else if (fe < Cpc.LEFT_DIS && carryState == Cpc.CarryState.noCarry && _carryKumaEm.GetComponent<kuma>().nowState != Cpc.KumaState.Lifting && _carryKumaEm.GetComponent<kuma>().nowState != Cpc.KumaState.Infested)
			{
				lift(Cpc.CarryState.emCarry);
				return;
			}
			else
			{
				//降ろす
				if ((carryState == Cpc.CarryState.myCarry || carryState == Cpc.CarryState.emCarry)  && nowActionType != Cpc.ActionType.Releasing)
				{
					releaseKuma ();
					Cpc.playSe(Cpc.SeType.pickup);
					return;
				}
			}
		}
		//ハンバーガーを掴む
		if (isPlayer == true && burgerObj_t != null)
		{
			f = Util.geoLength (burgerObj_t.transform.position.x, burgerObj_t.transform.position.z, this.gameObject.transform.position.x, this.gameObject.transform.position.z);
			if (f < 1.5f) {
				StartCoroutine(pickupBurger(burgerObj_t));
				return;
			}
		}

		//攻撃
		//もしハンバーガー持ってたら、手放す
		if (Cpc.myTeam == Cpc.Team.Red) {
			Transform hm = this.gameObject.transform.Find("hamburger_r(Clone)");
			if (hm != null)
			{
				StartCoroutine(pickupBurger(hm.gameObject));
			}
		} else {
			Transform hm = this.gameObject.transform.Find("hamburger_b(Clone)");
			if (hm != null)
			{
				StartCoroutine(pickupBurger(hm.gameObject));
			}
		}

		StartCoroutine(doAttackCol());

		Transform tr = nearestTarget();
		f = Util.geoLength (this.transform.localPosition.x, this.transform.localPosition.z, tr.localPosition.x, tr.localPosition.z);

		if (f < Cpc.ATK_RANGE*2)
		{
			if (isPlayer == true)
			{
				_myAvator.transform.LookAt (nearestTarget().FindChild("avator").transform);
			}
			else
			{
				this.transform.LookAt (nearestTarget().FindChild("avator").transform);
			}
		}

		if (myJob == Cpc.JobType.Knight) {
			Util.playEff ("attack", myTeam, this.transform, this.transform.position,0,_myAvator.transform.localEulerAngles.y,0f);
		} else if (myJob == Cpc.JobType.Sorcerer) {
			Util.playEff("attack_bk", Cpc.Team.NoTeam, this.transform, this.transform.position,0,_myAvator.transform.localEulerAngles.y,0f);
		} else if (myJob == Cpc.JobType.Priest) {
			Util.playEff("attack_wh", Cpc.Team.NoTeam, this.transform, this.transform.position,0,_myAvator.transform.localEulerAngles.y,0f);
		}
		nowActionType = Cpc.ActionType.Attack;
		playAnime(true);
		Cpc.playSe(Cpc.SeType.attack);

		if (myJob == Cpc.JobType.Sorcerer && isMagicAttacking == false && isMagicWait == false)
		{
			isMagicAttacking = true;
			isMagicWait = true;

			if (isPlayer == false) {
				_agent.speed = 0;
			}
			StartCoroutine (magicAttak ());
		}

		_timedelta = 0;

		StartCoroutine(attckEnd());

	}

	//アタックの遅延調整
	private IEnumerator doAttackCol()
	{
		if (Cpc.ONLINEMODE == true)
		{
			yield return new WaitForSeconds(0.4f);
		}
		else
		{
			yield return new WaitForSeconds(0.1f);
		}
		_atkCollider.enabled = true;

	}

	private IEnumerator pickupBurger(GameObject obj)
	{
		if (obj == null) yield return null;

		burgerObj = obj;
		burgerObj.GetComponent<BoxCollider> ().enabled = false;
		burgerObj.GetComponent<hamburger>().isFix = true;

		float fx = obj.transform.position.x - this.transform.position.x;
		float fz = obj.transform.position.z - this.transform.position.z;

		if (fx > 0) {fx = fx * -1;}
		if (fz > 0) {fz = fz * -1;}

		float j = -1f;
		while (j < 1f)
		{
			burgerObj.transform.position = new Vector3(burgerObj.transform.position.x+(fx/8f), j, burgerObj.transform.position.z+(fz/8f));

			j += 0.4f;
			yield return new WaitForSeconds (0.02f);
		}
		while (j > 1.4f)
		{
			burgerObj.transform.position = new Vector3(burgerObj.transform.position.x+(fx/8f), j, burgerObj.transform.position.z+(fz/8f));

			j -= 0.4f;
			yield return new WaitForSeconds (0.02f);
		}

		burgerObj.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+1.4f, this.transform.position.z);
		burgerObj.transform.localRotation = Quaternion.Euler (0, 0, 0);

		isHumCarry = true;
		Cpc.myState = Cpc.PLayerStatus.foodCarrying;

		if(isPlayer == true && isNetPlayer == false)
		{
			Cpc.uiC.showGuide(Cpc.GuideType.humburger);
		}

		_carryKumaEm.GetComponent<kuma>().showFeedBalloon();

		yield return null;
	}

	private IEnumerator feedBurger()
	{
		isHumCarry = false;

		Cpc.playSe(Cpc.SeType.eat);

		float fx = _carryKumaEm.transform.position.x - burgerObj.transform.position.x;
		float fz = _carryKumaEm.transform.position.z - burgerObj.transform.position.z;

		int i =0;
		while (i < 10)
		{
			burgerObj.transform.position = new Vector3(burgerObj.transform.position.x+(fx/8f), burgerObj.transform.position.y+0.1f, burgerObj.transform.position.z+(fz/8f));
			i++;
			yield return new WaitForSeconds (0.02f);
		}
		while (i < 12)
		{
			burgerObj.transform.position = new Vector3(burgerObj.transform.position.x+(fx/8f), burgerObj.transform.position.y-0.1f, burgerObj.transform.position.z+(fz/8f));
			i++;
			yield return new WaitForSeconds (0.02f);
		}
		Cpc.netC.SendHamPos(this.transform.position, burgerObj.GetComponent<PhotonView>().viewID);

		Destroy(burgerObj);
		Cpc.netC.SendHamDestroy(burgerObj.GetComponent<PhotonView>().viewID);

		Cpc.burgerNum[(int)myTeam]--;
		_carryKumaEm.GetComponent<kuma>().gainWeight();

		Cpc.myState = Cpc.PLayerStatus.normal;

		_carryKumaEm.GetComponent<kuma>().hideFeedBalloon();

		yield return null;
	}

	private IEnumerator releaseBurger()
	{
		isHumCarry = false;
		int iCount = 0;

		while (iCount < 10)
		{
			burgerObj.transform.position = new Vector3(burgerObj.transform.position.x+_myAvator.transform.localRotation.y*0.14f, burgerObj.transform.position.y+0.3f, burgerObj.transform.position.z+0.04f*(1f-_myAvator.transform.localRotation.y));

			iCount++;
			yield return new WaitForSeconds (0.01f);
		}

		while (burgerObj.transform.position.y > -4.9f)
		{
			burgerObj.transform.position = new Vector3(burgerObj.transform.position.x+_myAvator.transform.localRotation.y*0.14f, burgerObj.transform.position.y-0.3f, burgerObj.transform.position.z+0.04f*(1f-_myAvator.transform.localRotation.y));
			iCount++;
			yield return new WaitForSeconds (0.01f);
		}
		Cpc.netC.SendHamPos(this.transform.position, burgerObj.GetComponent<PhotonView>().viewID);

		yield return new WaitForSeconds (0.5f);
		burgerObj.GetComponent<BoxCollider> ().enabled = true;
		burgerObj.GetComponent<hamburger>().isFix = false;


		_carryKumaEm.GetComponent<kuma>().hideFeedBalloon();

		Cpc.myState = Cpc.PLayerStatus.normal;

	}

	private IEnumerator magicAttak()
	{
//		Cpc.playSe(Cpc.SeType.magic);

		Util.playEff("magicBk_ring",  Cpc.Team.NoTeam, _mgcCollider.transform, Vector3.zero ,-90f,0f,0f, true, 0.8f, 0.05f);
		yield return new WaitForSeconds (0.3f);
		_mgcCollider.enabled = true;

		yield return new WaitForSeconds (Cpc.MASIC_STIFFNESS);
		//魔法後の硬直解除
		isMagicAttacking = false;
		if (isPlayer == false) {
			_agent.speed =  Cpc.COM_SPD_MAX / leftWeight;
		}

		yield return new WaitForSeconds (Cpc.MASIC_INTERVAL);
		//再魔法許可
		isMagicWait = false;

		yield return null;
	}

	//被魔法攻撃
	public void magicBound(bool isNet=false)
	{
//		Debug.Log("<<<<< magicBound >>>>>>>");

		isMagicBound = true;

		if (isNet == false)
		{
			Cpc.netC.SendMagic(_charaInfo.playerNo);
		}
		StartCoroutine (magicBounded());
	}

	private IEnumerator magicBounded()
	{
		if (isPlayer == false && isNetPlayer == false) {
			_agent.speed = 0;
			_timePosCk = 0f;
		}
		Util.playEff("magicBK_attack", Cpc.Team.NoTeam, this.transform, this.transform.position,-90f,0f,0f);
		yield return new WaitForSeconds (0.3f);

		int i = 0;
		while(i != 15)
		{
			Util.playEff("magicBK_ball", Cpc.Team.NoTeam, this.transform, this.transform.position,0f,0f,0f);
			yield return new WaitForSeconds (0.4f);
			i++;
		}

		isMagicBound = false;
		if (isPlayer == false && isNetPlayer == false) {
			_agent.speed = Cpc.COM_SPD_MAX / leftWeight;
		}

		yield return null;
	}


	public void damaged(int playerNo, int atk, bool isByNet = false)
	{
//		Debug.Log("isDamaging :" + isDamaging);
//		Debug.Log("isByNet :" + isByNet);
//		Debug.Log("_charaInfo.isDead :" + _charaInfo.isDead);

		if (isDamaging == true && isByNet == false) return;
		if (_charaInfo.isDead == true) return;


//		Debug.Log("だめじ ########################");

		isDamaging = true;
		nowActionType = Cpc.ActionType.Damaged;

		_cui.damaged(atk);
		StartCoroutine(damageEnd(playerNo, atk, isByNet));
	}

	public void showRcvEffect()
	{
		if (isRecoverEffect == true)
			return;

//		Cpc.playSe(Cpc.SeType.heal);

		isRecoverEffect = true;
		StartCoroutine(recoverEffect());

	}
	private IEnumerator recoverEffect()
	{
		Util.playEff("magic_circle",  Cpc.Team.NoTeam, this.transform, this.transform.position,90f,0f,0f);

		yield return new WaitForSeconds (0.5f);
		isRecoverEffect = false;

		yield return null;
	}

	public void recovery(int cvr)
	{
		if (isRecovering == true)
			return;

		isRecovering = true;
		_cui.damaged(cvr, true);
		StartCoroutine(recoverEnd(cvr));
	}

	private IEnumerator damageEnd(int atkPlayer, int atk, bool isByNet)
	{
		int iCnt = 0;

		yield return new WaitForSeconds(0.1f);

		playAnime(true);

		while(iCnt < 20)
		{
			if (myJob == Cpc.JobType.Knight)
			{
				if ((iCnt%2) == 0)
				{
					_mySkin.materials[3].color = Color.red;
				}
				else
				{
					_mySkin.materials[3].color = Color.white;
				}
			}
			else
			{
				if ((iCnt%2) == 0)
				{
					_mySkin.materials[0].color = Color.red;
				}
				else
				{
					_mySkin.materials[0].color = Color.white;
				}
			}
			iCnt++;
			yield return new WaitForSeconds(0.02f);
		}

		isDamaging = false;

//		if (_charaInfo.isDead == false)
//		{
		if (isByNet == false)
		{
			Cpc.netC.SendDamaged(_charaInfo.playerNo, atkPlayer, atk);
		}
//		}
		if (_charaInfo.damaged(atk) == true)
		{
			_cui.damaged(999);
			//to die
			toDie(atkPlayer);
		}
		else
		{
			//人から攻撃を受けるとアタッカーに
			if (isPlayer == false && isNetPlayer == false)
			{
				if (myJob == Cpc.JobType.Knight &&  carryState == Cpc.CarryState.noCarry)
				{
//					Debug.Log("atkPlayer : " + atkPlayer);
					if (atkPlayer == 0)
					{
						decideAI((int)Cpc.AIType.attcker);
					}
					else if (Cpc.ONLINEMODE == true)
					{
						if (atkPlayer <= PhotonNetwork.room.PlayerCount)
						{
							decideAI((int)Cpc.AIType.attcker);
						}
					}
				}
			}
		}

		_mySkin.materials[3].color = Color.white;
	}

	private IEnumerator recoverEnd(int cvr)
	{
		int iCnt = 0;

		yield return new WaitForSeconds(0.1f);

		while(iCnt < 20)
		{
			if (myJob == Cpc.JobType.Knight)
			{
				if ((iCnt%2) == 0)
				{
					_mySkin.materials[3].color = Color.green;
				}
				else
				{
					_mySkin.materials[3].color = Color.white;
				}
			}
			else
			{
				if ((iCnt%2) == 0)
				{
					_mySkin.materials[0].color = Color.green;
				}
				else
				{
					_mySkin.materials[0].color = Color.white;
				}
			}
			iCnt++;
			yield return new WaitForSeconds(0.02f);
		}
		isRecovering = false;
		_charaInfo.damaged (cvr); //回復
		_mySkin.materials[3].color = Color.white;
	}



//	private IEnumerator attckEnd(bool isAdjust=false)
	private IEnumerator attckEnd()
	{
		yield return new WaitForSeconds(0.5f);
		/*
		//攻撃回転後の補正
		if (isAdjust == true)
		{
			if (isPlayer == true)
			{
				_myAvator.transform.localRotation = Quaternion.Euler(0, _myAvator.transform.localRotation.y, _myAvator.transform.localRotation.z);
			}
			else
			{
				this.transform.localRotation = Quaternion.Euler(0, this.transform.localRotation.y, this.transform.localRotation.z);
			}
		}
		*/

		_atkCollider.enabled = false;
		_mgcCollider.enabled = false;
	}

	public void jump(Vector3 newDirection)
	{
		if (isCanAction() == false) return;
		if ((carryState == Cpc.CarryState.myCarry || carryState == Cpc.CarryState.emCarry) ) return;

		_nowPosYBk = _nowPosY;
		nowActionType = Cpc.ActionType.Jump;

		isJumped = false;
		float tx = Math.Abs(newDirection.x);
		float tz = Math.Abs(newDirection.z);

		if (tx < 0.1f && tz < 0.1f)
		{

		}
		else
		{
			//向き変更
			if (newDirection != Vector3.zero)
			{
				_myAvator.transform.rotation = Quaternion.Euler(0,-90f+Util.GetAim(Vector2.zero, new Vector2(newDirection.z,newDirection.x)),0);
			}

			if (newDirection.x >= 0)
			{
				jumpX = -1 * tx / (tx + tz);
			}
			else
			{
				jumpX = tx / (tx + tz);
			}

			if (newDirection.z >= 0)
			{
				jumpZ = -1 * tz / (tx + tz);
			}
			else
			{
				jumpZ = tz / (tx + tz);
			}

		}
		Cpc.playSe(Cpc.SeType.jump);

		_nowPosX = this.transform.localPosition.x;
		_nowPosZ = this.transform.localPosition.z;

		playAnime(true);
	}

	public void idle()
	{
		if (nowActionType == Cpc.ActionType.Idle) return;

		//被ダメージは回復するが、死亡中は除く
		if (isCanAction() == true || nowActionType == Cpc.ActionType.Damaged)
		{
			if (isDead == false)
			{
				nowActionType = Cpc.ActionType.Idle;
				playAnime();
			}
		}
	}

	public void playAnime(bool isOnece = false, int netNo = -1)
	{
		if (isOnece == true || _bkActionType != nowActionType)
		{
			//[ネット対応]
			if (netNo != -1)
			{
				try{
					_nowAnime.clip = _animeClips[netNo];
				}catch(Exception e)
				{
					Debug.Log("_animeClips ERROR :" + netNo + "E " + e.ToString());
				}

			}
			else
			{
				int animeNo = -1;

				if (isDead == true)
				{
					_nowAnime.clip = _animeClips[(int)Cpc.ActionType.Dead];
					animeNo = (int)Cpc.ActionType.Dead;
				}
				else
				{
					if ((int)nowActionType > 12)
					{
						//LiftUPがない
						return;
					}
					if (nowActionType == Cpc.ActionType.Canon) {
						_nowAnime.clip = _animeClips[(int)Cpc.ActionType.Eat];
						animeNo = (int)Cpc.ActionType.Eat;
					} else {
						try{
							_nowAnime.clip = _animeClips[(int)nowActionType];
						} catch(Exception e)
						{
							Debug.Log("_animeClips ERROR :" + nowActionType + "E " + e.ToString());
						}
						animeNo = (int)nowActionType;
					}
				}

				Cpc.netC.SendAnime(_charaInfo.playerNo, animeNo, isOnece);
			}
			_nowAnime.Play();

		}
		_bkActionType = nowActionType;
	}

	//	public void lift(float weight, kuma ckuma)
	public void lift(Cpc.CarryState cState)
	{

		//		isCarry = true;
		if (cState == Cpc.CarryState.myCarry)
		{
			if (_carryKuma == null) return;
			if (_carryKuma.GetComponent<kuma>().nowState == Cpc.KumaState.Gaining) return;
			if (_carryKuma.GetComponent<kuma>().nowState == Cpc.KumaState.Infested) return;
			if (_carryKuma.GetComponent<kuma>().nowState == Cpc.KumaState.Lifting) return;
			_carryKuma.GetComponent<kuma>().doLift(this.gameObject);

			leftWeight = _carryKuma.GetComponent<kuma>().nowWeight;

			if(isPlayer == true && isNetPlayer == false)
			{
				Cpc.uiC.showGuide(Cpc.GuideType.pickup);
			}
			Cpc.pickingEvent (myTeam);
		}
		else
		{
			if (_carryKumaEm == null) return;
			if (_carryKumaEm.GetComponent<kuma>().nowState == Cpc.KumaState.Gaining) return;
			if (_carryKumaEm.GetComponent<kuma>().nowState == Cpc.KumaState.Infested) return;
			if (_carryKumaEm.GetComponent<kuma>().nowState == Cpc.KumaState.Lifting) return;
			_carryKumaEm.GetComponent<kuma>().doLift(this.gameObject);
			leftWeight = _carryKumaEm.GetComponent<kuma>().nowWeight;

			if(isPlayer == true && isNetPlayer == false)
			{
				Cpc.uiC.showGuide(Cpc.GuideType.epickup);
			}
		}
		//CUIの位置調整
		_cuiRect [0].GetComponent<RectTransform>().localPosition = new Vector2(_cuiBk.x+1.6f, _cuiBk.y-1f);
		_cuiRect [1].GetComponent<RectTransform>().localPosition = new Vector2(_cuiBk.x+1.6f, _cuiBk.y-1f);

		_nowSupportNum = 0;
		carryState = cState;


		_nowWeight = leftWeight;

		nowActionType = Cpc.ActionType.LiftUp;

		Cpc.playSe(Cpc.SeType.pickup);
	}

	public void changeState(Cpc.ActionType type)
	{
		if (isPlayer == true && isNetPlayer == false)
		{
			if (type == Cpc.ActionType.Carry) {

				if (carryState == Cpc.CarryState.myCarry)
				{
					Cpc.myState = Cpc.PLayerStatus.kumaCarrying;
				}
			} else if (type == Cpc.ActionType.Idle) {
				Cpc.myState = Cpc.PLayerStatus.normal;
			}
		}
		else
		{
			if (type == Cpc.ActionType.Carry)
			{
				_agent.speed = 0;
				isShrinkage = true;
				StartCoroutine(clearShrinkage());
			}
		}

		nowActionType = type;
	}

	private bool isCanAction()
	{
		if (nowActionType == Cpc.ActionType.Jump) return false;
		if (nowActionType == Cpc.ActionType.LiftUp) return false;
		if (nowActionType == Cpc.ActionType.Releasing) return false;
		if (nowActionType == Cpc.ActionType.Damaged) return false;
		if (nowActionType == Cpc.ActionType.ChageJob) return false;
		if (nowActionType == Cpc.ActionType.Canon) return false;
		if (nowActionType == Cpc.ActionType.Diying) return false;
		if (isMagicAttacking == true) return false;

		if (isDead == true) return false;

		return true;
	}

	private void releaseKuma()
	{
		nowActionType = Cpc.ActionType.Releasing;

		//kuma スクリプトから移動
//		changeState(Cpc.ActionType.Idle);

		if (carryState == Cpc.CarryState.myCarry)
		{
			_carryKuma.GetComponent<kuma>().released ();
		}
		else if (carryState == Cpc.CarryState.emCarry)
		{
			_carryKumaEm.GetComponent<kuma>().released ();
		}
		//kumaスクリプトから移動
//		changeState(Cpc.ActionType.Carry);

		carryState = Cpc.CarryState.noCarry;

		leftWeight = 1f;
		_nowWeight = leftWeight;

		if (isPlayer == false && isNetPlayer == false)
		{
			_agent.speed = Cpc.COM_SPD_MAX / leftWeight; 
		}

		_cuiRect [0].GetComponent<RectTransform>().localPosition = _cuiBk;
		_cuiRect [1].GetComponent<RectTransform>().localPosition = _cuiBk;

	}

	private void toDie(int atkPlayer)
	{
		if (isDead == true) return;

		nowActionType = Cpc.ActionType.Diying;
		_shadow[0].SetActive(false);
		_shadow[1].SetActive(false);

		if ((carryState == Cpc.CarryState.myCarry || carryState == Cpc.CarryState.emCarry) )
		{
			if (carryState == Cpc.CarryState.myCarry)
			{
				_carryKuma.GetComponent<kuma>().released ();
			}
			else
			{
				_carryKumaEm.GetComponent<kuma>().released ();
			}

			carryState = Cpc.CarryState.noCarry;
			leftWeight = 1f;
			_nowWeight = leftWeight;
		}

		/*
		if (myTeam == Cpc.Team.Red)
		{
			Cpc.charaPos_r.Remove (this.gameObject.transform);
		}
		else
		{
			Cpc.charaPos_b.Remove (this.gameObject.transform);
		}
		*/

		if (isPlayer == false && isNetPlayer == false)
		{
			if (_agent != null) {
				_agent.enabled = false;
			}
			//COM AI数減らす
			if (myTeam == Cpc.Team.Red) {
				Cpc.AICounter_r [(int)_nowAI]--;
				if (Cpc.AICounter_r [(int)_nowAI] < 0) {
					Cpc.AICounter_r [(int)_nowAI] = 0;
				}
			} else {
				Cpc.AICounter_b [(int)_nowAI]--;
				if (Cpc.AICounter_b [(int)_nowAI] < 0) {
					Cpc.AICounter_b [(int)_nowAI] = 0;
				}
			}
		}
		isDead = true;
		playAnime(true);
		Cpc.uiC.showName(_charaInfo);

		//kill数カウントアップ
		if (Cpc.isMaster == true)
		{
			if (myTeam == Cpc.Team.Red)
			{
				Cpc.killNum[(int)Cpc.Team.Blue]++;
//				Debug.Log ("kill数カウントアップ Blue:" + Cpc.killNum[(int)Cpc.Team.Blue]);
				Cpc.netC.SendKillNum (Cpc.Team.Blue);
				Cpc.uiC.showKillNum(Cpc.Team.Blue);

			}
			else
			{
				Cpc.killNum[(int)Cpc.Team.Red]++;
//				Debug.Log ("kill数カウントアップ Red:" + Cpc.killNum[(int)Cpc.Team.Red]);
				Cpc.netC.SendKillNum (Cpc.Team.Red);
				Cpc.uiC.showKillNum(Cpc.Team.Red);
			}
		}

		Cpc.killNumC[atkPlayer]++;

		if (Cpc.ONLINEMODE == true)
		{
//			Debug.Log("$$$$$$$$$$ " + atkPlayer + " |||| " + (PhotonNetwork.player.ID-1));

			if (atkPlayer != PhotonNetwork.player.ID-1)
			{
				//			Debug.Log ("kill数カウントアップ C:" + (atkPlayer) + " >>>>" + Cpc.killNumC[atkPlayer]);

				//自分以外の被kill情報は送らない
				if (_charaInfo.playerNo == PhotonNetwork.player.ID-1)
				{
					Cpc.netC.SendKillNumC (atkPlayer);
				}
				//MasterはComの被kill情報を送る
				else if (Cpc.isMaster == true && isPlayer == false && isNetPlayer == false)
				{
					Cpc.netC.SendKillNumC (atkPlayer);
				}
			}
		}

		StartCoroutine(dieEnd());
	}

	private IEnumerator dieEnd()
	{

		//ハンバーガーを放す
		if (isHumCarry == true)
		{
			StartCoroutine(releaseBurger());
		}

		yield return new WaitForSeconds(1.2f);
//		Util.playEff("lost", myTeam, this.transform, this.transform.position,_myAvator.transform.localRotation);
		Util.playEff("lost", myTeam, this.transform, this.transform.position,0f,_myAvator.transform.localEulerAngles.y,0f);

		yield return new WaitForSeconds(0.6f);
		if (isPlayer == false) {
			yield return new WaitForSeconds (1f);
			Cpc.charaNum[(int)myTeam]--;
		} else {
			Cpc.myState = Cpc.PLayerStatus.dead;
		}

		if(isPlayer == true)
		{
			Cpc.uiC._mapCamera.enabled = true;
		}

		yield return new WaitForSeconds(0.6f);
		toDestory();

//		Debug.Log("dieEnd : " + _charaInfo.toString());

		Cpc.playerRoster [_charaInfo.playerNo] = _charaInfo;

	}

	//死んだ状態
	private void toDestory()
	{
		if(isPlayer == true)
		{
			this.gameObject.transform.FindChild("CameraB").gameObject.SetActive(false);
		}
		releaseKuma();
		this.transform.localPosition = new Vector3(0f, -100f,0f);
	}

	//復活
	public void toAlive(Cpc.CharaInfo charac)
	{
		if (isPlayer == false && isNetPlayer == false)
		{
			isReadyAgent = false;
			this.gameObject.GetComponent<CharacterController>().enabled = true;
			StartCoroutine (setAgent());

		}

		if (Cpc.ONLINEMODE == true)
		{
			//ネットプレイヤー以外か、自分以外のネットプレイヤーの送信
//			Debug.Log("Comの復活送信 >>>> " + charac.name + "|" + isPlayer + "|" + isNetPlayer);
//			if (isPlayer == false || isNetPlayer == true)
			if (isNetPlayer == false || (isNetPlayer == true && (_charaInfo.playerNo != (PhotonNetwork.player.ID-1))))
			{
				Cpc.netC.SendComRestore(charac.playerNo, this.GetComponent<PhotonView>().viewID);
			}
		}

		_charaInfo = charac;
		myJob = Cpc.JobType.Knight;
		init();
	}

	//復活・再設定[Net用]
	public void toAliveByNet()
	{
		Debug.Log("toAliveByNet :" + _charaInfo.name);

		myJob = Cpc.JobType.Knight;
		init();
	}


	//クマサポート範囲判定
	private bool isInCarryingKuma()
	{
		if (_myKuma != null)
		{
			float f = Util.geoLength (this.transform.position.x, this.transform.position.z, _myKuma.transform.position.x, _myKuma.transform.position.z);

			if (_myKuma.GetComponent<kuma>().nowState == Cpc.KumaState.Lifting && f < Cpc.SUPPORT_RANGE)
			{
				if (_eff_support.activeSelf == false && carryState == Cpc.CarryState.noCarry)
				{
					_eff_support.SetActive(true);
					_myKuma.GetComponent<kuma>()._nowSupport++;
				}
				return true;
			}
		}

		if (_eff_support.activeSelf == true)
		{
			_eff_support.SetActive(false);
			_myKuma.GetComponent<kuma>()._nowSupport--;
		}
		return false;
	}


}
