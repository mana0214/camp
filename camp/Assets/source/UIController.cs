﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : SwipeDetector
{
	//ポインター関連
    [SerializeField]
    private GameObject pointer;
    [SerializeField]
    private GameObject pointerF;
    [SerializeField]
    private Canvas canvasT;

	[SerializeField]
	private Camera mainCamera;
	[SerializeField]
	public Camera _mapCamera;

	//メッセージ
	[SerializeField]
	private GameObject _msgG;
	[SerializeField]
	private Text _msgTxt;
	[SerializeField]
	private Image _msgBkColor;

	//アウトゲームUI関連
	[SerializeField]
	private GameObject _outGameUI;
	[SerializeField]
	private GameObject[] _btnInGames;

	//オープニング関連
	[SerializeField]
	private GameObject _opening;

	//リザルト関連
	[SerializeField]
	private GameObject[] _resultIcon;
	[SerializeField]
	private Sprite[] _teamResultIcopSp;
	[SerializeField]
	private Image[] _teamResultIcon;
	[SerializeField]
	private Text[] _resutlText;
	[SerializeField]
	private Text[] _killText;

	//インゲームUI関連
	[SerializeField]
	private GameObject _inGameUI;
	[SerializeField]
	private GameObject[] _bearIcon;
	[SerializeField]
	private Text _gameTimer;
	[SerializeField]
	public GameObject _fukiRed;
	[SerializeField]
	private Text _countRed;
	[SerializeField]
	public GameObject _fukiBlue;
	[SerializeField]
	private Text _countBlue;
	[SerializeField]
	private Text _starC;

	//カットイン
	[SerializeField]
	private GameObject[] _cutInG;
	[SerializeField]
	private GameObject[] _cutInImg;


	//フェード
	[SerializeField]
	private GameObject _fade;
	[SerializeField]
	private GameObject _fadeTxt;

	[SerializeField]
	private GameObject[] _message;
	[SerializeField]
	private GameObject _resultG;

	[SerializeField]
	private Image _naviIcon;
	[SerializeField]
	private Sprite[] _naviIconSp;

	[SerializeField]
	private GameObject[] _onLineG;
	[SerializeField]
	private GameObject[] _onLineIConR;
	[SerializeField]
	private GameObject[] _onLineIConB;
	[SerializeField]
	private Sprite[] _onLineIcon;
	[SerializeField]
	private GameObject _guide;

    private Vector2 touchPoint;
    private float _tapTime;

	private int _sitCount_r = Cpc.KUMACOUNT;
	public int sitCount_r
	{
		set{
			_sitCount_r = value;
			Cpc.netC.SendKumaCount(_sitCount_r, Cpc.Team.Red);
		}
		get{
			return _sitCount_r;
		}
	}

	private int _sitCount_b = Cpc.KUMACOUNT;
	public int sitCount_b
	{
		set{
			_sitCount_b = value;
			Cpc.netC.SendKumaCount(_sitCount_b, Cpc.Team.Blue);
		}
		get{
			return _sitCount_b;
		}
	}
	private float _kDistS_r = 1f;
	private float _kDistS_b = 1f;
	private bool _clicked;
	private bool isBlinking;
	private bool isFading;
	private float _reviveTime;
	private float _fadeF;

    void OnEnable ()
    {
		//ポインター初期化
//		canvasX = canvasT.GetComponent<RectTransform> ().sizeDelta.x;
//		canvasY = canvasT.GetComponent<RectTransform> ().sizeDelta.y;
		pointer.GetComponent<Image>().color = new Color(255, 255, 255, 0);
		pointerF.GetComponent<Image>().color = new Color(255, 255, 255, 0);
		_clicked = false;


    }

    void Update ()
    {
		if (isFading == true)
		{
			_reviveTime += Time.deltaTime;

			if (_reviveTime < (Cpc.TIME_REVIVE / 2f)) {

				_fadeF += ((Cpc.TIME_REVIVE * 0.12f) / 100f);
				_fade.GetComponent<Image> ().color = new Color (0f, 0f, 0f, _fadeF);
			} else {

				if (_fadeTxt.activeSelf == false)
				{
					_fadeTxt.SetActive(true);
				}
				_fadeF -= ((Cpc.TIME_REVIVE * 0.12f) / 100f);
				_fade.GetComponent<Image> ().color = new Color (0f, 0f, 0f, _fadeF);

				if (_fadeF < 0.1f) {
					isFading = false;
					_fade.SetActive (false);
				}
			}
		}

		if (Cpc.myState == Cpc.PLayerStatus.dead) return;
		
        checkSwipe ();

		//バトル中処理
		if (Cpc.appState == Cpc.AppState.battle || Cpc.appState == Cpc.AppState.countdown)
		{
			//タッチコントローラー
	        if (tp == TouchPhase.Began)
	        {
	 			pointer.GetComponent<Image>().color = new Color(255, 255, 255, 1);
				pointerF.GetComponent<Image>().color = new Color(255, 255, 255, 1);

				touchPoint = Util.changeCanvasPos();

	            pointer.transform.localPosition = new Vector3 (touchPoint.x, touchPoint.y, 0);
	            pointerF.transform.localPosition = new Vector3 (touchPoint.x, touchPoint.y, 0);

	        }
			else if (isIntouch == true)
	        {
				_tapTime += Time.deltaTime;

				Vector2 tpm = Util.changeCanvasPos();
				pointer.transform.localPosition = new Vector3 (tpm.x, tpm.y, 0);

				float px = pointer.transform.localPosition.x - pointerF.transform.localPosition.x;
				float py = pointer.transform.localPosition.y - pointerF.transform.localPosition.y;

				if (Cpc.myTeam == Cpc.Team.Blue)
				{
					px = px*-1;
					py = py*-1;
				}

				float MAX_F = 200f;

				if (px > MAX_F)
				{
					px = MAX_F;
				}
				if (py > MAX_F)
				{
					py = MAX_F;
				}
				if (px < -1*MAX_F)
				{
					px = -1*MAX_F;
				}
				if (py < -1*MAX_F)
				{
					py = -1*MAX_F;
				}
				Cpc.actChara(Cpc.ActionType.Run, new Vector3 (px, 0, py));
			}
	        else if (tp == TouchPhase.Ended)
	        {
	            //ジャンプ・攻撃 判定
				if (_tapTime < 0.5f)
				{
					Vector2 pos = Util.changeCanvasPos();

	                float f1 = Util.geoLength (touchPoint.x, touchPoint.y, pos.x, pos.y);
					if (f1 > Cpc.JUNMPRANGE)
	                {
						if (Cpc.myTeam == Cpc.Team.Red)
						{
							Cpc.actChara(Cpc.ActionType.Jump, new Vector3 (pointer.transform.localPosition.y - touchPoint.y , 0, touchPoint.x - pointer.transform.localPosition.x));
						}
						else
						{
							Cpc.actChara(Cpc.ActionType.Jump, new Vector3 (touchPoint.y - pointer.transform.localPosition.y , 0, pointer.transform.localPosition.x - touchPoint.x));
						}

	                }
	                else
	                {
						Cpc.actChara(Cpc.ActionType.Attack, Vector3.zero);
	                }
	            }
				else
				{
					Cpc.actChara(Cpc.ActionType.Idle, Vector3.zero);
				}
				_tapTime = 0f;
				pointer.GetComponent<Image>().color = new Color(255, 255, 255, 0);
				pointerF.GetComponent<Image>().color = new Color(255, 255, 255, 0);
	        }
		}


    }

	//インゲームUI初期化
	public void initIngameUI(float dist_r=0, float dist_b=0)
	{
		_mapCamera.enabled = false;

		_inGameUI.SetActive(true);

		showGameTime(Cpc.TIMEMAX);
		if (dist_r !=0 && dist_b != 0)
		{
			_kDistS_r = dist_r;
			_kDistS_b = dist_b;
		}

		_gameTimer.text = "";
		_countRed.text = sitCount_r+"";
		_countBlue.text = sitCount_b+"";
		_starC.text = 0+"";
		_bearIcon[(int)Cpc.Team.Red].GetComponent<RectTransform>().localPosition = new Vector2(0, -114);
		_bearIcon[(int)Cpc.Team.Blue].GetComponent<RectTransform>().localPosition = new Vector2(0, -114);
		_onLineG[0].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(140f, -124f, 0);
		_onLineG[1].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(1870f, -124f, 0);

		_fukiRed.gameObject.SetActive(false);
		_fukiBlue.gameObject.SetActive(false);

		for(int i=0; i<_message.Length; i++)
		{
			_message[i].SetActive(false);
		}
		_resultG.SetActive(false);

		showMessage(Cpc.AppState.battleStart);
	}

	public void initName()
	{
//		Debug.Log("############## initName ##################");

		//ネーム情報初期化

		if (Cpc.ONLINEMODE == false)
		{
			_onLineIConR[0].SetActive(true);
			_onLineIConB[0].SetActive(false);
			for (int i=1; i<10; i++)
			{
				_onLineIConR[i].SetActive(false);
				_onLineIConB[i].SetActive(false);
			}
		}
		else
		{
			int iR = 0;
			int iB = 0;
			for (int i=0; i<Cpc.MaxChara*2; i++)
			{
//				Debug.Log("TEAM :" + Cpc.playerRoster[i].name + "  TEAM :" + Cpc.playerRoster[i].team+ "  isPlayer :" + Cpc.playerRoster[i].isPlayer);

				if (Cpc.playerRoster[i].team == Cpc.Team.Red)
				{
					if (Cpc.playerRoster[i].isPlayer == false)
					{
						_onLineIConR[iR].SetActive(false);
					}
					else
					{
						_onLineIConR[iR].SetActive(true);
					}
					iR++;
				}
				else
				{
					if (Cpc.playerRoster[i].isPlayer == false)
					{
						_onLineIConB[iB].SetActive(false);
					}
					else
					{
						_onLineIConB[iB].SetActive(true);
					}
					iB++;
				}
			}
		}
	}

	//ネーム表示
	public void showName(Cpc.CharaInfo info, bool isShow=false)
	{

		if (info.team == Cpc.Team.Red)
		{
//			Debug.Log("info.playerNo R:" + info.playerNo + " | " + info.name + " | " + info.isDead );
			if (isShow == true)
			{
//				Debug.Log("SHOW!!!!!! info.playerNo R:" + info.playerNo + " | " + info.name);

				_onLineIConR[info.playerNo/2].SetActive(true);
				_onLineIConR[info.playerNo/2].transform.FindChild("Name").GetComponent<Text>().text = info.name;
			}

			if (info.isDead == true)
			{
				_onLineIConR[info.playerNo/2].transform.FindChild("Name").GetComponent<Text>().color = Util.rgbColor(0,0,0);
			}
			else
			{
				_onLineIConR[info.playerNo/2].transform.FindChild("Name").GetComponent<Text>().color = Util.rgbColor(238,158,158);
			}
		}
		else
		{
//			Debug.Log("info.playerNo :B" + info.playerNo + " | " + info.name + " | " + info.isDead );
			if (isShow == true)
			{
				_onLineIConB[info.playerNo/2].SetActive(true);
				_onLineIConB[info.playerNo/2].transform.FindChild("Name").GetComponent<Text>().text = info.name;
			}

			if (info.isDead == true)
			{
				_onLineIConB[info.playerNo/2].transform.FindChild("Name").GetComponent<Text>().color = Util.rgbColor(0,0,0);
			}
			else
			{
				_onLineIConB[info.playerNo/2].transform.FindChild("Name").GetComponent<Text>().color = Util.rgbColor(139,207,255);
			}
		}
	}

	//メッセージ表示
	public void showMessage(Cpc.AppState state)
	{
		if (state == Cpc.AppState.battleEnd)
		{
			Cpc.changeAppState(Cpc.AppState.battleTimeUp);

			if (Cpc.myTeam == Cpc.Team.Red)
			{
				if (sitCount_r < sitCount_b)
				{
					Cpc.changeAppState(Cpc.AppState.battleWin);
				}
				else if (sitCount_r > sitCount_b)
				{
					Cpc.changeAppState(Cpc.AppState.battleLose);
				}
			}
			else
			{
				if (sitCount_r < sitCount_b)
				{
					Cpc.changeAppState(Cpc.AppState.battleLose);
				}
				else if (sitCount_r > sitCount_b)
				{
					Cpc.changeAppState(Cpc.AppState.battleWin);
				}
			}
		}

		for(int i=0; i<_message.Length; i++)
		{
			_message[i].SetActive(false);
		}

		float waits = 2f;
		switch(Cpc.appState)
		{
		case Cpc.AppState.battleStart:
			waits = 2f;
			_message[0].SetActive(true);
			break;
		case Cpc.AppState.battleWin:
			_message[1].SetActive(true);
			waits = 2f;
			break;
		case Cpc.AppState.battleLose:
			_message[2].SetActive(true);
			waits = 2f;
			break;
		case Cpc.AppState.battleTimeUp:
			_message[3].SetActive(true);
			waits = 2f;
			break;
		}

		StartCoroutine(chageMessage(Cpc.appState, waits));
	}

	//メッセージ表示変更
	private IEnumerator chageMessage(Cpc.AppState state, float waits)
	{
		yield return new WaitForSeconds(waits);
		for(int i=0; i<_message.Length; i++)
		{
			_message[i].SetActive(false);
		}

		switch(state)
		{
		case Cpc.AppState.battleStart:
			Cpc.changeAppState(Cpc.AppState.battle);
			break;
		case Cpc.AppState.battleWin:
		case Cpc.AppState.battleLose:
		case Cpc.AppState.battleTimeUp:
			showResult();
			break;
		}

		yield return null;
	}

	public void showGameTime(int gameTime)
	{
		_gameTimer.text = ((gameTime / 60) % 60).ToString("D2") + ":" + (gameTime % 60).ToString("D2");
	}

	//リザルト表示
	private void showResult()
	{
		_resultIcon[0].SetActive(false);
		_resultIcon[1].SetActive(false);
		_resultIcon[2].SetActive(false);
		_onLineG[0].gameObject.SetActive(false);
		_onLineG[1].gameObject.SetActive(false);

		int r = 2;

		if (Cpc.myTeam == Cpc.Team.Red)
		{
			if (sitCount_r < sitCount_b)
			{
				r = 0;
			}
			else if  (sitCount_r > sitCount_b)
			{
				r = 1;
			}
			else
			{
				if (Cpc.killNum[(int)Cpc.Team.Red] > Cpc.killNum[(int)Cpc.Team.Blue])
				{
					r = 0;
				}
				else if (Cpc.killNum[(int)Cpc.Team.Red] < Cpc.killNum[(int)Cpc.Team.Blue])
				{
					r = 1;
				}
			}
		}
		else
		{
			if (sitCount_b < sitCount_r)
			{
				r = 0;
			}
			else if  (sitCount_b > sitCount_r)
			{
				r = 1;
			}
			else
			{
				if (Cpc.killNum[(int)Cpc.Team.Blue] > Cpc.killNum[(int)Cpc.Team.Red])
				{
					r = 0;
				}
				else if (Cpc.killNum[(int)Cpc.Team.Blue] < Cpc.killNum[(int)Cpc.Team.Red])
				{
					r = 1;
				}
			}
		}

		_resultIcon[r].SetActive(true);

		if (r == 0)
		{
			Cpc.soC.changeBgm(2);
		}
		else
		{
			Cpc.soC.changeBgm(0);
		}

		if (Cpc.myTeam == Cpc.Team.Red)
		{
			_resutlText[0].text = Cpc.killNum[(int)Cpc.Team.Red]+"";
			_resutlText[1].text = Cpc.killNum[(int)Cpc.Team.Blue]+"";
			_teamResultIcon[0].sprite = _teamResultIcopSp[0];
			_teamResultIcon[1].sprite = _teamResultIcopSp[1];
		}
		else
		{
			_resutlText[0].text = Cpc.killNum[(int)Cpc.Team.Blue]+"";
			_resutlText[1].text = Cpc.killNum[(int)Cpc.Team.Red]+"";
			_teamResultIcon[0].sprite = _teamResultIcopSp[1];
			_teamResultIcon[1].sprite = _teamResultIcopSp[0];
		}
		if (Cpc.ONLINEMODE == true) {
			_resutlText[2].text = Cpc.killNumC[PhotonNetwork.player.ID-1]+"";
		} else {
			_resutlText[2].text = Cpc.killNumC[0]+"";
		}

		_resultG.SetActive(true);
	}

	//タイトルへ
	public void onTitleButton()
	{
		if (_clicked)
		{
			return;
		}
		_clicked = true;
		Cpc.playSe(Cpc.SeType.click_yes);

		SceneManager.LoadScene(0);
		if (Cpc.ONLINEMODE == true)
		{
			PhotonNetwork.Disconnect();
		}
	}

	//くまアイコン表示
	public void showBearIcon(float distance, Cpc.Team team)
	{
		if (team == Cpc.Team.Red)
		{
			_bearIcon[(int)Cpc.Team.Red].GetComponent<RectTransform>().localPosition = new Vector2(-1 * Cpc.KUMAUIMAX * ((_kDistS_r-distance)/_kDistS_r), _bearIcon[(int)Cpc.Team.Red].GetComponent<RectTransform>().localPosition.y);
		}
		else
		{
			_bearIcon[(int)Cpc.Team.Blue].GetComponent<RectTransform>().localPosition = new Vector2(Cpc.KUMAUIMAX * ((_kDistS_b-distance)/_kDistS_b), _bearIcon[(int)Cpc.Team.Blue].GetComponent<RectTransform>().localPosition.y);
		}
	}

	//カウンター表示
	public void showCounter(Cpc.Team team)
	{
		if (team == Cpc.Team.Red)
		{
			sitCount_r--;
			_countRed.text = sitCount_r+"";
		}
		else
		{
			sitCount_b--;
			_countBlue.text = sitCount_b+"";
		}

		if (sitCount_r == 0 || sitCount_b == 0)
		{
			Cpc.changeAppState(Cpc.AppState.battleEnd);
		}

	}

	//吹き出し表示
	public void showFuki(Cpc.Team team)
	{
		if (team == Cpc.Team.Red)
		{
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 0, 0);
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(-35f, 152f);
			_fukiRed.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(255,0,0,1f);
			_fukiRed.GetComponent<Image>().color = Util.rgbColor(255,255,255,1f);
			sitCount_r = Cpc.KUMACOUNT;
			_fukiRed.gameObject.SetActive(true);
			Cpc.changeAppState(Cpc.AppState.countdown);

			if (Cpc.myTeam == Cpc.Team.Red)
			{
				showCutIn(Cpc.myTeam, true);
			}
			else
			{
				showCutIn(Cpc.myTeam, false);
			}

		}
		else
		{
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 180, 0);
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(29f, 152f);
			_fukiBlue.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(0,107,255,1f);
			_fukiBlue.GetComponent<Image>().color = Util.rgbColor(255,255,255,1f);
			sitCount_b = Cpc.KUMACOUNT;
			_fukiBlue.gameObject.SetActive(true);
			Cpc.changeAppState(Cpc.AppState.countdown);

			if (Cpc.myTeam == Cpc.Team.Red)
			{
				showCutIn(Cpc.myTeam, false);
			}
			else
			{
				showCutIn(Cpc.myTeam, true);
			}
		}
	}

	public void hideFuki(Cpc.Team team)
	{
		Cpc._kumaObj[(int)team].GetComponent<kuma>().delEffect(team);
		StartCoroutine(doHideFuki(team));
	}

	private IEnumerator doHideFuki(Cpc.Team team)
	{
		yield return new WaitForSeconds(0.5f);

		if (team == Cpc.Team.Red)
		{
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 0, -10);

			yield return new WaitForSeconds(0.1f);
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 0, 10);
			yield return new WaitForSeconds(0.1f);
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 0, -10);
			yield return new WaitForSeconds(0.1f);
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 0, 10);
			yield return new WaitForSeconds(0.1f);
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 0, -20);
			yield return new WaitForSeconds(0.1f);

			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.x, _fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.y-20);
			_fukiRed.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(255,0,0,0.7f);
			_fukiRed.GetComponent<Image>().color = Util.rgbColor(255,255,255,0.7f);
			yield return new WaitForSeconds(0.1f);
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.x, _fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.y-20);
			_fukiRed.GetComponent<Image>().color = Util.rgbColor(255,255,255,0.4f);
			_fukiRed.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(255,0,0,0.4f);
			yield return new WaitForSeconds(0.1f);
			_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.x, _fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.y-20);
			_fukiRed.GetComponent<Image>().color = Util.rgbColor(255,255,255,0.1f);
			_fukiRed.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(255,0,0,0.1f);
			yield return new WaitForSeconds(0.1f);
		}
		else
		{
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 180, -10);
			yield return new WaitForSeconds(0.1f);
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 180, 10);
			yield return new WaitForSeconds(0.1f);
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 180, -10);
			yield return new WaitForSeconds(0.1f);
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 180, 10);
			yield return new WaitForSeconds(0.1f);
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localRotation = Quaternion.Euler(0, 180, -20);
			yield return new WaitForSeconds(0.1f);

			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.x, _fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.y-10);
			_fukiBlue.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(0,107,255,0.7f);
			_fukiBlue.GetComponent<Image>().color = Util.rgbColor(255,255,255,0.7f);
			yield return new WaitForSeconds(0.1f);
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.x, _fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.y-10);
			_fukiBlue.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(0,107,255,0.4f);
			_fukiBlue.GetComponent<Image>().color = Util.rgbColor(255,255,255,0.4f);
			yield return new WaitForSeconds(0.1f);
			_fukiBlue.gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector2(_fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.x, _fukiRed.gameObject.GetComponent<RectTransform>().transform.localPosition.y-10);
			_fukiBlue.gameObject.transform.FindChild("time").GetComponent<Text>().color = Util.rgbColor(0,107,255,0.1f);
			_fukiBlue.GetComponent<Image>().color = Util.rgbColor(255,255,255,0.1f);
			yield return new WaitForSeconds(0.1f);
		}

		yield return new WaitForSeconds(0.2f);

		if (team == Cpc.Team.Red)
		{
			_fukiRed.gameObject.SetActive(false);
			Cpc.changeAppState(Cpc.AppState.battle);
			sitCount_r = Cpc.KUMACOUNT;
			_countRed.text = sitCount_r+"";
		}
		else
		{
			_fukiBlue.gameObject.SetActive(false);
			Cpc.changeAppState(Cpc.AppState.battle);
			sitCount_b = Cpc.KUMACOUNT;
			_countBlue.text = sitCount_b+"";
		}

		yield return null;
	}

	//メッセージ管理
	public void showMessage(String txt, bool isBlink=false, bool isFeed=false)
	{
		_msgBkColor.color = new Color(_msgBkColor.color.r,_msgBkColor.color.g,_msgBkColor.color.b, 1f);
		_msgTxt.color = new Color(_msgTxt.color.r,_msgTxt.color.g,_msgTxt.color.b, 1f);
		_msgTxt.text = txt;
		_msgG.SetActive(true);

		isBlinking = false;
		if (isBlink == true)
		{
			StartCoroutine(blinkMessage());
		} else if (isFeed == true)
		{
			StartCoroutine(feedMessage());
		}
		else
		{
			StartCoroutine(normalMessage());
		}
	}

	private IEnumerator blinkMessage()
	{
		int iCnt = 0;
		float iA = 0f;
		bool fg = false;

		isBlinking = true;
		while(isBlinking == true)
		{
			if (fg == true)
			{
				iA -= 0.1f;
			}
			else
			{
				iA += 0.1f;
			}

			if (iA > 1.0f)
			{
				iCnt++;
				iA = 1.0f;
				fg = true;
			}
			else if (iA < 0.3f)
			{
				iA = 0.3f;
				fg = false;
			}
			_msgBkColor.color = new Color(_msgBkColor.color.r,_msgBkColor.color.g,_msgBkColor.color.b, iA);

			yield return new WaitForSeconds(0.1f);
		}

		yield return null;
	}

	private IEnumerator feedMessage()
	{
		float iA = 1f;
		while(iA >  0.1f)
		{
			if (iA < 0.4f)
			{
				_msgBkColor.color = new Color(_msgBkColor.color.r,_msgBkColor.color.g,_msgBkColor.color.b, 0f);
			}
			else
			{
				_msgBkColor.color = new Color(_msgBkColor.color.r,_msgBkColor.color.g,_msgBkColor.color.b, iA);
			}
			_msgTxt.color = new Color(_msgTxt.color.r,_msgTxt.color.g,_msgTxt.color.b, iA);
			iA -= 0.05f;
			yield return new WaitForSeconds(0.1f);
		}
		_msgBkColor.color = new Color(_msgBkColor.color.r,_msgBkColor.color.g,_msgBkColor.color.b, 1f);

		hideMessage();
		yield return null;
	}

	private IEnumerator normalMessage()
	{
		_msgBkColor.color = new Color(_msgBkColor.color.r,_msgBkColor.color.g,_msgBkColor.color.b, 1f);
		yield return new WaitForSeconds(3f);
	}

	public void hideMessage()
	{
		_msgTxt.text = "";
		_msgG.SetActive(false);
	}

	private void hideButtons()
	{
		_btnInGames[0].SetActive(false);
		_btnInGames[1].SetActive(false);
		_btnInGames[2].SetActive(false);
	}

	public void onClickOffline()
	{
		if (_clicked)
		{
			return;
		}
		Cpc.playSe(Cpc.SeType.click_yes);

		_clicked = true;
		hideButtons();

		Cpc.ONLINEMODE = false;
		Cpc.makeKuma ();

		if (Cpc.isSkipOpening == true)
		{
			_outGameUI.SetActive(false);
			mainCamera.clearFlags = CameraClearFlags.Depth;
			startOffLine();
		}
		else
		{
			Cpc.changeAppState(Cpc.AppState.matching);
			StartCoroutine(startOpening ());
		}
		_clicked = false;

	}

	public void onClick3vs3()
	{
		if (_clicked)
		{
			return;
		}
		_clicked = true;
		Cpc.playSe(Cpc.SeType.click_yes);

		Cpc.nowRoomMode = Cpc.RoomMode.three;
		Cpc.MaxChara = 3;
//		Cpc.MaxChara = 1;
		onClickInRoom();
	}

	public void onClick10vs10()
	{
		if (_clicked)
		{
			return;
		}
		_clicked = true;
		Cpc.playSe(Cpc.SeType.click_yes);

		Cpc.nowRoomMode = Cpc.RoomMode.ten;
		Cpc.MaxChara = 10;
		onClickInRoom();
	}

	private void onClickInRoom()
	{
		hideButtons();

		Cpc.ONLINEMODE = true;

		showMessage("Now Connecting...", true);
		Cpc.netC.JoinLobby ((error) => {
			_clicked = false;
			if (error) {
				showMessage ("Join lobby failed");
			} else {
				showMessage ("Join Network Connected", false, true);
			}
		});

	}

	public IEnumerator startOpening()
	{
		_outGameUI.SetActive(false);
		mainCamera.clearFlags = CameraClearFlags.Depth;

		_opening.SetActive(true);

		yield return null;
	}

	public void hideOpening()
	{
		_opening.SetActive(false);
	}

	public void showErrorMsg(string msg)
	{
		_outGameUI.SetActive(true);
		showMessage (msg, false, false);

		StartCoroutine(toRestart());
	}

	private IEnumerator toRestart()
	{
		Cpc.clearStatic();
		if (Cpc.ONLINEMODE == true)
		{
			PhotonNetwork.Disconnect();
		}

		yield return new WaitForSeconds(3.0f);
		SceneManager.LoadScene(0);
	}

	public void startOffLine()
	{
		Cpc.initStatus ();
		Cpc.changeAppState(Cpc.AppState.battleStart);
	}

	public void changeNaviIcon()
	{
		_naviIcon.gameObject.SetActive (true);
		switch (Cpc.myState) {
		case Cpc.PLayerStatus.normal:
			if (Cpc.myTeam == Cpc.Team.Red) {
				_naviIcon.sprite = _naviIconSp[0];
			} else {
				_naviIcon.sprite = _naviIconSp[2];
			}
			break;
		case Cpc.PLayerStatus.kumaCarrying:
		case Cpc.PLayerStatus.supporting:
			if (Cpc.myTeam == Cpc.Team.Red) {
				_naviIcon.sprite = _naviIconSp[1];
			} else {
				_naviIcon.sprite = _naviIconSp[3];
			}
			break;
		case Cpc.PLayerStatus.foodCarrying:
			_naviIcon.sprite = _naviIconSp[4];
			break;
		case Cpc.PLayerStatus.dead:
			_naviIcon.gameObject.SetActive (false);
			break;
		}
	}

	public void showFade()
	{
		_reviveTime = 0f;
		_fadeF = 0f;
		_fadeTxt.SetActive(false);
		_fade.SetActive (true);
		_fade.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
		isFading = true;
	}

	public void onTapMapDown()
	{
		if (Cpc.myTeam == Cpc.Team.Blue) {
			GameObject obj = GameObject.Find ("mapCamera");
			obj.transform.rotation = Quaternion.Euler (90, 0, 180);
		}

		_mapCamera.enabled = true;
		Cpc.disableCamera ();
	}

	public void onTapMapUp()
	{
		if (Cpc.myTeam == Cpc.Team.Blue) {
			GameObject obj = GameObject.Find ("mapCamera");
			obj.transform.rotation = Quaternion.Euler (90, 0, 180);
		}

		_mapCamera.enabled = false;
		Cpc.enableCamera ();
	}

	//オンラインアイコン表示
	public void showOnlineIcon()
	{
		_onLineG[0].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-720f, 274f, 0);
		_onLineG[1].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(2730f, 274f, 0);

		_onLineG[0].gameObject.SetActive(true);
		_onLineG[1].gameObject.SetActive(true);

		StartCoroutine(initOnlineIcon());
	}

	private IEnumerator initOnlineIcon()
	{
		for (int i=0; i<10; i++)
		{
			if(i < Cpc.MaxChara)
			{
				_onLineIConR[i].gameObject.SetActive(true);
//				_onLineIConR[i].transform.FindChild("img_icon").GetComponent<Image>().sprite = _onLineIcon[2];
//				_onLineIConR[i].transform.FindChild("Name").GetComponent<Text>().text = "COM";
			}
			else
			{
				_onLineIConR[i].gameObject.SetActive(false);
			}
		}
		for (int i=0; i<10; i++)
		{
			if(i < Cpc.MaxChara)
			{
				_onLineIConB[i].gameObject.SetActive(true);
//				_onLineIConB[i].transform.FindChild("img_icon").GetComponent<Image>().sprite = _onLineIcon[2];
//				_onLineIConB[i].transform.FindChild("Name").GetComponent<Text>().text = "COM";
			}
			else
			{
				_onLineIConB[i].gameObject.SetActive(false);
			}
		}
		float f = -720f;
		while(f < 140f)
		{
			_onLineG[0].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(_onLineG[0].gameObject.GetComponent<RectTransform>().localPosition.x+10f, 274f, 0);
			_onLineG[1].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(_onLineG[1].gameObject.GetComponent<RectTransform>().localPosition.x-10f, 274f, 0);

			f += 10f;
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (2f);
	}


	//アイコン表示変更
	public void changeOnlineIcon(int playerNo, string name)
	{
		Debug.Log("playerNo : " + playerNo + " name : " + name);


		int it = 0;
		if (playerNo % 2 == 0)
		{
			it = (int)(playerNo/2);
			_onLineIConR[it].transform.FindChild("img_icon").GetComponent<Image>().sprite = _onLineIcon[0];
			_onLineIConR[it].transform.FindChild("Name").GetComponent<Text>().text = name;
		}
		else
		{
			Debug.Log("playerNo : " + playerNo);

			it = (int)((playerNo-1)/2f);
			_onLineIConB[it].transform.FindChild("img_icon").GetComponent<Image>().sprite = _onLineIcon[1];
			_onLineIConB[it].transform.FindChild("Name").GetComponent<Text>().text = name;
		}

	}

	public void setKumaCount(int count, Cpc.Team team)
	{
		if (team == Cpc.Team.Red)
		{
			sitCount_r = count;
		}

		if (team == Cpc.Team.Blue)
		{
			sitCount_b = count;
		}
	}

	//キル数表示
	public void showKillNum(Cpc.Team team)
	{
		StartCoroutine(doShowKillNum(team));
	}

	private IEnumerator doShowKillNum(Cpc.Team team)
	{
		if (team == Cpc.Team.Red)
		{
			for (int i=0; i<12; i++)
			{
				if (i % 2 == 0)
				{
					_killText[(int)Cpc.Team.Red].GetComponent<Text>().fontSize = 80;
					_killText[(int)Cpc.Team.Red].GetComponent<Text>().color = Util.rgbColor(255,0,0,0.7f);
				}
				else
				{
					_killText[(int)Cpc.Team.Red].GetComponent<Text>().fontSize = 60;
					_killText[(int)Cpc.Team.Red].GetComponent<Text>().color = Util.rgbColor(255,0,0,1f);
				}
				if (i == 6)
				{
					_killText[(int)Cpc.Team.Red].text = Cpc.killNum[(int)Cpc.Team.Red]+"";
				}
				yield return new WaitForSeconds(0.05f);

			}
			yield return new WaitForSeconds(0.2f);
			_killText[(int)Cpc.Team.Red].GetComponent<Text>().fontSize = 70;
			_killText[(int)Cpc.Team.Red].GetComponent<Text>().color = Util.rgbColor(255,255,255,1f);
		}
		else
		{
			for (int i=0; i<12; i++)
			{
				if (i % 2 == 0)
				{
					_killText[(int)Cpc.Team.Blue].GetComponent<Text>().fontSize = 80;
					_killText[(int)Cpc.Team.Blue].GetComponent<Text>().color = Util.rgbColor(0,107,255,0.7f);
				}
				else
				{
					_killText[(int)Cpc.Team.Blue].GetComponent<Text>().fontSize = 60;
					_killText[(int)Cpc.Team.Blue].GetComponent<Text>().color = Util.rgbColor(0,107,255,1f);
				}
				if (i == 6)
				{
					_killText[(int)Cpc.Team.Blue].text = Cpc.killNum[(int)Cpc.Team.Blue]+"";
				}
				yield return new WaitForSeconds(0.05f);
			}
			yield return new WaitForSeconds(0.2f);
			_killText[(int)Cpc.Team.Blue].GetComponent<Text>().fontSize = 70;
			_killText[(int)Cpc.Team.Blue].GetComponent<Text>().color = Util.rgbColor(255,255,255,1f);
		}

	}


	//カットイン関連
	public void showCutIn(Cpc.Team team, bool isGood)
	{
		if (team == Cpc.Team.Red)
		{
			_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(-1920f,0);
			_cutInImg[0].SetActive(false);
			_cutInImg[1].SetActive(false);
			if (isGood == true)
			{
				_cutInImg[0].SetActive(true);
			}
			else
			{
				_cutInImg[1].SetActive(true);
			}
		}
		else
		{
			_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(1920f,0);
			_cutInImg[2].SetActive(false);
			_cutInImg[3].SetActive(false);
			if (isGood == true)
			{
				_cutInImg[2].SetActive(true);
			}
			else
			{
				_cutInImg[3].SetActive(true);
			}
		}

		StartCoroutine(doCutIn(team));
	}

	private IEnumerator doCutIn(Cpc.Team team)
	{
		if (team == Cpc.Team.Red)
		{
			int f = -1920;

			while(f != 0)
			{
				_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(f,0);
				f += 192;
				yield return new WaitForSeconds(0.01f);
			}
			_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(0,0);
			yield return new WaitForSeconds(0.8f);

			while(f < 1900)
			{
				_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(f,0);
				f += 192;
				yield return new WaitForSeconds(0.01f);
			}
			_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(1920,0);

		}
		else
		{
			int f = 1920;

			while(f != 0)
			{
				_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(f,0);
				f -= 192;
				yield return new WaitForSeconds(0.01f);
			}
			_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(0,0);
			yield return new WaitForSeconds(0.8f);

			while(f > -1900)
			{
				_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(f,0);
				f -= 192;
				yield return new WaitForSeconds(0.01f);
			}
			_cutInG[(int)team].GetComponent<RectTransform>().transform.localPosition = new Vector2(-1920,0);

		}
	}

	public void showGuide(Cpc.GuideType type)
	{

		if (Cpc.isGuideShow[(int)type] == true) return;

		StartCoroutine(hideGuide(type));
	}

	private IEnumerator hideGuide(Cpc.GuideType type)
	{
		Cpc.isGuideShow[(int)type] = true;

		if (type == Cpc.GuideType.start)
		{
			yield return new WaitForSeconds(3f);
		}

		_guide.SetActive(true);

		switch (type)
		{
		case Cpc.GuideType.start:
			_guide.transform.FindChild("Text").GetComponent<Text>().text = "画面上部が敵だからな\n早くクマを奪い返しに行けよ\nクマの近くでタップすれば\n持ち上げられるぜ";
			break;
		case Cpc.GuideType.pickup:
			_guide.transform.FindChild("Text").GetComponent<Text>().text = "お、クマを持ち上げたな。\nさっさと玉座に座らせろ。\nお前らの玉座は画面下部だ。";
			break;
		case Cpc.GuideType.sitting:
			_guide.transform.FindChild("Text").GetComponent<Text>().text = "なかなかやるじゃん。\n後はまた奪われないように\n30秒守り抜けよ。";
			break;
		case Cpc.GuideType.humburger:
			_guide.transform.FindChild("Text").GetComponent<Text>().text = "ハンバーガーは\n敵のクマに食べさせると\n太って運びにくくなる！\n近づくだけで食うぞ";
			break;
		case Cpc.GuideType.epickup:
			_guide.transform.FindChild("Text").GetComponent<Text>().text = "こいつは敵のクマだぜ！\n牢屋に閉じ込めたら、\nハンバーガー食わせて\n太らせてしまえ！";
			break;
		}

		yield return new WaitForSeconds(4f);
		_guide.SetActive(false);
	}
}