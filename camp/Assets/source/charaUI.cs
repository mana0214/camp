﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class charaUI : MonoBehaviour
{
	[SerializeField]
	private chara _chara;

	[SerializeField]
	private Transform _avator;
	[SerializeField]
	private SkinnedMeshRenderer _acatorSkin;

	//Avator Source
	[SerializeField]
	private Mesh[] _avatorMesh;
	[SerializeField]
	private Material[] _avatorMaterial;
	[SerializeField]
	private GameObject[] _avatorWeapon;

	//UI
	[SerializeField]
	private GameObject ui;
	[SerializeField]
	private GameObject ui_rv;

	//プレイヤーフラグ
	public bool isPlayer
	{
		set;
		get;
	}
	public bool isDead
	{
		set;
		get;
	}

	//Status
	public Cpc.ActionType nowActionType
	{
		set;
		get;
	}
	private Cpc.ActionType _bkActionType;

	private GameObject _name;
	[SerializeField]
	private GameObject _navi;
	private float _timeCheck = 0.5f;
	private float _timedelta = 0f;
	private string _prefix = "_r";

	public void init()
	{
		if (_chara.isPlayer == false &&  _chara.isNetPlayer == false)
		{
			Vector3 v = this.gameObject.transform.FindChild("UI").transform.localPosition;

//			this.gameObject.transform.FindChild("UI").transform.localPosition = new Vector3(v.z, -0.36f, v.z);
//			this.gameObject.transform.FindChild("UI_rv").transform.localPosition = new Vector3(v.z, -0.36f, v.z);
			_navi.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () {

		//リング更新
		if (isPlayer == true && _navi != null)
		{
			//idleに戻す
			if (_timedelta > _timeCheck)
			{
				_timedelta = 0;
				searchObj();
			}
			else
			{
				_timedelta += Time.deltaTime;
			}
		}
		
	}

	public void damaged(int damage, bool isRecover=false)
	{
		StartCoroutine(changeHP(damage, isRecover));
	}

	private IEnumerator changeHP(int atai, bool isRecover)
	{
		int nowHp = _chara._charaInfo.nowHp;
		int dmg_t = atai;
		float f = 0f;

		if (isRecover == true) {
			f = 1.46f * (_chara._charaInfo.nowHp - 1 * atai) / (float)_chara._charaInfo.nowMaxHp;
			if (f >= 1.46f) {
				f = 1.46f;
			}

			ui.transform.FindChild ("dg").gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0.2f);
			ui.transform.FindChild ("rv").gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (f, 0.2f);
			ui_rv.transform.FindChild ("dg").gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0.2f);
			ui_rv.transform.FindChild ("rv").gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (f, 0.2f);
			yield return new WaitForSeconds (0.6f);
		} else {
			ui.transform.FindChild ("rv").gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0.2f);
			ui_rv.transform.FindChild ("rv").gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0.2f);
		}


		while(dmg_t != 0 && nowHp != 0)
		{
			f = 1.46f * nowHp / (float)_chara._charaInfo.nowMaxHp;

			if (_chara.myTeam == Cpc.Team.Red)
			{
				ui.transform.FindChild("hp_r").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
				ui_rv.transform.FindChild("hp_r").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
			}
			else
			{
				ui.transform.FindChild("hp_b").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
				ui_rv.transform.FindChild("hp_b").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
			}

			if (isRecover == false) {
				nowHp -= 10;
				dmg_t -= 10;
			} else {
				nowHp += 10;
				dmg_t += 10;
				if (dmg_t > 0) {dmg_t = 0;}
			}

			if (nowHp <= 0)
			{
				nowHp = 0;
				break;
			}
			if (nowHp >= _chara._charaInfo.nowMaxHp)
			{
				nowHp = _chara._charaInfo.nowMaxHp;
				break;
			}
			yield return new WaitForSeconds(0.01f);
		}

		_chara._charaInfo.damaged(atai);

		f = 1.46f * _chara._charaInfo.nowHp / (float)_chara._charaInfo.nowMaxHp;
		if (_chara.myTeam == Cpc.Team.Red)
		{
			ui.transform.FindChild("hp_r").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
			ui_rv.transform.FindChild("hp_r").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
		}
		else
		{
			ui.transform.FindChild("hp_b").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
			ui_rv.transform.FindChild("hp_b").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
		}

		if (isRecover == false) {
			yield return new WaitForSeconds(0.6f);
			ui.transform.FindChild("dg").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
			ui_rv.transform.FindChild("dg").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(f, 0.2f);
		}

		yield return null;
	}

	public void jobchg(bool isP, Cpc.JobType toJob, Cpc.CharaInfo cInfo, bool isChange=false)
	{
//		if (_chara.nowActionType == Cpc.ActionType.ChageJob) return;
//		if (_chara.myJob == toJob) return;

		_chara.isShrinkage = true;

		float sp = 0f;
		if (_chara._agent !=null && _chara._agent.enabled == true)
		{
			sp = _chara._agent.speed;
			_chara._agent.speed = 0;
		}

		isPlayer = isP;

		if (isChange == true && _chara.myJob == toJob)
		{
			return;
		}

		_chara.myJob = toJob;
		StartCoroutine(doJobChg(isChange, cInfo.name, sp));
	}

	private IEnumerator doJobChg(bool ischange, string name, float speed)
	{
		if (ischange == true)
		{
			_chara.changeState(Cpc.ActionType.ChageJob);
			switch(_chara.myJob)
			{
				case Cpc.JobType.Knight:
					Util.playEff("job_kn_chg", Cpc.Team.NoTeam, this.transform, this.transform.position,0f,0f,0f);
					break;
				case Cpc.JobType.Sorcerer:
					Util.playEff("job_bk_chg", Cpc.Team.NoTeam, this.transform, this.transform.position,0f,0f,0f);
					break;
				case Cpc.JobType.Priest:
					Util.playEff("job_wh_chg", Cpc.Team.NoTeam, this.transform, this.transform.position,0f,0f,0f);
					break;
			}
			yield return new WaitForSeconds(0.2f);

			int i = 0;
			while(i < 40)
			{
				_avator.localRotation = Quaternion.Euler(0, 60*i, 0);
				yield return new WaitForSeconds(0.01f);
				i++;
			}
			_avator.localRotation = Quaternion.Euler(0, 0f, 0);

			//ジャンプ後の補正
			if (isPlayer == true)
			{
				if (_avator.transform.localPosition.y > -1.4f)
				{
					_avator.transform.localPosition = new Vector3(0, -1.4f, 0);
				}
			}

			if (_chara._agent !=null && _chara._agent.enabled == true)
			{
				_chara._agent.speed = speed;
			}

			_chara.changeState(Cpc.ActionType.Idle);
		}

		initUI(name);

		_chara._charaInfo.jobChange (_chara.myJob);

		Material[] materials;
		materials = _acatorSkin.materials;
		if (_chara.myJob == Cpc.JobType.Knight){materials[3] = _avatorMaterial[(int)_chara.myJob*2 + (int)_chara.myTeam];}
		else{materials[0] = _avatorMaterial[(int)_chara.myJob*2 + (int)_chara.myTeam];}

		_acatorSkin.materials = materials;
		_acatorSkin.sharedMesh = _avatorMesh[(int)_chara.myJob];

		_avatorWeapon[0].SetActive(false);
		_avatorWeapon[1].SetActive(false);
		_avatorWeapon[2].SetActive(false);
		_avatorWeapon[3].SetActive(false);
		_avatorWeapon[(int)_chara.myJob].SetActive(true);

		//jobチェンジ後のNet送信
		if(Cpc.ONLINEMODE == true)
		{
			Cpc.netC.SendComInfo (_chara._charaInfo.toString());
		}

		yield return null;
	}
		

	private void initUI(string name)
	{
		//init
		if (_chara.myTeam == Cpc.Team.Red) {
			ui.transform.FindChild("name_r").gameObject.SetActive(false);
			ui.transform.FindChild("hp_r").gameObject.SetActive(false);
			ui_rv.transform.FindChild("name_r").gameObject.SetActive(false);
			ui_rv.transform.FindChild("hp_r").gameObject.SetActive(false);
//			transform.FindChild("aura_r").gameObject.SetActive(false);
		} else {
			ui.transform.FindChild("name_b").gameObject.SetActive(false);
			ui.transform.FindChild("hp_b").gameObject.SetActive(false);
			ui_rv.transform.FindChild("name_b").gameObject.SetActive(false);
			ui_rv.transform.FindChild("hp_b").gameObject.SetActive(false);
//			transform.FindChild("aura_b").gameObject.SetActive(false);
		}
		ui.transform.FindChild("waku").gameObject.SetActive(false);
		ui_rv.transform.FindChild("waku").gameObject.SetActive(false);

		if (_chara.myJob == Cpc.JobType.Bare)
		{
			ui.transform.FindChild("balloon").gameObject.SetActive(true);
			ui_rv.transform.FindChild("balloon").gameObject.SetActive(true);
		}
		else
		{
			ui.transform.FindChild("waku").gameObject.SetActive(true);
			ui.transform.FindChild("dg").gameObject.SetActive(true);
			ui.transform.FindChild("dg").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
			ui.transform.FindChild("rv").gameObject.SetActive(true);
			ui.transform.FindChild("rv").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
			ui_rv.transform.FindChild("waku").gameObject.SetActive(true);
			ui_rv.transform.FindChild("dg").gameObject.SetActive(true);
			ui_rv.transform.FindChild("dg").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
			ui_rv.transform.FindChild("rv").gameObject.SetActive(true);
			ui_rv.transform.FindChild("rv").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
			if (_chara.myTeam == Cpc.Team.Red)
			{
//				transform.FindChild("aura_r").gameObject.SetActive(true);
				ui.transform.FindChild("name_r").gameObject.SetActive(true);

				ui.transform.FindChild("hp_r").gameObject.SetActive(true);
				ui.transform.FindChild("hp_r").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
				ui.transform.FindChild("name_r").gameObject.GetComponent<Text>().text = name;

				ui_rv.transform.FindChild("name_r").gameObject.SetActive(true);
				ui_rv.transform.FindChild("hp_r").gameObject.SetActive(true);
				ui_rv.transform.FindChild("hp_r").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
				ui_rv.transform.FindChild("name_r").gameObject.GetComponent<Text>().text = name;
				if (isPlayer == true)
				{
					_navi.SetActive(true);
				}
			}
			else
			{
//				transform.FindChild("aura_b").gameObject.SetActive(true);
				ui.transform.FindChild("name_b").gameObject.SetActive(true);
				ui.transform.FindChild("hp_b").gameObject.SetActive(true);
				ui.transform.FindChild("hp_b").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
				ui.transform.FindChild("name_b").gameObject.GetComponent<Text>().text = name;

				ui_rv.transform.FindChild("name_b").gameObject.SetActive(true);
				ui_rv.transform.FindChild("hp_b").gameObject.SetActive(true);
				ui_rv.transform.FindChild("hp_b").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1.46f, 0.2f);
				ui_rv.transform.FindChild("name_b").gameObject.GetComponent<Text>().text = name;
				if (isPlayer == true)
				{
					_navi.SetActive(true);
				}
			}
		}
		if (_chara.myTeam == Cpc.Team.Blue)
		{
			_prefix = "_b";
		}
	}



	private void searchObj()
	{
		GameObject obj = null;
		switch(Cpc.myState)
		{
		case Cpc.PLayerStatus.normal:
			obj = GameObject.Find("Kuma"+_prefix+"(Clone)");
			break;
		case Cpc.PLayerStatus.kumaCarrying:
		case Cpc.PLayerStatus.supporting:
			obj = GameObject.Find("Bench"+_prefix);
			break;
		case Cpc.PLayerStatus.foodCarrying:
			if (_chara.myTeam == Cpc.Team.Red)
			{
				obj = GameObject.Find("Kuma_b(Clone)");
			}
			else
			{
				obj = GameObject.Find("Kuma_r(Clone)");
			}
			break;
		}


		if (obj != null)
		{

			float r = Util.GetAim(new Vector2(obj.transform.localPosition.x, obj.transform.localPosition.z), new Vector2(this.transform.localPosition.x, this.transform.localPosition.z));
			_navi.transform.rotation = Quaternion.Euler(0, 360-r, 0);
		}
	}

}
