﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class kuma : MonoBehaviour
{
	[SerializeField]
	public Cpc.Team _myTeam;
//	[SerializeField]
	private Transform _bench;
	[SerializeField]
	private Animation _nowAnime;
	[SerializeField]
	private AnimationClip[] _animeClips;
	[SerializeField]
	private Transform _avator;
	//UI
	[SerializeField]
	private GameObject ballon;
	[SerializeField]
	private GameObject ballonH;

	//Balloon

	[SerializeField]
	private GameObject _testChara;

	[SerializeField]
	private Transform _ui;
	[SerializeField]
	private Transform _ui_rv;
//	[SerializeField]
	private Transform _uiCamera;

	private bool isBalloonYoko = false;
	private bool isBalloonTate = false;
	private float _weightCheck = 0f;
	private GameObject _shadow;
	private float _checkTime = 0f;

	public Cpc.KumaState nowState
	{
		private set;
		get;
	}

	private int _nowWeight;
	public int nowWeight
	{
		private set
		{
			_nowWeight = value;
			sendWeight();
		}
		get
		{
			return _nowWeight;
		}
	}

	public int _nowSupport
	{
		set;
		get;
	}

	private GameObject _liftChara;

	void OnEnable()
	{
//		init();
		this.gameObject.transform.parent = GameObject.Find ("map").transform;
		if (_myTeam == Cpc.Team.Red)
		{
			this.transform.localPosition = Cpc.KUMAInitPos_r;
		}
		else
		{
			this.transform.localPosition = Cpc.KUMAInitPos_b;
		}
		_shadow = this.transform.FindChild ("UI").FindChild ("shadow").gameObject;
	}

	public void init()
	{
		_nowSupport = 0;
		nowWeight = Cpc.KUMAWEIGHT;

		changeState(Cpc.KumaState.Putting);

		if (_myTeam == Cpc.Team.Red) {
			_bench = GameObject.Find ("Bench_r").gameObject.transform;
		} else {
			_bench = GameObject.Find ("Bench_b").gameObject.transform;
		}
		_uiCamera = GameObject.Find ("UICamera").gameObject.transform;

		changeBalloon("早く来い！");

		_nowAnime.clip = _animeClips[9];
		_nowAnime.Play ();

		isBalloonYoko = true;
		StartCoroutine (BalloonAnime());
	}

	void Update()
	{
		//UI部を常に正面に
		//			_cui.gameObject.GetComponent<RectTransform>().rotation = Quaternion.LookRotation(_uiCamera.forward, Vector3.up) * Quaternion.FromToRotation(Vector3.up, Vector3.forward);
		if (_ui != null && _uiCamera != null) {
			_ui.GetComponent<RectTransform>().rotation = Quaternion.LookRotation(_uiCamera.forward, Vector3.up) * Quaternion.FromToRotation(Vector3.up, Vector3.forward);
			_ui_rv.GetComponent<RectTransform>().rotation = Quaternion.LookRotation(_uiCamera.forward, Vector3.up) * Quaternion.FromToRotation(Vector3.up, Vector3.forward);
		}

		//時間で痩せる
		if (nowWeight > Cpc.KUMAWEIGHT && nowState == Cpc.KumaState.Putting)
		{
			if (_weightCheck > Cpc.LOSEWTTIME)
			{
				_weightCheck = 0f;
				nowWeight--;

				_avator.transform.localScale = new Vector3(1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.2f, 1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.2f, 1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.2f);
			}
			else
			{
				_weightCheck += Time.deltaTime;
			}
		}

		if (nowState == Cpc.KumaState.Putting)
		{
			if (this.transform.localPosition.y > -4.6f)
			{
				this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y-0.1f, this.transform.localPosition.z);
				Cpc.netC.SendKumaPos(this.transform.localPosition, _myTeam);
			}
		}

		if (_checkTime > 0.1f) {
			_checkTime = 0f;
			//影の表示
			if (_shadow != null && _shadow.activeSelf == false && this.transform.localPosition.y < -3f && nowState == Cpc.KumaState.Putting) {
				_shadow.SetActive (true);
			}
		} else {
			_checkTime += Time.deltaTime;
		}
	}


	public void showIcon()
	{
		this.gameObject.transform.FindChild("ICON").gameObject.SetActive(true);
	}

	public void doLift(GameObject chara)
	{
		_shadow.SetActive (false);

		changeState(Cpc.KumaState.Lifting);
		_nowSupport = 0;
		_liftChara = chara;

		this.GetComponent<BoxCollider>().enabled = false;
//		this.GetComponent<Rigidbody> ().isKinematic = true;

		//エフェクト消去
//		delEffect();

		StartCoroutine(Carring(chara));
	}

	public void delEffect(Cpc.Team team)
	{
		//エフェクト消去
		if (team == Cpc.Team.Red) {
			GameObject obj = GameObject.Find ("eff_sitting_r(Clone)");
			if (obj != null) {
				Destroy (obj.gameObject);
			}
		}
		else if (team == Cpc.Team.Blue){
			GameObject obj = GameObject.Find ("eff_sitting_b(Clone)");
			if (obj != null) {
				Destroy (obj.gameObject);
			}
		}
	}

	public void released()
	{
		this.GetComponent<BoxCollider>().enabled = true;
		//ベンチが近い場合は座る
		//		if (_myTeam == Cpc.Team.Red && getBenchDistance() < Cpc.BENCHRANGE)
		if (getBenchDistance() < Cpc.BENCHRANGE)
		{
			StartCoroutine(Sitting());
		}
		else
		{
			StartCoroutine(Releasing());
		}
	}

	private IEnumerator Carring(GameObject chara)
	{
		this.GetComponent<BoxCollider> ().enabled = false;
		this.GetComponent<CharacterController>().enabled = false;

		float fx = this.transform.position.x - _liftChara.transform.position.x;
		float fz = this.transform.position.z - _liftChara.transform.position.z;

		if (fx > 0) {fx = fx * -1;}
		if (fz > 0) {fz = fz * -1;}

		float j = 1f;
		while (j < 2f)
		{
			this.transform.position = new Vector3(this.transform.position.x+(fx/8f), j, this.transform.position.z+(fz/8f));

			j += 0.4f;
			yield return new WaitForSeconds (0.02f);
		}
		while (j > 1.4f)
		{
			this.transform.position = new Vector3(this.transform.position.x+(fx/8f), j, this.transform.position.z+(fz/8f));

			j -= 0.4f;
			yield return new WaitForSeconds (0.02f);
		}

//		this.transform.parent = _liftChara.transform;
		this.transform.localPosition = new Vector3(_liftChara.transform.localPosition.x, _liftChara.transform.localPosition.y+1.4f, _liftChara.transform.localPosition.z);
		Cpc.netC.SendKumaPos(this.transform.localPosition, _myTeam);

		this.transform.localRotation = Quaternion.Euler (0, 0, 0);
		isBalloonTate = true;
		isBalloonYoko = false;
		_nowAnime.clip = _animeClips [4];
		_nowAnime.Play ();

		if (_myTeam == chara.GetComponent<chara> ().myTeam) {
			changeState(Cpc.KumaState.Lifting);
			changeBalloon ("戻るぞ！");
		} else {
			changeState(Cpc.KumaState.Infested);
			changeBalloon ("さらわれた！");
		}

		_liftChara.GetComponent<chara>().changeState(Cpc.ActionType.Carry);
		yield return null;
	}

	private IEnumerator Releasing()
	{
		float j = 1f;
		while (j < 2f)
		{
			this.transform.position = new Vector3(this.transform.position.x, j, this.transform.position.z);

			j += 0.4f;
			yield return new WaitForSeconds (0.02f);
		}
		while (j > 1.4f)
		{
			this.transform.position = new Vector3(this.transform.position.x, j, this.transform.position.z);

			j -= 0.4f;
			yield return new WaitForSeconds (0.02f);
		}

//		this.GetComponent<Rigidbody> ().isKinematic = false;
		this.GetComponent<BoxCollider> ().enabled = true;
		this.GetComponent<CharacterController>().enabled = true;

		this.transform.localRotation = Quaternion.Euler (0, 0, 0);
		isBalloonTate = false;
		isBalloonYoko = true;
		changeBalloon ("おいおい！");
		_nowAnime.clip = _animeClips [9];
		_nowAnime.Play ();

		if (_liftChara != null)
		{
			_liftChara.GetComponent<chara>().changeState(Cpc.ActionType.Idle);
		}

		yield return new WaitForSeconds (1f);
		changeState(Cpc.KumaState.Putting);

		yield return null;
	}

	private IEnumerator Sitting()
	{
		//体重リセット
		nowWeight = Cpc.KUMAWEIGHT;
		_avator.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

		float fx = this.transform.position.x - _bench.position.x;
		float fz = this.transform.position.z - _bench.position.z;

		if (fx < 0) {fx = fx * -1;}
		if (fz < 0) {fz = fz * -1;}

		float j = 1f;
		while (j < 2f)
		{
			this.transform.position = new Vector3(this.transform.position.x+(fx/8f), j, this.transform.position.z+(fz/8f));

			j += 0.4f;
			yield return new WaitForSeconds (0.02f);
		}
		while (j > 1.4f)
		{
			this.transform.position = new Vector3(this.transform.position.x+(fx/8f), j, this.transform.position.z+(fz/8f));

			j -= 0.4f;
			yield return new WaitForSeconds (0.02f);
		}

		if (_myTeam == Cpc.Team.Red) {
			this.transform.position = new Vector3(_bench.position.x, _bench.position.y, _bench.position.z+1.1f);
		} else {
			this.transform.position = new Vector3(_bench.position.x, _bench.position.y, _bench.position.z-1.1f);
		}

		isBalloonTate = false;
		isBalloonYoko = false;
		changeBalloon ("やったね！");
		_nowAnime.clip = _animeClips [0];
		_nowAnime.Play ();
		Util.playEff("sit",_myTeam,this.transform,this.transform.position,0f,-90f,0f);
		Util.playEff("sitting",_myTeam,this.transform,this.transform.position,0f,-90f,0f, false);

		changeState(Cpc.KumaState.Sitting);

		if(_liftChara.GetComponent<chara>().isPlayer == true && _liftChara.GetComponent<chara>().isNetPlayer == false)
		{
			Cpc.uiC.showGuide(Cpc.GuideType.sitting);
		}

		_liftChara.GetComponent<chara>().changeState(Cpc.ActionType.Idle);

		yield return null;
	}

	public void gainWeight()
	{
		Util.playEff("feed",Cpc.Team.NoTeam,this.transform,this.transform.position,0f,0f,0f,true, 1.5f);
		this.GetComponent<BoxCollider>().enabled = false;
		this.GetComponent<CharacterController>().enabled = false;

		StartCoroutine(gaining());
	}

	private IEnumerator gaining()
	{
		changeState(Cpc.KumaState.Gaining);

		if (nowWeight <= Cpc.MAXWEIGHT)
		{
			nowWeight++;
			sendWeight();
		}

		Vector3 _nowScale = _avator.transform.localScale;
		_nowAnime.Stop();

		int i = 0;
		while(i < 6)
		{
			_avator.transform.localScale = new Vector3(_nowScale.x - 0.1f, _nowScale.y - 0.1f, _nowScale.z - 0.1f);
			yield return new WaitForSeconds(0.1f);
			_avator.transform.localScale = new Vector3(_nowScale.x + 0.3f, _nowScale.y + 0.3f, _nowScale.z + 0.3f);
			yield return new WaitForSeconds(0.1f);
			i++;
		}

		yield return new WaitForSeconds(0.2f);
		_avator.transform.localScale = new Vector3(1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.5f, 1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.5f, 1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.5f);

		yield return new WaitForSeconds(0.2f);
		this.GetComponent<BoxCollider>().enabled = true;
		this.GetComponent<CharacterController>().enabled = true;

		_nowAnime.Play();

		_weightCheck = 0;
		changeState(Cpc.KumaState.Putting);
		yield return null;
	}

	private void sendWeight()
	{
		Cpc.netC.SendKumaWeight(nowWeight, _myTeam);
	}

	public void acceptWeight(int weight)
	{
		//直更新(net送信なし)
		_nowWeight = weight;
		_avator.transform.localScale = new Vector3(1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.5f, 1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.5f, 1.0f + (nowWeight-Cpc.KUMAWEIGHT)*0.5f);
	}


	public float getBenchDistance()
	{
		return Util.geoLength (this.transform.position.x, this.transform.position.z, _bench.position.x, _bench.position.z);
	}

	public Transform getMyBench()
	{
		return _bench;
	}


	private void changeBalloon(string txt)
	{
		ballon.transform.FindChild("Text").gameObject.GetComponent<Text>().text = txt;

	}

	public void showFeedBalloon()
	{
		ballonH.SetActive(true);
	}

	public void hideFeedBalloon()
	{
		ballonH.SetActive(false);
	}


	private IEnumerator Jumping()
	{
		int i = 0;
		int j = 0;
		while (i == 0)
		{
			_nowAnime.clip = _animeClips[j];
			_nowAnime.Play ();
			j++;
			if (j > 10)
			{
				j = 0;
			}

			yield return new WaitForSeconds (1.8f);
		}
	}

	private IEnumerator BalloonAnime()
	{
		float j = 0f;
		int k = 0;
		bool fg = false;

		while(isBalloonYoko == true || isBalloonTate == true)
		{
			if (fg == false)j += 0.05f;
			else j -= 0.05f;

			if (j > 0.08f) fg = true;
			if (j < -0.08f){fg = false;k++;}

			if (isBalloonYoko == true) {
				ballon.GetComponent<RectTransform> ().localPosition = new Vector2 (ballon.GetComponent<RectTransform> ().localPosition.x+j, ballon.GetComponent<RectTransform> ().localPosition.z);
				ballonH.GetComponent<RectTransform> ().localPosition = new Vector2 (ballonH.GetComponent<RectTransform> ().localPosition.x+j, ballonH.GetComponent<RectTransform> ().localPosition.z);
			} else {
				ballon.GetComponent<RectTransform> ().localPosition = new Vector2 (ballon.GetComponent<RectTransform> ().localPosition.x, ballon.GetComponent<RectTransform> ().localPosition.z+j);
				ballonH.GetComponent<RectTransform> ().localPosition = new Vector2 (ballonH.GetComponent<RectTransform> ().localPosition.x+j, ballonH.GetComponent<RectTransform> ().localPosition.z);
			}
			/*
			*/
			yield return new WaitForSeconds (0.01f);

			if (k == 10)
			{
				k = 0;
				yield return new WaitForSeconds (0.5f);
			}
		}

		yield return new WaitForSeconds (0.4f);
	}

	public void changeState(Cpc.KumaState state, bool isNet=false)
	{
		nowState = state;

		if (isNet == false)
		{
			Cpc.netC.SendKumaState(state, _myTeam);
		}
		else
		{
			//外部ネット更新の場合、自分のくまの状態を同期とる
//			if (Cpc.myTeam == _myTeam)
//			{
				if (state == Cpc.KumaState.Sitting)
				{
					changeBalloon ("やったね！");
				}
				else if (state == Cpc.KumaState.Lifting)
				{
					changeBalloon ("戻るぞ！");
					//エフェクト消去
//					delEffect();
				}
				else if (state == Cpc.KumaState.Infested)
				{
					changeBalloon ("助けて！");
				}
				else
				{
					changeBalloon ("おいおい！");
				}
//			}
		}
	}
}
